# **React Hook是用来做什么的**

关于React Hook的作用，让我们先从一段代码讲起。

例如：下面我们在一个页面中写入一个init方法，它的作用是在进入页面的时候实时获取服务端数据，然后初始化页面，并且在获取过程中动态切换“loading”图标。init方法在componentDidMount方法中调用

> 【备注】不同的页面请求时用到的请求链接是不一样的

```js
export default class Test extends React.Component {
  state = {currentInfo: {}, loading: true};

  componentDidMount() {
    this.init();
  }

  init = () => {
    request.get(config.getUrl).then(({data}) => {
      const {currentInfo} = data;
      this.setState({currentInfo, loading: false});
    });
  };
  render () {
    // ...
  }
}
```

如果我们有多个页面有这个功能需求，我们自然想把这个方法也迁移到其他页面。

如果采用"复制-粘贴"的方式就显得太粗暴了，会增加很多冗余代码

于是我们想：**能不能把init这个方法抽象成一个外部方法进行复用呢？**

我们可能会感到有点棘手：因为这个方法涉及到了组件内state，并和其紧密耦合在一起,抽取出来比较困难。



## **于是我们开始思考一个问题**

- 页内state相关的逻辑,包括**state的修改和响应**的逻辑怎么**提取和复用**？

## **也许我们可能会想到几种方式去解决它**

- HOC：将逻辑提取到高阶组件中，在高阶组件中写state和请求方法，然后将state和方法通过props传递到需要的子组件中，类似于Redux的connect
- Render-Props: 除了HOC外，还可以借助render-props实现，它是React 组件之间使用一个值为函数的 prop 共享代码的技术

> 参考资料

```text
https://zh-hans.reactjs.org/docs/render-props.html#gatsby-focus-wrapper
```

## **但仔细想想，这样还是会带来一些问题**

- render-props的使用方式比较“别扭”，有时比较难以阅读
- 我们如果需要大量的和细粒度的逻辑抽象，并将其注入一个组件上的时候，无论是HOC还是render-props，都会面临组件层级复杂化和数据流难以追溯的问题

## **能否升级为Redux?**

这个看起来也不太可行

- 一来Redux是全局共享状态，是个单例。而我们上面各个页面请求的数据并不一样，也就是说：**我们预期的是“共享状态逻辑”，而非“共享状态”，这就偏离了Redux的场景**
- Redux编写时涉及较多跨文件编程，例如Action/Reducer/Selector等，即使场景能用，偶尔会让人感觉“杀鸡用牛刀”

## **于是我们想实现以下需求**

- 使用更简单方便，最好在框架层面支持
- 使用方式不会带来组件结构复杂化的副作用
- 使用起来“轻量化”，不用编写太多跨文件的代码

## **于是我们看到了React hook**

而React Hook的可以帮我们解决这个问题，它提供的useState这个基础API，就可以帮助我们实现对组件内state的抽象和复用，而且简单优雅。

下面是一个使用React hook改造上面逻辑的例子：

- useInitEffect是一个自定义hook,我们将有关React-state的逻辑都封装到这个自定义hook里面，然后就就可以随处引入和复用了
- useEffect是对上面componentDidMount内逻辑做的抽象封装
- useState是对state响应变化相关逻辑的抽象封装

代码如下

```js
import React, { useEffect, useState } from "react";

// 通过setTimeOut模拟请求
const request = {
  get: function(getUrl) {
    return new Promise((resolve, reject) => {
      setTimeout(
        () => resolve({ currentInfo: `request address: ${getUrl}` }),
        3000
      );
    });
  }
};

function useInitEffect(url) {
  let [currentInfo, setCurrentInfo] = useState("default value");
  let [loading, setLoading] = useState(true);
  useEffect(() => {
    request.get(url).then(({ currentInfo }) => {
      setCurrentInfo(currentInfo);
      setLoading(false);
    });
  }, [url]);
  return [loading, currentInfo];
}
```

上面的useInitEffect是一个自定义hook，编写完了我们可以在不同的组件(页面)中使用它

```js
function Page1() {
  let [loading, currentInfo] = useInitEffect("url1");
  return <div>{loading ? "正在加载中" : currentInfo}</div>;
}

function Page2() {
  let [loading, currentInfo] = useInitEffect("url2");
  return <div>{loading ? "正在加载中" : currentInfo}</div>;
}

export default function Example() {
  return (
    <div>
      <Page1 />
      <Page2 />
    </div>
  );
}
```

**运行结果如下**

初始加载时



![img](https://pic4.zhimg.com/80/v2-111f2c949101fddd01c539875b9a78bb_720w.webp)



3秒后



![img](https://pic1.zhimg.com/80/v2-457e0c9cedf5bb80596d4ffa4fc95cf4_720w.webp)





## **使用React hook的好处**

- 提供涉及state和生命周期钩子逻辑的抽象和复用，解决了过去因为和组件耦合导致的state和钩子的复用问题
- 将props的链式数据传导转化为横切式的传导方式，减少了props的传导层级，简化了结构
- 使函数组件也能操作state，完善了函数式组件的功能，避免了因为逻辑的复杂化时要把函数组件改成class组件的麻烦
- 和render-props/高阶组件实现方案的对比：比起别扭的render-props来，Hook的代码结构清晰明了。而相比起高阶组件，hook有内置的API，使用起来更简单方便

## **React Hook的使用要求**

- Hook 是 React 16.8 的新增特性
- Hook只能在函数式组件或者自定义hook中使用，不能在class组件中使用
- Hook可以作为子组件和其他class组件一起嵌套在组件层级中
- Hook只能在顶层调用，不能在循环中使用，因为这可能会导致预料之外的bug

##  **从Class组件开发过渡到Hook开发面临的两个问题**

熟悉Class组件开发的同学，从日常熟悉的Class组件过渡到Hook开发， 难免遇到一些水土不服的问题。究其原因，是因为很多需要关注的细节点发生了变化，我将这些细节总结成了以下几个方面

1. 计算损耗问题
2. 旧值问题



## **计算损耗问题**

其中之一是计算损耗问题，需要意识到的是

- 我们在class组件编程中，state和this作用域变量的初始化会在constructor中完成，而且一般的事件处理函数或者钩子都是放在render函数外部的，这使我们比较少需要关注重复渲染时的冗余调用问题。
- 而在React hook中，我们会把state的初始化和hook调用写在函数渲染组件内部，这些hook在每次组件重新渲染的时候都是会被重新调用的。这就需要我们关注它们是否会被冗余调用的问题。

当你发现有这种问题的时候，也许可以从以下两个方面去考虑和解决。

**1.回调参数**

一些hook例如useState，提供了回调接收参数的方式，这样只在第一次调用时候会处理回调，从而避免重复调用导致的性能损失。当然，一般情况下如果useState只是携带基本类型数值的话，因为性能损耗很小则可以忽略不计。

```js
export default function Example() {
  const [count, setCount] = useState(() => {});
  return <div>{count}</div>;
}
```

**2.依赖数组**

另外一种功能是依赖数组，它会筛选钩子函数的调用时机，依赖数组的功能在React hook中很常见，例如useEffect/useCallback/useMemo都提供了依赖数组的功能，并且使用方法都是类似的

**A.非空数组: useEffect只在count改变时候的时候调用**

```js
useEffect(() => {
  // ...
}, [count]);
```

**B.空数组: useEffect只在函数组件入栈的时候调用一次**

```js
useEffect(() => {
  // ...
}, []);
```



## **旧值问题**

另一个是旧值问题

- 在Class编程方式中，我们经常和this打交道，例如通过this直接访问获取一个值，这种引用取值的方式通常能够帮我们获取到最新的值
- 在React hook中，因为函数式组件没有可以访问的“this”,我们在不经意之间可能会访问到预期之外的旧值。

下面请看：

首先我们用Class组件的方式编写一个样例：

- 页面上有一个按钮和一个弹框
- 点击按钮后在页面上同步显示[点击次数]
- 点击弹框时在几秒后把当前[点击次数]以弹框的形式弹出。

```js
export default class Example extends React.Component{
  state = {
    count:0
  }
  handleAlertClick = () => {
    setTimeout(() => {
      alert(`你点击了:${this.state.count}次`);
    }, 3000);
  }

  onClick = () => {
    const { count:preCount } = this.state;
    this.setState({
      count: preCount+1
    });
  }

  render () {
    const { count } = this.state;
    return (
      <div>
        <p>You clicked {count} times</p>
        <button onClick={this.onClick }>按钮</button>
        <button onClick={this.handleAlertClick}>弹框</button>
      </div>
    );
  }
}
```



![img](https://pic3.zhimg.com/80/v2-fabf73795edcf2572789a20c3be64896_720w.webp)

运行之后看起来没什么问题：无论先点击按钮和弹框，几秒后弹框弹出的都是当前点击次数的最新统计值。

**下面我们把这个例子通过React Hook的方式改造一下：**

```js
import React, {  useState } from "react";
export default function Example() {
  const [count, setCount] = useState(0);

  function handleAlertClick() {
    setTimeout(() => {
      alert("你点击了: " + count);
    }, 3000);
  }

  return (
    <div>
      <p>You clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>按钮</button>
      <button onClick={handleAlertClick}>弹框</button>
    </div>
  );
}
```

这个时候我们观察到一个“bug”现象：

- 如果先点击按钮再点击弹框，那么显示是符合预期的，即弹框内的统计数等于点击次数
- 但如果先点击弹框再点击按钮，显示就不符合预期了，弹框内的统计数< 总点击次数



![img](https://pic4.zhimg.com/80/v2-7509c58f15df09832f72beda1cf65307_720w.webp)



为什么会出现这两种截然不同的结果？我们来分析一下。

1.先点按钮再点弹框

这会触发setCount并使页面重渲染，handleAlertClick会被重新声明，重新声明时它获取到的count是最新的，这时候当然页面显示是同步的

2.先点弹框再点按钮

点击弹框的瞬间就发起了一个异步调用，这个时候读取的count是一个基本类型的数值而不是一个引用，所以它的值就被“固定”下来了，这就是导致弹框内的弹出次数滞后于按钮点击次数的原因

**使用useRef解决这个问题**

useRef创建的是一个在函数组件生命周期内一直存续的对象引用，能够帮助解决这种“旧值”问题

如下所示，我们通过useRef进行改造

```js
import React, { useCallback, useEffect, useState, useRef } from "react";

export default function Example() {
  const [count, setCount] = useState(0);
  const countRef = useRef(0);
  function handleAlertClick() {
    // 通过ref获取最新值
    setTimeout(() => {
      alert("你点击了: " + countRef.current);
    }, 3000);
  }

  return (
    <div>
      <p>你点击了按钮 {count} 次</p>
      <button
        onClick={() => {
          setCount(count + 1);
          // 修改ref内存储的数据
          countRef.current = count + 1;
        }}
      >
        按钮
      </button>
      <button onClick={handleAlertClick}>弹框</button>
    </div>
  );
}
```

如此以来，就可以解决以上的旧值问题了