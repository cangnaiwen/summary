# UseMemo 和 UseCallback 使用场景

## 背景

在React项目中，在含有父子组件的页面中，我们会遇到，父组件的属性修改了，子组件的属性并没有修改，但是还是会被重新Render。这时我们需要缓存一些内容，以避免在需渲染过程中因大量不必要的耗时计算而导致的性能问题。React 提供了 memo 来解决这样的场景。

## React.memo

将子组件放在 React.memo 里，子组件就不会随这父组件的影响了，只有在依赖的Props发生变化时才执行。但React.memo有一个情况，还是会被执行。那就Props里传入的是一个函数，当父组件被重新执行的时候，函数地址也会发生变化，这样刚好触发了子组件依赖的Props发生变化，从而导致执行。

## UseMemo

这时UseMemo来监控依赖数据（如 count），若是count 变化，才会执行子组件。

```js
import { useMemo, useState, memo } from "react";

let Child = memo((props) => {
  console.log("render Con")
  return (<div onClick={props.onClick}>{props.value}</div>)
})

export default function Home() {
  const [count, setCount] = useState(0);
  const [value, setValue] = useState(0);
  const cachedValue = useMemo(() => {
    return count + 1
  }, [count])
  const handleClick = () => {}

  return (
    <> 
       <div>
         {count}
       </div>
       <div>
         {value}
       </div>
       <Child value={cachedValue} onClick={handleClick} />
       <button onClick={() => setCount(v => v + 1)}>Add Count</button>
       <button onClick={() => setValue(v => v + 1)}>Add Value</button>
    </>
  );
}
```

useMemo的第一个参数：`()=> value`，没有形参，返回一个value。第二个参数：依赖`[count]`。只有当依赖变化时，才会计算新的value。依赖没变，就重用之前的value

注意：如果你的value是一个函数，那么就要写成

```text
useMemo（()=>(x)=>console.log(x)）
```

这是一个返回函数的函数

是不是觉得很难用，那么 `useCallback` 来了

## useCallback的用法（useMemo的语法糖）

```text
useCallback（x=>console.log(x)，[count]）
```

等价于

```text
useMemo（()=>(x)=>console.log(x)，[count])
```