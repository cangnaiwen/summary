# react usestate 更新复杂数据

很多时候我们某个state数据不是简单数据类型(值类型)，而是数组、对象之类(引用类型)。而React组件的更新机制对state只进行浅对比，也就是更新某个复杂类型数据时只要它的引用地址没变，那就不会重新渲染组件。



主要的解决办法：改变指向变成一个新的对象

const arrCopy = array.slice() 深拷贝简单数组

const objCopy = Object.assign({}, obj) 深拷贝简单Object

注意：当对象中只有一级属性，没有二级属性的时候，Object.assign()方法为深拷贝，但是对象中有对象的时候，此方法在二级属性以后就是浅拷贝。
