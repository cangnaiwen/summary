- **useEffect：**

  useEffect可以帮助我们在DOM更新完成后执行某些副作用操作，如数据获取，设置订阅以及手动更改 React 组件中的 DOM 等

  有了useEffect，我们可以在函数组件中实现 像类组件中的生命周期那样某个阶段做某件事情,具有:

  - componentDidMount
  - componentDidUpdate
  - componentWillUnmount

  基本用法：

  ```text
  useEffect(() => {
      console.log('这是一个不含依赖数组的useEffect，每次render都会执行！')
  })
  ```

  useEffect的规则

  - 没有传第二个参数时，在每次 render 之后都会执行 useEffect中的内容
  - useEffect接受第二个参数来控制跳过执行，下次 render 后如果指定的值没有变化就不会执行
  - useEffect 是在 render 之后浏览器已经渲染结束才执行
  - useEffect 的第二个参数是可选的，类型是一个数组
  - 根据第二个参数的不同情况，useEffect具有不同作用

  **useCallback和useMemo**

  useCallback 和 useMemo 都是性能优化的手段，类似于类组件中的 shouldComponentUpdate，在子组件中使用 shouldComponentUpdate， 判定该组件的 props 和 state 是否有变化，从而避免每次父组件render时都去重新渲染子组件。

  **区别**

  useCallback 和 useMemo 的区别是useCallback返回一个函数，当把它返回的这个函数作为子组件使用时，可以避免每次父组件更新时都重新渲染这个子组件，

  ```text
  const renderButton = useCallback(
       () => (
           <Button type="link">
              {buttonText}
           </Button>
       ),
       [buttonText]    // 当buttonText改变时才重新渲染renderButton
  );
  ```

  useMemo返回的的是一个值，用于避免在每次渲染时都进行高开销的计算。例：

  // 仅当num改变时才重新计算结果

  ```text
  const result = useMemo(() => {
      for (let i = 0; i < 100000; i++) {
        (num * Math.pow(2, 15)) / 9;
      }
  }, [num]);
  ```