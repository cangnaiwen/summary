# 关于React中「useEffect」的五个使用问题

之前的一篇文章讲到了如何在useEffect函数里使用async？[葡萄zi：React useEffect 不支持 async function 你知道吗？](https://zhuanlan.zhihu.com/p/425129987)，但是关于他还有很多的“故事”，本文就来揭开他的神秘面纱～～～～～

你在使用 useEffect 的过程中是否有过这样的疑惑：

- useEffect 可以模拟类组件中的componentDidMount生命周期函数吗？可以是可以，但是好像又有些不同。
- 为什么有时候在 useEffect 里拿到的是旧的 state 或 prop？
- 在 useEffect 里发送请求的正确姿势是什么？[] 又是什么？
- 为什么有时候会出现无限重复请求的问题？
- 函数能作为 useEffect 的依赖吗？如果不能有啥问题呢？

如果你看完这几个问题依然“晕乎乎”的，那么请你继续往下看。

## 1、useEffect 可以模拟类组件中的componentDidMount生命周期函数吗？

我们可以用类似 useEffect(fn, []) 的方式来模拟 componentDidMount，但它们并不完全相等！**和 componentDidMount 不一样，useEffect 会捕获 props 和 state，所以即便在回调函数里，我们拿到的还是初始的 props 和 state**。这句话啥意思，看完下面的[例子](https://link.zhihu.com/?target=https%3A//codesandbox.io/s/bold-panini-t53gq%3Ffile%3D/src/App.js)你就懂啦

```js
import React, { useState, useEffect } from "react";

function FuncExample() {
  const [count, setCount] = useState(0);
  useEffect(() => {
    setTimeout(() => {
      console.log("FuncExample", count);
    }, 10000);
  }, []);
  return (
    <div>
      <p>FuncExample clicked {count} times</p>
      <button onClick={() => setCount(count + 1)}>Click me</button>
    </div>
  );
}

class ClassExample extends React.Component {
  state = {
    count: 0
  };
  componentDidMount() {
    setTimeout(() => {
      console.log("ClassExample", this.state.count);
    }, 10000);
  }
  render() {
    const { count } = this.state;
    return (
      <div>
        <p>ClassExample clicked {count} times</p>
        <button onClick={() => this.setState({ count: count + 1 })}>
          Click me
        </button>
      </div>
    );
  }
}

export default function App() {
  return (
    <div className="App">
      <FuncExample />
      <ClassExample />
    </div>
  );
}
```

上面例子里分别在类组件里使用 componentDidMount 和 函数组件里用 useEffect 模拟 componentDidMount，看下最后的运行结果：

![img](https://pic3.zhimg.com/80/v2-aee6b9dd89074eef60c63565aab82f76_1440w.webp)



我们发现当我分别在两个组件里点击了五次之后，在两个组件里得到的值：类组件是5，函数组件里模拟后的结果是 0，现在懂上面那句话的意思了吧，useEffect里记录的是最初的值！

如果我们想要得到最新的值，可以使用 ref 来进行操作，不过通常会有更简单的实现方式，所以并不一定要用 ref，关于这一点我们会在下面详细来进行介绍。

effect 的运行方式和 componentDidMount 以及其他生命周期是不同的，**effect 更倾向于数据同步，而不是响应生命周期事件。**

## 2、为什么有时候在 useEffect 里拿到的是旧的 state 或 prop？

useEffect 拿到的总是定义它的那次渲染中的 props 和 state，这一点通过上面的例子也能看的出来，当useEffect的第二个参数传递 [] 数组的时候，拿到的是旧的值。为啥这样设计呢？因为这种情况下意味着该 hook 只在组件挂载时运行一次，并非重新渲染。

如果我的 effect 的依赖频繁变化，我该怎么办？（官网例子）

有时候，useEffect 可能会使用一些频繁变化的值。我们可能会忽略依赖列表中 state，但这通常会引起 Bug：

```js
function Counter() {
  const [count, setCount] = useState(0);

  useEffect(() => {
    const id = setInterval(() => {
      setCount(count + 1); // 这个 effect 依赖于 `count` state
    }, 1000);
    return () => clearInterval(id);
  }, []); //   Bug: `count` 没有被指定为依赖

  return <h1>{count}</h1>;
}
```

上面的例子里在在 setInterval 的回调中，count 的值不会发生变化。因为当 effect 执行时，我们会创建一个闭包，并将 count 的值被保存在该闭包当中，且初值为`0`。每隔一秒，回调就会执行 setCount(0 + 1) ，因此，count 永远不会超过 1

或许你会说可以指定`[count]`作为依赖列表，但这样会导致每次改变发生时定时器都被重置。 要解决这个问题，可以使用`"https://zh-hans.reactjs.org/docs/hooks-reference.html#functional-updates">setState的函数式更新形式。它允许我们指定 state 该*如何* 改变而不用引用*当前*state：`

```js
function Counter() {
  const [count, setCount] = useState(0);

  useEffect(() => {
    const id = setInterval(() => {
      setCount(c => c + 1); // ✅ 在这不依赖于外部的 `count` 变量
    }, 1000);
    return () => clearInterval(id);
  }, []); // ✅ 我们的 effect 不使用组件作用域中的任何变量

  return <h1>{count}</h1>;
}
```

此时，`setInterval`的回调依旧每秒调用一次，但每次`setCount`内部的回调取到的`count`是最新值（在回调中变量命名为`c`）

但是`setCount(c => c + 1)`也并不完美，它看起来有点怪，并且非常受限于它能做的事，如果我们有两个互相依赖的状态，或者我们想基于一个`prop`来计算下一次的`state`，它并不能做到。幸运的是，`setCount(c => c + 1)`有一个更强大的模式，那就是`useReducer`。它可以让我们把组件内发生了什么（`actions`）和状态如何响应并更新分开表述 。具体的demo可以参考这篇文章，本文就不copy了 ：[https://adamrackis.dev/state-and-use-reducer/](https://link.zhihu.com/?target=https%3A//adamrackis.dev/state-and-use-reducer/) 大概的意思就是 **用一个`dispatch`依赖去替换 useEffect 的依赖。**

## 3、在 useEffect 里发送请求的正确姿势是什么？[] 又是什么？

这个问题参考之前的 [葡萄zi：React useEffect 不支持 async function 你知道吗？](https://zhuanlan.zhihu.com/p/425129987) 这篇文章吧，或者还是看不懂我写的 那就耐心看下大佬写的这篇[文章](https://link.zhihu.com/?target=https%3A//www.robinwieruch.de/react-hooks-fetch-data)吧 。

至于[] 是啥？他表示 useEffect 的依赖列表是空的，就是类似于 componentDidMount 的效果，只在初始化的时候执行一次！

## 4、为什么有时候会出现无限重复请求的问题？

这个通常发生于我们在 effect 里做数据请求并且没有设置 effect 依赖参数的情况，如果没有设置依赖，effect 会在每次渲染后执行一次，然后在 effect 中更新了状态引起渲染并再次触发 effect，这样就形成了无限循环，但是也可能是因为我们设置的依赖总是在改变。

这个问题怎么解决呢？通过一个一个移除的方式排查出哪个依赖导致了问题吗？这样通常是不对的。比如某个函数可能会导致这个问题，我们可以把它们放到`effect`里，或者提到组件外面，或者用`useCallback`包一层，或者使用`useMemo`等方式都可以避免重复生成对象。

其中useCallback 本质上是添加了一层依赖检查，使用useCallback函数完全可以参与到数据流中，可以说如果一个函数的输入改变了，这个函数就改变了，如果没有，函数也不会改变。这个解决方案也可以参考 [葡萄zi：React useEffect 不支持 async function 你知道吗？](https://zhuanlan.zhihu.com/p/425129987)

## 5、函数能作为 useEffect 的依赖吗？如果不能有啥问题呢？

![img](https://pic1.zhimg.com/80/v2-0bf8f6ec1d887affb208e9dde14a8b78_1440w.webp)

一般建议把不依赖`props`和`state`的函数提到相关组件外面，并且把那些仅被`effect`使用的函数放到`effect`里面，这样做了以后，如果我们的`effect`还是需要用到组件内的函数，包括通过`props`传进来的函数。[官方说明](https://link.zhihu.com/?target=https%3A//reactjs.org/docs/hooks-faq.html%23is-it-safe-to-omit-functions-from-the-list-of-dependencies)

有啥方法解决吗？我们可以在定义它们的地方用`useCallback`包一层。因为使用useCallback函数完全可以参与到数据流中，因此这些函数可以访问到`props`和`state` 。

举个 ：

```js
function Parent() {
  const [query, setQuery] = useState('test')
  const fetchData = useCallback(() => {
    const url = '...' + query
    // ...
  }, [query])
  return <Child fetchData={fetchData} />
}

function Child({ fetchData }) {
  let [data, setData] = useState(null)
  useEffect(() => {
    fetchData().then(setData)
  }, [fetchData])
  // ...
}
```

`fetchData`只有在`Parent`的`query`状态变更时才会改变，所以我们的`Child`只会在需要的时候才去重新请求数据，使用`useCallback`，函数完全可以参与到数据流中，我们可以说如果一个函数的输入改变了，这个函数就改变了，如果没有，函数也不会改变。

类似的，`useMemo` 可以让我们对复杂对象做类似的事情

```js
function App() {
  const [color, setColor] = useState('pink')
  const style = useMemo(() => ({ color }), [color])
  return <Child style={style} />
}
```

到处使用`useCallback`并不好，当我们需要将函数传递下去并且函数会在子组件的`effect`中被调用的时候，`useCallback`是很好的技巧，但总的来说`Hook`本身能更好地避免。

也推荐看下官推荐的在大型的组件树中的解决方式：[https://zh-hans.reactjs.org/docs/hooks-faq.html#what-can-i-do-if-my-effect-dependencies-change-too-often](https://link.zhihu.com/?target=https%3A//zh-hans.reactjs.org/docs/hooks-faq.html%23what-can-i-do-if-my-effect-dependencies-change-too-often)

![img](https://pic2.zhimg.com/80/v2-6d1371b1ee6caf34289e6dd6cb895c79_1440w.webp)

这篇文章的内容部分参考了[深入 useEffect](https://link.zhihu.com/?target=https%3A//heptaluan.github.io/2020/11/07/React/17/) 这篇文章，摘取的部分内容做了深入的研究学习，记录此文，希望能帮助到你～