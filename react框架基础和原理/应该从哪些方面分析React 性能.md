# 应该从哪些方面分析React 性能

进行任何性能优化的首先你要知道有哪些衡量的指标？其次找出存在的问题？然后才能针对性地进行优化。

## 一、性能分析指标有哪些

**定性**：加载性能、运行性能：滚动&更新

**定量**：

1. 加载性能指标：reponseStart、domInteractive、DomContentLoadedEventEnd、loadEventStart、FCP FSP FMP TTI
2. 运行性能指标：FPS 、 内存 CPU I/O 网络 磁盘

**治理**：采集、运维

更多可参考：[葡萄zi：你了解哪些衡量前端性能的指标？](https://zhuanlan.zhihu.com/p/425144740)

## 二、性能分析的两个阶段

### 分析阶段

1. 通过分析器(Profiler)找出重新渲染的组件、重新渲染的次数、以及重新渲染耗费的资源与时间
2. 变动检测，通过分析器我们可以知道：什么被重新渲染？重新渲染的代价？变动检测要回答的问题就是： 为什么这些进行了重新渲染？

### 优化阶段

优化阶段我们针对分析阶段抛出的问题进行解决，下面简单列举下React 进行渲染性能优化的三个方向：

1、**前端通用优化。**这类优化在所有前端框架中都存在，重点就在于如何将这些技巧应用在 React 组件中。

2、**减少不必要的组件更新。**这类优化是在组件状态发生变更后，通过减少不必要的组件更新来实现，对应到 React 中就是：**减少渲染的节点 、降低组件渲染的复杂度、充分利用缓存避免重新渲染**（利用缓存可以考虑使用PureComponent、React.memo、hook函数useCallback、useMemo等方法）

> PureComponent 是对**类组件**的 Props 和 State 进行浅比较；React.memo 是对**函数组件**的 Props 进行浅比较

3、**提交阶段优化。**这类优化的目的是减少提交阶段耗时。

详细的优化方法可参考：待完成

## 三、通过工具查看指标和度量

### 1、 React Dev Tools & Redux Dev Tools

React v16.5 引入了新的 Profiler 功能，让分析组件渲染过程变得更加简单，而且可以很直观地查看哪些组件被渲染.

![img](https://pic3.zhimg.com/80/v2-49da7296f3aeb2ccf980df0a8960b72e_1440w.webp)

**高亮更新**

首先最简单也是最方便的判断组件是否被重新渲染的方式是“**高亮更新**(Hightlight Updates)”，通过高亮更新，基本上可以确定哪些组件被重新渲染。

设置方式如下：

![img](https://pic1.zhimg.com/80/v2-896bf9a7b03b3d4e60a07e6667f4aefc_1440w.webp)

例如合理使用了React.memo的列表组件比不使用，性能更好，“纯组件”是 React 优化的第一张牌, 也是最有效的一张牌。

**分析器**

如果`高亮更新`无法满足你的需求，比如**你需要知道具体哪些组件被渲染、渲染消耗多少时间、进行了多少次的提交(渲染)等等**, 这时候就需要用到分析器了.

来了解一下 Profiler 面板的基本结构：

![img](https://pic1.zhimg.com/80/v2-2f1562b11bb6a5c5b743b115ddd3301c_1440w.webp)

1、 commit 列表

commit 列表表示录制期间发生的 commit(可以认为是渲染) 操作，要理解 commit 的意思还需要了解 React 渲染的基本原理。在 v16 后 React 组件渲染会分为两个阶段，即 render 和 commit 阶段。

- render 阶段决定需要进行哪些变更。这个阶段 React 会调用 render 函数，并将结果和上一次 render 的结果进行 diff, 计算出需要进行变更的操作队列
- commit阶段。或者称为提交阶段, 在这个阶段会执行 render 阶段 diff 出来的变更请求。比如 DOM 插入、更新、删除、排序等等。在这个阶段 React 还会调用 componentDidMount 和 componentDidUpdate 生命周期函数.

2、选择其他图形展示形式

例如`Ranked 视图`，这个视图按照渲染消耗时间对组件进行排序：

![img](https://pic1.zhimg.com/80/v2-33342243b6a861f7e82697e7d683f6d0_1440w.webp)

3、火焰图

这个图其实就是**组件树**，Profiler 使用颜色来标记哪些组件被重新渲染。和 commit 列表以及 Ranked 图一样，颜色在这里是有意义的，比如灰色表示没有重新渲染；从渲染消耗的时间上看的话: `黑色 > 黄色 > 蓝色`, 通过 Ranked 图可以直观感受到不同颜色之间的意义

4、另外可以通过设置，筛选 Commit，以及是否显示原生元素:

![img](https://pic2.zhimg.com/80/v2-cc51596e8730e6ffe466188943683895_1440w.webp)


5、当前选中组件或者 Commit 的详情, 双击具体组件可以详细比对每一次 commit 消耗的时间

![img](https://pic2.zhimg.com/80/v2-31b809274af9a2eeee6ed328e564de35_1440w.webp)



简单总结下查看流程：

1、改配置：排除影响因素，去掉无意义的 commit，开启 render 原因记录

![img](https://pic4.zhimg.com/80/v2-e7ed25de3de31e4b992dc9e73b5ff147_1440w.webp)

2、横看缩略图

![img](https://pic3.zhimg.com/80/v2-8f4bdf4a9a329d75f12f5294191b8ad6_1440w.webp)

3、纵看火焰图

![img](https://pic2.zhimg.com/80/v2-281ec2e7a16333ac3183c4e008088661_1440w.webp)

4、跟踪单个组件

![img](https://pic3.zhimg.com/80/v2-ade8aa8355553836767b58e288197a52_1440w.webp)

### 2、Chrome Dev Tools

a、Performance

在 v16.5 之前，一般都是利用 Chrome 自带的 Performance 来进行 React 性能测量:

![img](https://pic3.zhimg.com/80/v2-eca4e413d40c5d4bf0bdf605ca02597a_1440w.webp)

React 使用标准的`User Timing API`(所有支持该标准的浏览器都可以用来分析 React)来记录操作，所以我们在 Timings 标签中查看 React 的渲染过程。React 还特意使用 emoji 标记。


相对 React Devtool 而言 Performance 工具可能还不够直观，但是它非常强大， **使用 Performance 可以用来定位一些比较深层次的问题，这可能需要你对 React 的实现原理有一定了解, 就像使用 Wireshark 你需要懂点网络协议一样**

所以说使用 Performance 工具有以下优势:

- 可以测量分析整个渲染的过程细节. 它可以定位某些具体方法的调用过程和消耗, 方便定位一些深层次问题.
- 可以测量分析底层 DOM 的绘制、布局、合成等细节。方便定位浏览器性能问题

**具体应该怎么看？**

- 尽可能缩小范围
- 主要看：时序图、火焰图、占比图

![img](https://pic1.zhimg.com/80/v2-905e6b4b186721b541ad18c62925d87c_1440w.webp)

火焰图

b、Memory

![img](https://pic1.zhimg.com/80/v2-356a31c2bac6cd0834a16b4c8a088e90_1440w.webp)

c、Light House

![img](https://pic3.zhimg.com/80/v2-5ea739e4af0cf55e10d0bed519e0e55a_1440w.webp)