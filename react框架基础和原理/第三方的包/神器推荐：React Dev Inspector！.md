# 神器推荐：React Dev Inspector！

## 景 

1. 有没有遇到过刚到公司，接手项目就去排bug或者一个小需求，因为项目太复杂，你找不到你要修改的对应文件位置？
2. 有没有遇到看到别人项目，想看看实现却因为项目结构太深，而花了1个小时以上才找到你想要的地方。

react-dev-inspector([https://github.com/zthxxx/react-dev-inspector](https://link.zhihu.com/?target=https%3A//github.com/zthxxx/react-dev-inspector))就是为了这些目的而诞生的。

它的神奇之处就在于，可以从页面上识别react组件，直接跳转到本地ide的代码片段上，厉害吧

[](https://vdn6.vzuu.com/SD/722dd3ba-2ee3-11eb-b89b-aa779726a8c3.mp4?pkey=AAXmytqkM_DDK0EAXuFNtULAocTfpMKsBdINglr2mobu3UP8vgxhumyIW62oYXD97I2MbGz2ZpGJfvBs71Jy-g-4&c=avc.0.0&f=mp4&pu=078babd7&bu=078babd7&expiration=1672128904&v=ks6)



## 好好研究一下

源码位置：

[https://github.com/zthxxx/react-dev-inspector/tree/master/sitegithub.com/zthxxx/react-dev-inspector/tree/master/site](https://link.zhihu.com/?target=https%3A//github.com/zthxxx/react-dev-inspector/tree/master/site)

![img](https://pic1.zhimg.com/80/v2-99807d87f9b14194ac967d19e72fe76c_1440w.webp)



### 安装

```text
npm i -D react-dev-inspector
```

### 使用和配置

![img](https://pic1.zhimg.com/80/v2-f6f93d3e71e708d5dd71d8d9aaeb7a90_1440w.webp)

> 可以自定义开关键值，或者在devtool里面通过window.REACT_DEV_INSPECTOR_TOGGLE()开启

### `<Inspector>` 组件属性

ts类型定义文件在 `react-dev-inspector/es/Inspector.d.ts`

![img](https://pic3.zhimg.com/80/v2-ac1f4a503baa6cf267079623834e24de_1440w.webp)

### umi3插件

![img](https://pic2.zhimg.com/80/v2-a66495c415fa178ca67e8bb7c9a7a879_1440w.webp)

### webpack插件

![img](https://pic3.zhimg.com/80/v2-5b9130ca6c03c9aeead4bc11bf48089a_1440w.webp)

### 示例

代码: [https://github.com/zthxxx/react-dev-inspector/tree/master/site](https://link.zhihu.com/?target=https%3A//github.com/zthxxx/react-dev-inspector/tree/master/site)

演示: [https://react-dev-inspector.zthxxx.me](https://link.zhihu.com/?target=https%3A//react-dev-inspector.zthxxx.me)

## 原理

### 1. 如何跳转到指定文件的line?

- react官方浏览器插件有相关能力，可惜他会跳转到chrome source中，利用的是v8的api，inspect(xxx), 这里显然不是我们想要的结果
- 最简单来说，在create-react-app中，当出现错误后，会出现一层error overlay，点击对应的错误栈，就会跳转到对应的地方
- 通过查看对应源码，在react-dev-utils（[https://github.com/facebook/create-react-app/blob/master/packages/react-dev-utils/launchEditorEndpoint.js](https://link.zhihu.com/?target=https%3A//github.com/facebook/create-react-app/blob/master/packages/react-dev-utils/launchEditorEndpoint.js)）中能找到对应的详细实现。
- 以vscode为例子，就是在错误栈找到相关信息，在dev server层增加一个createLaunchEditorMiddleware，点击后在middleware层执行 code xxx.js的指令就可以，详细指令可以查看相关文档

### 2. 如何获取到react组件的相关信息

- 这里采取的方案是webpack loader。通过ast遍历，获取相关JSXOpeningElement的相关file，line，column信息。把这些信息绑定在了指定dom的data attributes上，这样你在hover或者click的时候就能获得对应组件的相关信息了。
- 查找组件的displayName，这里利用react fiber架构在dom上绑定__reactInternalInstance$属性的特点，通过其type.displayName获取组件的displayName，如果找不到则递归找其return父组件