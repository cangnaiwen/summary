# React 函数父子组件调用

作者：P13G13
链接：https://www.zhihu.com/question/366760813/answer/2802760496
来源：知乎
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。



我们都知道[子组件](https://www.zhihu.com/search?q=子组件&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A2802760496})调用父组件的方法，可以直接通过传递的props参数使用即可。那么父组件如何调用子组件的方法呢。

------

我们知道ref无法挂载到函数组件，因为其无法生成实例。

### useImperativeHandler函数使用

第二个参数代表将返回的对象内容，挂载到父组件的[ref.current](https://www.zhihu.com/search?q=ref.current&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A2802760496})上。

```text
import React, { useRef } from 'react';

const ParentCom= () => {
    const cRef = useRef(null);
    
    const handleClick = () => {
        if(cRef.current){
            cRef.current.getData()
        }
    }

    return (
        <div>
            <button onClick={handleClick}>点我</button>
            <ChildCom ref={cRef} />
        </div>
    )
}
```

### forwardRef函数

创建一个[ref组件](https://www.zhihu.com/search?q=ref组件&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A2802760496})，这个组件会将接收到的ref属性转发到其他组件数下的另一个组件中。

```text
import React, { useImperativeHandle, forwardRef } from 'react';

const ChildCom = forwardRef(({...props}, ref) => {

    useImperativeHandle(ref, () => ({
        getData
    }))
    
    const getData = () => {
        console.log('cCom data')
    }
    
    return (
        <div>子组件</div>
    )
})
```

### 写在最后

文章是平时开发要点的总结，通过两个useImperativeHandler和forwardRef的Hooks函数实现父组件调用子组件的方法，希望能够帮助到大家。如有错误和遗漏，欢迎指出。