# 如何理解 react.js 中的事件系统



从一下几个方面解答下这个问题吧

## 为什么会有合成事件？

React中的事件是一个合成事件（SyntheticEvent），React 根据 [W3C 规范](https://link.zhihu.com/?target=https%3A//www.w3.org/TR/DOM-Level-3-Events/)来定义这些合成事件，它的产生主要有两方面原因

1、统一解决了跨浏览器的兼容性问题

2、避免这类DOM事件滥用，如果DOM上绑定了过多的[事件处理函数](https://www.zhihu.com/search?q=事件处理函数&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A2243562173})，整个页面响应以及内存占用可能都会受到影响

## 合成事件的原理是什么？

React并不是将[click事件](https://www.zhihu.com/search?q=click事件&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A2243562173})绑在该div的真实DOM上，而是在document处监听所有支持的事件，当事件发生并冒泡至document处时，React将事件内容封装并交由真正的处理函数运行.

其中，由于[event对象](https://www.zhihu.com/search?q=event对象&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A2243562173})是复用的，事件处理函数执行完后，属性会被清空，所以event的属性无法被异步访问，详情请查阅[event-pooling](https://link.zhihu.com/?target=https%3A//reactjs.org/docs/events.html%23event-pooling)

![img](https://picx.zhimg.com/80/v2-b98e564ccee95084d0e14a4df037a28a_720w.webp?source=1940ef5c)

## 如何理解react中的click事件？

提供onClick和onClickCapture参数，分别表示添加冒泡阶段的[侦听函数](https://www.zhihu.com/search?q=侦听函数&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A2243562173})，和捕获阶段的侦听函数，他们在react dom树上的发生顺序与原生JS事件流的捕获冒泡顺序相同，由外层到内层触发捕获，再由内层到外层触发冒泡

> 注意：[react dom树](https://www.zhihu.com/search?q=react dom树&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A2243562173})的结构 与 编译后真实dom[树结构](https://www.zhihu.com/search?q=树结构&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A2243562173})可能不同，如图所示，真实的dom结构是同一层级，而我们在代码中写到了最外层的div内部

![img](https://picx.zhimg.com/80/v2-a2311e61e8a3814c2f520dffdb02bea4_720w.webp?source=1940ef5c)

![img](https://picx.zhimg.com/80/v2-91e7dca244a9a1607c06cb2a80a75994_720w.webp?source=1940ef5c)

此时，如果最外层上的div添加了[点击事件](https://www.zhihu.com/search?q=点击事件&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A2243562173})监听，我们点击商品详情这部分依然会冒泡到外层div上，这个点击事件仍然会被触发。

## 自定义的原生事件

### 如何添加原生事件？

虽然React封装了几乎所有的原生事件，但有些场景需要添加原生事件，诸如：

- Modal开启以后点其他空白区域需要关闭Modal
- 引入了一些以原生事件实现的第三方库，并且相互之间需要有交互

由于原生事件需要绑定在真实DOM上，所以一般是在componentDidMount阶段/ref的函数执行阶段进行绑定操作，在componentWillUnmount阶段进行解绑操作以避免[内存泄漏](https://www.zhihu.com/search?q=内存泄漏&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A2243562173})。

### 原生事件在react代码中发生时机

目前React提供的的[事件函数](https://www.zhihu.com/search?q=事件函数&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A2243562173})是可以满足正常需求的，我们也很少会在react代码中手动添加[事件监听函数](https://www.zhihu.com/search?q=事件监听函数&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A2243562173})，但是搞清楚事件被触发时机是很重要的，所以我们简单做了如下测试

![img](https://picx.zhimg.com/80/v2-389970fa22f5aa3e578549c79ff51544_720w.webp?source=1940ef5c)

结论如下：

1. 除了在document上自定义的事件发生符合正常事件流顺序，其它自定义的原生事件都会优先触发完成，然后触发[react合成事件](https://www.zhihu.com/search?q=react合成事件&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A2243562173})。
2. 阻止原生事件的冒泡后，会阻止合成事件的[监听器](https://www.zhihu.com/search?q=监听器&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A2243562173})执行
3. 使用合成事件的e.stopPropagation()不能阻止冒泡到原生的document上面的事件触发，只能阻止外层react合成事件
4. 合成事件阻止自定义的document上的冒泡，可以在合成事件中使用如下代码

```text
e.nativeEvent.stopPropagation(); // 不好使❌
e.nativeEvent.stopImmediatePropagation(); // 横向阻止多个事件监听器中的非必要执行
```

> 原理: *合成事件的代理并不是在document上同时注册捕获/冒泡阶段的事件监听器的，事实上只有冒泡阶段的事件监听器*，每一次DOM事件的触发，React会在event._dispatchListeners上注入所有需要执行的函数，然后依次循环执行

path为react的[组件树](https://www.zhihu.com/search?q=组件树&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A2243562173})，由下向上遍历，就是[child, parent]； 然后先将标记为captured的监听器置入_dispatchListeners，此时顺序是path从后往前； 再是标记为bubbled的监听器，顺序是从前往后