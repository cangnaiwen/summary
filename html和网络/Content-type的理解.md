# Content-type的理解

## **请求头中常见的content-type**

请求头中的content-type在chrome浏览器的network里能够看到。

![img](https://pic2.zhimg.com/80/v2-1a3aa335a96796c2efb669e33d2ff8ad_720w.webp)

1）、application/x-www-form-urlencoded

在使用**表单提交**时，请求方式是**post**时，form标签的属性entry=”application/x-www-form-urlencoded“（这也是默认值），请求头中的content-type的值就是 application/x-www-form-urlencoded。同时，浏览器会自动把处于form标签里的表单元素的内容组织成键值对的方式（`key1=val1&key2=val2`）。其中，键 就是每个表单元素的name属性的值；值就是表单元素的value属性的值。键和值都进行了URL的转码。并放到请求实体里面。（注意如果是中文或特殊字符如"/"、","、“:" 等会自动进行URL转码）。代码示例如下：

```html
  <form method="POST" action="regSave.php" >
        用户名：<input type="text" name="username" /><br/>
        密  码：<input type="password" name="userpass" /><br/>
        重复密码：<input type="password"  /><br/>
        <input type="submit" value="注册" />
    </form>   
```

如果使用ajax发送post请求，需要用 setRequestHeader();设置content-type。代码如下：

```js
XMLHttpRequest对象.setRequestHeader("Content-type","application/x-www-form-urlencoded");
```

2）、multipart/form-data

这个一般使用在**文件上传时**。表单提交方式，请求方式是**post**，form标签的属性 entry="multipart/form-data",请求头中的content-type的值是： multipart/form-data; boundary=----WebKitFormBoundarybi5XUwMxC7yc9s5a。既可以上传文件等二进制数据，也可以上传表单键值对，只是最后会转化为一条信息。

示例代码：

```html
   <form action="/upload" enctype="multipart/form-data" method="post">
        <input type=file  name="fieldname" />
        <input type="submit" value="上传">
    </form>
```

3）、application/json

这种格式（json格式）是目前在前后端分离开发场景中使用最多的的。前后端的数据交互使用json格式进行，可阅读性好，简介，方便。

这种格式一般用在发送ajax请求时，要么明确设置了 content-type 是application/json。要么，有的第三方库默认是application/json。如下示例代码：

```js
XMLHttpRequest对象.setRequestHeader("Content-type","application/json");
```

4）、text/xml

这种格式（XML格式）是比较早的格式。现在用的比较少了，因为XML格式的数据被json的数据大，复杂。所以，基本上被淘汰了。

## **响应头中的常见的content-type**

如果在响应头里，有Content-Type: text/html; charset=utf-8。表示，我服务器端给你客户端响应的内容是html格式的内容，字符集是utf-8。