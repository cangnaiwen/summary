# [get请求数组参数问题（序列化）](https://www.cnblogs.com/lgnblog/p/15628640.html)

**问题：**

当我们需要通过get方式传递一个数组作为参数 tag:[1,2,3,4]

预期是解析为:`https://www.cnblogs.com/enter?tag=1&tag=2&tag=3&tag=4`

然而真相是这样的：`https://www.cnblogs.com/enter?tag[]=1&tag[]=2&tag[]=3&tag[]=4`,后台是不可能解析到传递的参数。

 

**解决方法**

1、qs插件处理

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```js
1、qs.stringify({ a: ['b', 'c'] }, { arrayFormat: 'indices' })
// 输出结果：'a[0]=b&a[1]=c'
2、qs.stringify({ a: ['b', 'c'] }, { arrayFormat: 'brackets' })
// 输出结果：'a[]=b&a[]=c'
3、qs.stringify({ a: ['b', 'c'] }, { arrayFormat: 'repeat' })
// 输出结果：'a=b&a=c'
4、qs.stringify({ a: ['b', 'c'] }, { arrayFormat: 'comma' })
// 输出结果：'a=b,c'
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

2、使用URLSearchParams方式

```js
let params = new URLSearchParams()
 params.append('ids','1')
 params.append('ids','2')

//输出结果ids=1&ids=2
```

3、Axios配置

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```js
axios.interceptors.request.use(async (config) => {
//只针对get方式进行序列化
 if (config.method === 'get') {
   config.paramsSerializer = function(params) {
     return qs.stringify(params, { arrayFormat: 'repeat' })
   }
 }
}
```

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

4、小程序

[![复制代码](https://common.cnblogs.com/images/copycode.gif)](javascript:void(0);)

```js
 let urlQueryString = qs.stringify(options.params, {   //使用到qs ,先下载，后引入
      addQueryPrefix: true,
      allowDots: true,
      arrayFormat: 'repeat'
    });
    myUrl += urlQueryString;
```