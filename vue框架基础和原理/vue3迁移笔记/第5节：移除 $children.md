# 第5节：移除 $children

## 概述

在 Vue 3.x 中，将移除且不再支持 $children 。

## Vue 2.x 中的 $children

官方描述：
vm.$children
类型：Array<Vue instance>

只读

详细：

当前实例的直接子组件。**需要注意 $children 并不保证顺序，也不是响应式的。**如果你发现自己正在尝试使用 $children 来进行数据绑定，考虑使用一个数组配合 v-for 来生成子组件，并且使用 Array 作为真正的来源。

## 用法

借助$children，可以获取到当前组件的所有子组件的全部实例。所以，借助$children可以访问子组件的data、方法等。一种常用的用法就是用于父子组件间的通讯途径。

### Vue 3.x 中 $children的替代形式$refs

因为 Vue 3.x 中，$children被移除了。如果我们要想访问子组件实例，可以通过$refs来实现。

### Vue 2.x 中，$children和$refs对比

在Vue 2.x 中$refs就已经存在了。

如下例子：


```vue
<template>
  <div>
    <child id="my-id" class="my-class" attrA="inProps" attrB="outProps" ref="child"></child>
  </div>
</template>
<script>
import child from "@/components/classANdStyle/child.vue";
export default {
  components:{
    child
  },
  mounted(){
    console.log(this.$children)
    console.log(this.$refs)
  }
};
</script>
```

控制台输出：

![](https://img-blog.csdnimg.cn/20210127112238706.jpg)

结论：

 可以看出，两者获取到的数据基本没有什么差别。所以，`$refs`替代`$children`完成没问题。

上面例子在 Vue 3.x 运行的结果：

![](https://img-blog.csdnimg.cn/20210127112248565.jpg)