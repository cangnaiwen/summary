# 第1节：v-for 中的 Ref 数组

Vue 2.x v-for 中的 Ref 数组的问题
在 Vue 2 中，在 v-for 语句中使用ref属性时，会生成refs数组插入$refs属性中。

如下：

<div v-for="item in 5" :key="item" :ref="item">
  {{ item }}
</div>
```vue
<div v-for="item in 5" :key="item" :ref="item">
  {{ item }}
</div>
```

$refs的结果如下：

![](https://img-blog.csdnimg.cn/20210126101609934.jpg)

由于当存在嵌套的v-for时，这种处理方式会变得复杂且低效。

```vue
<div v-for="item in 5" :key="item" :ref="item">
  <div v-for="y in 5" :key="y" :ref="y">
    {{ item }} - {{ y }}
  </div>
</div>
```

$refs的结果如下：

![](https://img-blog.csdnimg.cn/20210126101627716.jpg)

Vue 3.x 的优化
因此，在 Vue 3，在 v-for 语句中使用ref属性 将不再会自动在$refs中创建数组。而是，将 ref 绑定到一个 function 中，在 function 中可以灵活处理ref。

如下：

```vue
<template>
	<div>
		<h1>v-for Array Refs</h1>
		<div>
			<div v-for="item in 5" :key="item" :ref="setItemRef">
				 {{ item }}
			</div>
			<button @click="print">打印 $refs</button>
		</div>
	</div>
</template>
<script>
export default {
	name: 'VforArrayRefs',
	data() {
		return {
			itemRefs: []
		};
	},
	methods: {
		setItemRef(el) {
			this.itemRefs.push(el);
		},
		print() {
			console.log('打印 $refs：');
      console.log(this.$refs);
			console.log('打印 itemRefs');
      console.log(this.itemRefs);
		}
	}
};
</script>
```

结果如下：




注意：![](https://img-blog.csdnimg.cn/2021012610164867.jpg?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3dlaXhpbl80NDg2OTAwMg==,size_16,color_FFFFFF,t_70)

itemRefs不强制要求是数组，也可以是对象；
如有必要，itemRef 也可以是响应式的且可以被监听。（这点很好理解，因为itemRefs是data属性）
