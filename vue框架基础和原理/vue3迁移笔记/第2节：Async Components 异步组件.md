# 第2节：Async Components 异步组件

## 概述

Vue 3.x 中，对异步组件的使用跟 Vue 2.x 不同了。变化主要有三点：

异步组件声明方法的改变：Vue 3.x 新增一个辅助函数defineAsyncComponent，用来显示声明异步组件;
异步组件高级声明方法中的 component 选项更名为loader；
loader绑定的组件加载函数不再接收resolve和reject参数，而且必须返回一个Promise；

## 辅助函数defineAsyncComponent

在 Vue 2.x 中，声明一个异步组件只需这样：

```js
const asyncPage = () => import('./NextPage.vue')
```

但是，到了 Vue 3.x 上面的用法就不适用了。

如下例子：

```vue
<template>
	<div>
		<h1>Async Components</h1>
		<p>异步组件测试</p>
    <child></child>
	</div>
</template>

<script>
const child = () => import('@/components/async-component-child.vue')

export default {
  name: 'async-components',
  components:{
    'child': child
  }
};
</script>
```



异步加载的组件将不会再页面显示，且控制台报如下错误：

![](https://img-blog.csdnimg.cn/20210126101804678.jpg)

在 Vue 3.x 中，异步组件的导入需要使用辅助函数defineAsyncComponent来进行显式声明。如下：

```vue
<template>
	<div>
		<h1>Async Components</h1>
		<p>异步组件测试</p>
    <child></child>
	</div>
</template>

<script>
import { defineAsyncComponent } from 'vue'
const child = defineAsyncComponent(() => import('@/components/async-component-child.vue'))

export default {
  name: 'async-components',
  components:{
    'child': child
  }
};
</script>
```

Vue 3.x 引入defineAsyncComponent辅助函数的原因：

Now, in Vue 3, since functional components are defined as pure functions, async components definitions need to be explicitly defined by wrapping it in a new defineAsyncComponent helper.

机器翻译如下：

现在，在 Vue 3 中，由于函数组件被定义为纯函数，异步组件定义需要通过将其包装在一个新的 defineAsyncComponent helper 中来显式定义。

想要对其进一步了解，可查阅：0008-render-function-api-change

## component 选项更名为loader

Vue 2.x 中异步组件的声明有更高级的声明方式。

```js
const asyncPageWithOptions  = {
  component: () => import('./NextPage.vue'),
  delay: 200,
  timeout: 3000,
  error: ErrorComponent,
  loading: LoadingComponent
}
```

所以，下面的异步组件声明

```js
const asyncPage = () => import('./NextPage.vue')
```

等价于

```js
const asyncPageWithOptions  = {
  component: () => import('./NextPage.vue')
}
```

同样的，Vue 3.x 中也可以这样声明异步组件。只是，其中的component需要改为loader。

```js
const asyncPageWithOptions  = defineAsyncComponent({
  loader: () => import('./NextPage.vue'),
  delay: 200,
  timeout: 3000,
  error: ErrorComponent,
  loading: LoadingComponent
})
```

## 组件加载函数不再接收resolve和reject参数，而且必须返回一个Promise

此外，Vue 3.x 的异步组件加载函数将不再接收resolve和reject，而且必须始终返回**Promise**

```js
// 2.x version
const oldAsyncComponent = (resolve, reject) => {
  /* ... */
}

// 3.x version
const asyncComponent = defineAsyncComponent(
  () =>
    new Promise((resolve, reject) => {
      /* ... */
    })
)
```

也就是说，工厂函数接收 resolve 回调的方式定义异步组件在 Vue 3.x 不能使用了：

```js
export default {
  components: {
    asyncPage: resolve => require(['@/components/NextPage.vue'], resolve)
  },
}
```

在 Vue 2.x，异步组件的声明有两种方式：

结合 webpack 的 code-splitting 功能 声明异步组件，其工厂函数接收 resolve 回调；
基于 ES2015 和 Webpack 2 声明异步组件，其工厂函数返回一个 Promise 对象。
想了解更多，请查阅：《异步组件》

## 与路由懒加载的区分

上面提及的变化只适用于异步组件，对于路由懒加载是无效的。也就是说，Vue 2.x 路由懒加载怎么写，在Vue 3.x 就怎么写。

这里我们需要区分路由懒加载和异步组件。

### 路由懒加载写法

```js
// router/index.js
const routes = [
	{
		path: '/async-component',
		name: 'asyncComponent',
		component: () =>
			import(
				/* webpackChunkName: "asyncComponent" */ '@/components/async-component.vue'
			)
	}
];
```

### 异步组件的写法

```vue
<template>
	<div>
		<h1>Async Components</h1>
		<p>异步组件测试</p>
    <child></child>
	</div>
</template>


<script>
import { defineAsyncComponent } from 'vue'
const child = defineAsyncComponent(() => import('@/components/async-component-child.vue'))

export default {
  name: 'async-components',
  components:{
    'child': child
  }
};
</script>
```

简单来说，写在路由配置文件中的异步加载就是路由懒加载的用法，而写在组件内部的异步加载就是异步组件用法。
