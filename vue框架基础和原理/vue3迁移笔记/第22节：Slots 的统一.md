# 第22节：Slots 的统一

## 概述

Vue 3.x 统一了$slots 和 $scopedSlots:

this.$slots 作为函数对位暴露
移除 this.$scopedSlots

## Vue 2.x 的 $slots 和 $scopedSlots

Vue 2.x，当我们用渲染函数书写组件时，就需要用到$slots 和 $scopedSlots。

你可以通过 [`this.$slots`](https://cn.vuejs.org/v2/api/#vm-slots) 访问静态插槽的内容，每个插槽都是一个 VNode 数组：

```js
render: function (createElement) {
// `<div><slot></slot></div>`
return createElement('div', this.$slots.default)
}
```

也可以通过 [`this.$scopedSlots`](https://cn.vuejs.org/v2/api/#vm-scopedSlots) 访问作用域插槽，每个作用域插槽都是一个返回若干 VNode 的函数：

```js
props: ['message'],
render: function (createElement) {
	// `<div><slot :text="message"></slot></div>`
  return createElement('div', [
     this.$scopedSlots.default({
         text: this.message
     })
  ])
}
```

## Vue 3.x 的 `$slots` 和 `$scopedSlots`

在 Vue 3.x, slots 被定义为当前节点的子节点且作为一个对象：

```js
render: function (createElement) {
// `<div><slot name="header"></slot></div>`
return createElement('div',{}，{
	header: ()=> createElement('div')
	})
}
```

当你需要以编程方式引用作用域 slot 时，它们现在被统一到 `$slots` 选项中

```js
// 2.x Syntax
this.$scopedSlots.header

// 3.x Syntax
this.$slots.header()
```

