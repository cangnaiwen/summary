# vue3 hooks使用

## **hooks 特点**

vue3 中的 hooks 函数相当于 vue2 里面的 mixin 混入，不同在于 hooks 是函数。

vue3 中的 hooks 函数可以提高代码的复用性，能够在不同的组件当中都利用 hooks 函数。

hooks 函数可以与 mixin 连用，但是不建议。

## **hooks 基本使用**

首先我们不管 hooks 哈，我们先写一个小功能，就是获取页面的宽高值，这个是讲解 hooks 的常用案例了，都是老演员了，我们也来整一个。

```js
<template>
  <h3>hooks</h3>

  <p>页面宽度: {{screen.width}}</p>
  <p>页面高度: {{screen.height}}</p>

  <el-button @click="getWH">获取页面的宽高</el-button>
</template>
<script setup>
  import { reactive } from 'vue'

  const screen = reactive({
    width: 0,
    height: 0
  })

  const getWH = () => {
    screen.width = document.documentElement.clientWidth
    screen.height = document.documentElement.clientHeight
  }

</script>
<style scoped>

</style>
```

上面的代码其实很简单了就，有两个标签，显示可视页面的长度和宽度，然后有一个按钮获取最新的长宽进行显示。

![img](https://pic3.zhimg.com/80/v2-300e1cf14d6806e40dae99b62f4d5f0e_720w.webp)

这个功能是可以顺利实现的哈。如果我们需要在另一个页面也想实现这个功能的话，也很简单，在直接把上面的代码复制一下到另一个需要实现的页面就可以了。但是 有没有发现一个问题，就是一个页面需要就复制一遍，一个页面需要就复制一遍，如果有一百个页面就复制一百遍，代码一两行还好，如果是一个超级庞大的工具类，那么在像这样实现的话，是不是就过于复杂了，而且还不好实现，那这个问题怎么解决呢？啊哈哈哈哈，没错了宝子们，就是 hooks 。

我们针对上面的案例，我们使用 hooks 简单的实现一下。

首先，我们在 src 文件夹下创建一个 hooks 文件夹。

![img](https://pic1.zhimg.com/80/v2-d5a51223458a8052b8cced50ceded960_720w.webp)

在 hooks 文件夹下创建一个文件，名字就叫做 useScreenWh.js 文件

![img](https://pic2.zhimg.com/80/v2-901804c0ee4e877cac5f9444ac3978f5_720w.webp)

接下来就很简单了，我们把获取可视化界面的代码放进这个 js 文件，然后导出去，给其他页面使用就可以了。

```text
import { reactive } from 'vue'

export default function () {  // 导出一个默认方法

  // 创建一个对象，保存宽度和高度值
  const screen = reactive({
    width: 0,
    height: 0
  })

  // 创建一个方法，获取可视化界面的宽度和高度值
  const getWH = () => {
    screen.width = document.documentElement.clientWidth
    screen.height = document.documentElement.clientHeight
  }

  return { screen, getWH }  // 方法返回宽高值
}
```

然后在需要使用 hooks 的文件引入就可以使用了。

```text
<template>
  <h3>hooks</h3>

  <p>页面宽度: {{screen.width}}</p>
  <p>页面高度: {{screen.height}}</p>

  <el-button @click="getWH">获取页面的宽高</el-button>

</template>
<script setup lang="ts">

  // 导入 hooks 
  import screenWH from '../hooks/useScreenWh.js'

  // 因为 screenWH 是一个导出的方法，所以需要调用一下子，然后顺便解构一下就可以在模板使用了。
  let { screen, getWH } = screenWH()

</script>
<style scoped>

</style>
```

好了，我们保存看一下效果。

![img](https://pic3.zhimg.com/80/v2-300e1cf14d6806e40dae99b62f4d5f0e_720w.webp)

和之前是完全一样的。

好了这就是 hooks 的基本使用，注意啊，是基本使用，hooks 相关的东西很多，这里只是简单的是用，剩下的我们慢慢说.