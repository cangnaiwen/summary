# Vue3中的v-model与Vue2中的不同

一用处:

1).v-model可以用到表单元素上 ,

Vue2中v-model默认绑定的属性是value绑定的事件是@input

Vue3中v-model默认绑定的属性是modelValue,默认绑定的事件是@update:modelValue

v-model= "uname"

   双向数据的绑定,页面数据变化,绑定的数据也会跟着变化;

  绑定的数据变化,页面也会跟着变化

<input v-model="uname"/>

<input :value="uname" @input = "uname=$event.target.value">
2).v-model可以用于组件上

  实现父子之间数据交互的是双向的

   1.Vue2的规则:

 <my-com v-model="info"></my-com>

<my-com :value="info" @input = "info = $event"></my-com>

v-model仅仅只能使用一次,多次使用只能使用.sync

<my-com v-model="info" :msy.sync="msg"></my-com>
   2.Vue3的规则:

.sync被废除,Vue3中v-model可以绑定多个值第一个值为modelValue,从第二个值开始就是它本身

<my-com v-model = "info"></my-com> 

<my-com :modelValue="info" @update:modelValue="info=$event"></my-com>
父组件:

<template>
  <div>v-model指示</div>
  <hr />
  <div>{{ info }}---msg:{{ msg }}</div>
  <text-event v-model="info" v-model:msg="msg"></text-event>
  <!-- <text-event :modelValue="info" @update:modelValue="info = $evevt"></text-event> -->
</template>

<script>
import TextEvent from './TextEvent.vue'
import { ref } from 'vue'
export default {
  name: 'App',
  components: {
    TextEvent
  },
  setup() {
    const info = ref('hello')
    const msg = ref('中国你好')
    return { info, msg }
  }
}
</script>

<style></style>

 子组件:

<template>
  <div>textevent---{{ modelValue }}</div>
  <button @click="handlerClick">点击修改</button>
</template>

<script>
export default {
  name: 'TextEvent',
  props: {
    // vue3中默认的b
    modelValue: {
      type: String,
      default: ''
    },
    msg: {
      type: String,
      default: ''
    }
  },
  setup(props, context) {
    const handlerClick = function() {
      context.emit('update:modelValue', 'nihao')
      context.emit('update:msg', '你最棒!')
    }
    return { handlerClick }
  }
}
</script>

<style></style>
