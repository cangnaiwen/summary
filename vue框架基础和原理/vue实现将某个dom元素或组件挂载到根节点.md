# vue实现将某个dom元素或组件挂载到根节点

在写手机端的时候经常用到tab，tab切换一般都是transition滑动的，如果此时我们用position:fixed定位会发现，元素定位并不是我们想象中的相对浏览器定位，这是因为fixed定位会被拥有transition属性的元素影响。

为了结果这个问题，我们需要将元素变成相对于浏览器定位，这个时候我们就要将这个需要定位的dom元素挂载到根节点下，实现方式如下



------

## 1.vue2.0的实现方式

### 1.1 将某个.vue组件挂载到根节点

```text
import hello from "@/components/HelloWorld.vue";
mounted(){
    this.appendCompToRoot(hello);
},
appendCompToRoot(component) { 
    //component 参数为组件名
      var instance = new Vue({
        el: document.createElement("div"),
        render: h => h(component)
      });
      document.body.appendChild(instance.$el);
}
```

### 1.2 将某个dom元素挂载到根节点

```text
 <div class="home">    
        <h1>home</h1>   
         <div id="mountDiv" ref="mountDiv">      
                <img src="logo.png" alt />   
         </div> 
 </div> 
 mounted() {
    this.appendDomToRoot(document.querySelector("#mountDiv"), "#container");
 },
appendDomToRoot(el, selector) {
    //el 为要操作的dom元素，selector为要插入的根节点的选择器，为空的话，直接插入到body中 
      var container;
      if (selector) {
        container = document.querySelector(selector);
      }
      if (!container) {
        container = document.body;
      }
      container.appendChild(el);
    },
2.vue3.0的实现方式
```

### 2.1 将某个.vue组件挂载到根节点

只需要将想要dom放入到teleport标签中即可，这个是vue3的新特性，挺好用的

```text
<teleport to="#app">
      <div id="mountDiv" ref="mountDiv">
            <img src="logo.png" alt />
      </div>
</teleport>
```