# Vue computed 的内部实现原理

### 一. computed 的使用

vue 的计算属性在实际应用中的使用频率很高，可以避免在模板中写入复杂的逻辑表达，让代码更加简洁优雅。使用的方法也非常的简单：

```html
<div id="example">
  <p>Original message: "{{ message }}"</p>
  <p>Computed reversed message: "{{ reversedMessage }}"</p>
</div>

<script>
    var vm = new Vue({
      el: '#example',
      data: {
        message: 'Hello'
      },
      computed: {
        reversedMessage: function () {
          return this.message.split('').reverse().join('')
        }
      }
    })
</script>
```

输出结果：

Original message: "Hello"
Computed reversed message: "olleH"

### 二. computed 的特点

computed 计算属性的特点，就是基于他们的响应式依赖进行缓存的，只有当相关的响应式依赖发生改变时，他们才会重新求值，如果依赖没有改变而访问计算属性，则会立即返回之前的计算结果，并不会再次执行函数。

### 三. computed 的源码解读

源码路径：

![img](https://pic2.zhimg.com/80/v2-0f25dc9c8af8bcd67d1b4b64bd175985_720w.webp)

\1. 组件初始化状态的时候，如果设置有 computed 属性，则初始化 computed

```js
export function initState (vm: Component) {
  vm._watchers = []
  const opts = vm.$options
  if (opts.props) initProps(vm, opts.props)
  if (opts.methods) initMethods(vm, opts.methods)
  if (opts.data) {
    initData(vm)
  } else {
    observe(vm._data = {}, true /* asRootData */)
  }
  if (opts.computed) initComputed(vm, opts.computed)  // 初始化 computed 属性
  if (opts.watch && opts.watch !== nativeWatch) {
    initWatch(vm, opts.watch)
  }
}
```

\2. 初始化 computed

```js
// 将 computedWatcher 的 lazy 属性默认设置为 true
const computedWatcherOptions = { lazy: true }  

function initComputed (vm: Component, computed: Object) {
  // 创建 watcher 为一个空对象，用于存储每个 computed 属性的 watcher
  // 并同时存于组件的 _computedWatchers 属性中
  const watchers = vm._computedWatchers = Object.create(null)
  const isSSR = isServerRendering()

  // 遍历 computed 的每个属性
  for (const key in computed) {
    // 将每个computed key 的 value 存于 userDef 中
    const userDef = computed[key]  
    const getter = typeof userDef === 'function' ? userDef : userDef.get
    if (process.env.NODE_ENV !== 'production' && getter == null) {
      warn(
        `Getter is missing for computed property "${key}".`,
        vm
      )
    }

    if (!isSSR) {
      // 为每个计算属性创建一个对应的 watcher
      // 注意 computedWatcherOptions 中的 lazy 属性默认已经设置为 true
      watchers[key] = new Watcher(
        vm,
        getter || noop,
        noop,
        computedWatcherOptions
      )
    }

    // 判断这个计算属性是否已经存在于实例中，即检验每个计算属性是否与 props 或者 data 里的属性重复
    if (!(key in vm)) {
      defineComputed(vm, key, userDef)
    } else if (process.env.NODE_ENV !== 'production') {
      if (key in vm.$data) {
        warn(`The computed property "${key}" is already defined in data.`, vm)
      } else if (vm.$options.props && key in vm.$options.props) {
        warn(`The computed property "${key}" is already defined as a prop.`, vm)
      }
    }
  }
}
```

\3. 为每个计算属性做响应式处理

```js
const sharedPropertyDefinition = {
  enumerable: true,
  configurable: true,
  get: noop,
  set: noop
}

export function defineComputed (
  target: any,
  key: string,
  userDef: Object | Function
) {
  const shouldCache = !isServerRendering()
  // 如果计算属性的值是一个函数
  // get 为 createComputedGetter(key) 返回的函数
  // set 设置为空函数，所以手动设置 computed 属性的值是无效的
  if (typeof userDef === 'function') {
    sharedPropertyDefinition.get = shouldCache
      ? createComputedGetter(key)
      : createGetterInvoker(userDef)
    sharedPropertyDefinition.set = noop   
  } else {
    // 如果计算属性的值是一个对象
    // get 为用户设置的 get，get会判断对象的 cache 属性，如果没有关闭缓存，则还是createComputedGetter(key) 返回的函数
    // set 为用户设置的 set，如果用户没有设置，则为空函数
    sharedPropertyDefinition.get = userDef.get
      ? shouldCache && userDef.cache !== false
        ? createComputedGetter(key)
        : createGetterInvoker(userDef.get)
      : noop
    sharedPropertyDefinition.set = userDef.set || noop
  }
  if (process.env.NODE_ENV !== 'production' &&
      sharedPropertyDefinition.set === noop) {
    sharedPropertyDefinition.set = function () {
      warn(
        `Computed property "${key}" was assigned to but it has no setter.`,
        this
      )
    }
  }
  // 在 vm 实例上监测 computed 属性的数据变化
  Object.defineProperty(target, key, sharedPropertyDefinition)
}

function createComputedGetter (key) {
  return function computedGetter () {
    // 从实例的 _computedWatchers 属性中拿到计算属性的 key 所对应的 watcher 
    const watcher = this._computedWatchers && this._computedWatchers[key]
    if (watcher) {
      // 如果 dirty 属性为 true，执行 watcher.evaluate()
      if (watcher.dirty) {  
        watcher.evaluate()
      }
      if (Dep.target) {
        watcher.depend()
      }
      return watcher.value 
    }
  }
}

function createGetterInvoker(fn) {
  return function computedGetter () {
    return fn.call(this, this)
  }
}
```

\4. watcher 更新数据

![img](https://pic2.zhimg.com/80/v2-ad35372464ca08e4b1fa7507d5822989_720w.webp)

```js
   /**
   * Subscriber interface.
   * Will be called when a dependency changes.
   */
  update () {
    /* istanbul ignore else */
    if (this.lazy) {
      this.dirty = true
    } else if (this.sync) {
      this.run()
    } else {
      queueWatcher(this)
    }
  }

  /**
   * Evaluate the value of the watcher.
   * This only gets called for lazy watchers.
   */
  evaluate () {
    // 读取数据会触发当前属性的 get，即会执行传入的回调函数
    // 并将 dirty 属性设置为 false
    this.value = this.get()  
    this.dirty = false
  }

  /**
   * Depend on all deps collected by this watcher.
   */
  depend () {
    let i = this.deps.length
    while (i--) {
      this.deps[i].depend()
    }
  }
```

在组件 render 阶段时，读取到 computed 属性时，执行 computedGetter

1. 通过属性名，在组件的_computedWatchers属性中，获取当前 key 对应的 watcher
2. 如果 watcher.dirty 属性为 true，调用 watcher.evaluate（），因为首次读取属性时，dirty 默认为 true，所以首次获取值时会调用 watcher.evaluate（）。
3. evaluate 会触发当前属性的 get，即会执行传入的回调函数 ，并将 dirty 设置为 false。
4. 先执行 computed 的回调函数，如果回调函数里有依赖了其他属性如 this.a 和 this.b，则又会触发 a 和 b 属性的 get，则会将当前 computed 属性的 computedWatcher 收集到a，b 属性的依赖收集 dep 里，当a，b 属性发生改变时，它们的 dep 会执行它们的 computedWatcher 的 update，将 computedWatcher 的 dirty 设置为 true，表示数据已经改变，需要重新获取，而不是读取缓存。
5. a，b 发生更新，页面重新 render 时，又需要读取 computed 的属性时，则再次执行 computedGetter，此时 dirty 为 true，则触发 watcher 的 get，即中心执行回调函数，返回最新的值，如果 dirty 为 false，则返回旧的 watcher 的值。

### 四. computed 的内部原理

简单来说，computed 是定义在 vm 上的一个特殊的 getter 方法，之所以说特殊，是因为在 vm 上定义 getter 方法时，get 并不是用户提供的函数，而是 Vue.js 内部的一个代理函数，在代理函数中可以结合 watcher 实现缓存与依赖等功能。

计算属性的结果会被缓存，且只有在计算属性所依赖的响应式属性或者说计算属性的返回值发生变化时才会重新计算，那么如何知道计算属性的返回值是否发生变化？这其实是结合 watcher 的 dirty 属性来判断：当 dirty 属性为 true 时，说明数据已经“脏”了，需要重新计算“计算属性”的返回值，当 dirty 属性为 false 时，说明计算属性的值并没有改变，不需要重新计算。

当计算属性中的内容发生变化后，计算属性的 watcher 和组件的 watcher 都会得到通知，计算属性的 watcher 会将自己的 dirty 属性设置为 true，当下次读取计算属性时，就会重新计算一次值，然后组件的 watcher 也会得到通知，从而执行 render 函数进行重新渲染的操作，由于要重新执行 render 函数，所以会重新读取计算属性的值，这个时候计算属性的 watcher 已经把自己的 dirty 属性设置为 true，所以会重新计算一次计算属性的值，用于本次的渲染。

计算属性会通过 watcher 来观察它所用到的所有属性的变化，当这些属性发生变化时，计算属性会将自身的 watcher 属性设置为 true，说明自身的返回值变了。

如下图：

![img](https://pic2.zhimg.com/80/v2-89c0323f979ba39960d8b904941c45a9_720w.webp)

上诉 computed 方法存在的问题：

组件的 watcher 会观察计算属性中用到的所有数据的变化，这就导致一个问题：如果计算属性中用到的状态发生了变化，但最终的计算属性的返回值并没有改变，这时计算属性依然会认为自己的返回值变了，组件也会重新走一遍渲染流程，只不过在最终由于虚拟 DOM 的 diff 中发现没有变化，所以在视觉上并不会发现 UI 有变化，其实渲染函数也会被执行。

也就是说，计算属性只是观察它所用到的所有数据是否发生了变化，但并没有真正的去检验它自身的返回值是否有变化，所以当它所使用的数据发生了变化后，它就认为自己的返回值也会有变化，但事实并不总是这样的。

为了解决这个问题，vue 在计算属性的实现上做了一些改动，改动后的逻辑是：组件的 watcher 不再观察计算属性用到的数据的变化，而是让计算属性的 watcher 得到通知后，计算一次计算属性的值，如果发现这一次计算出来的值与上一次计算出来的值不一样，再主动通知组件 watcher 进行重新渲染操作，这样就可以解决前面提到的问题，只有计算属性的返回值真的变了，才会重新执行渲染函数。

如下图：

![img](https://pic2.zhimg.com/80/v2-ce7469cc05811ec8a44b49026b61269d_720w.webp)



### 五. 计算属性 computed，方法 methods，侦听属性 watch 的比较

\1. computed 计算属性是基于他们的响应式依赖进行缓存的，只有当相关的响应式依赖发生改变时，他们才会重新求值，如果依赖没有改变而访问计算属性，则会立即返回之前的计算结果，并不会再次执行函数。

\2. methods 没有缓存机制，只要外部条件满足，则会立即重新执行相应的函数。

\3. 虽然 computed 在大多数情况下更合适，比 watch 要轻量级，但是有时候也需要一个自定义的侦听器 watch，特别是，当需要在数据变化时执行异步或者开销较大的操作时，watch 最有用。