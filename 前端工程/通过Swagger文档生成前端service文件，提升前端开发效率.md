# 通过Swagger文档生成前端service文件，提升前端开发效率

在企业级的项目开发过程中，一般会采用前后端分离的开发方式，前后端通过api接口进行通信，所以接口文档就显得十分的重要。

目前大多数的公司都会引入Swagger来自动生成文档，大大提高了前后端分离开发的效率。

但是在前端开发过程中还是会出现一些问题，比如：

- 由于需求的频繁变更，接口也会相应的改变
- 多人协作时，每个人的代码风格不同，导致service文件非常混乱，不易于维护
- 新人接手时，不清楚接口有没有定义，导致重复定义接口
- 每次定义接口都是重复性工作，消耗鼠标键盘耐久

所以，如果能把这种重复性的工作交给机器做就好了，既能保证速度，也能保证质量，何乐而不为呢。

在github上找了一下这类工具，发现了umijs有一款插件叫openapi，可以实现生成service文件的功能。但是在使用过后发现这个工具存在不少问题，比如：对中文的支持不太好，如果Swagger文档中出现了中文会报错。而且它只支持最新的Swagger文档规范（OAS3）,公司里用的还是旧的OAS2规范。

所以，我决定自己造一个轮子。

我把这个工具分成三个步骤：

1. 从Swagger文档的url或者json中获取OAS2/OAS3数据
2. 通过OAS2/OAS3数据生成一个通用的数据结构
3. 通过js模板库把通用的数据结构转换成service文件

[![img](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/0cfbc963d08043a6a6cd97acdf3f9656~tplv-k3u1fbpfcp-zoom-1.image)](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/0cfbc963d08043a6a6cd97acdf3f9656~tplv-k3u1fbpfcp-zoom-1.image)

生成service步骤

我把这个工具命名为openapi-tool，通过名字就可以看出，我并不想它只能做生成service的工作，他应该是一个围绕着OpenApi的工具。所以，我加入了插件机制，并参考了Vue插件系统的设计，使得学习成本更低。通过插件系统，可以对接Mock平台，也可以做自动化接口测试等进一步提高开发效率的功能。

下面是使用openapi-tool的一个栗子：

[![img](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/780a0e06c2964705bf9147cf177f70a1~tplv-k3u1fbpfcp-zoom-1.image)](https://p3-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/780a0e06c2964705bf9147cf177f70a1~tplv-k3u1fbpfcp-zoom-1.image)

生成的结果如下：



```markdown
└── service
    ├── api.ts
    ├── login.ts
    ├── rule.t
    └── typings.ts
```

可以看到，生成ts文件时会自动生成对应的类型文件，并自动引入依赖。如果想生成js文件，只需要把typescript设置为false即可。