# 前端搭建npm私服

npm私服的使用场景
为什么需要搭建npm私服

可能有人会说我们不是可以把包发布到npm上吗？为什么要废那么大的劲搭建自己的私服呢
这是因为npm上的包其实是不能发私包的，如果需要发布的话需要支付7$一个人。
如何搭建
本文以linux为背景

安装docker(因为docker是一个可移植的容器)
可以使用yum安装，直到输入docker version显示如下图，表示成功

![](https://img-blog.csdnimg.cn/2020033122290870.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3ZpbmNlbnRjaGVuenE=,size_16,color_FFFFFF,t_70)

安装nexus

![](https://img-blog.csdnimg.cn/20200331223353850.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3ZpbmNlbnRjaGVuenE=,size_16,color_FFFFFF,t_70)

```js
docker search nexus  // 查找nexus的包
docker pull sonatype/nexus3  // 下载nexus的镜像
docker images  // 使用此命令可以看到刚刚下载的nexus镜像包
```

3. 启动nexus

```js
docker run -p 8081:8081 --name vincentNexus sonatype/nexus3 
```

此处将docker的8081端口映射到外网的8081端口

4. 登录密码

   ![](https://img-blog.csdnimg.cn/20200331224908331.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3ZpbmNlbnRjaGVuenE=,size_16,color_FFFFFF,t_70)

此时发现密码在/nexus-data/admin.password中
输入以下命令，将vincentNexus替换成你自己的nexus名字，红框为admin的密码
登录之后可以修改密码

![](https://img-blog.csdnimg.cn/20200331225219459.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3ZpbmNlbnRjaGVuenE=,size_16,color_FFFFFF,t_70)

搭建完毕如何使用
按照如下步骤点击Create repository

![](https://img-blog.csdnimg.cn/2020040122253974.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3ZpbmNlbnRjaGVuenE=,size_16,color_FFFFFF,t_70)

2. 我们需要创建三个仓库
  npm(hosted) – 自己发布私仓的地址

  ![](https://img-blog.csdnimg.cn/20200401223335223.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3ZpbmNlbnRjaGVuenE=,size_16,color_FFFFFF,t_70)

npm(proxy) – 这个相当于代理，和npm淘宝代理的功能是一样的
只需要填下面2个框的内容 直接创建即可

![](https://img-blog.csdnimg.cn/20200401223132231.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3ZpbmNlbnRjaGVuenE=,size_16,color_FFFFFF,t_70)

npm(group) – 可以将下面两个合并为一个group

![](https://img-blog.csdnimg.cn/20200401223651986.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3ZpbmNlbnRjaGVuenE=,size_16,color_FFFFFF,t_70)

添加到npm registry
如果是用了nrm的话可以直接nrm add name url的方式添加

![](https://img-blog.csdnimg.cn/20200401224016766.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3ZpbmNlbnRjaGVuenE=,size_16,color_FFFFFF,t_70)

踩坑日记
Unable to authenticate, need: BASIC realm=“Sonatype Nexus Repository Manager”
这是因为package-lock.json是用了其他的registry，删除package-lock.json再重新 npm i
配置安全策略

![](https://img-blog.csdnimg.cn/20200401232149792.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3ZpbmNlbnRjaGVuenE=,size_16,color_FFFFFF,t_70)

