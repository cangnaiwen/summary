# nrm的作用和安装

## **一、nrm是什么？**

这是官方的原话：

　　开发的npm registry 管理工具 [nrm](https://link.zhihu.com/?target=https%3A//github.com/Pana/nrm), 能够查看和切换当前使用的registry, 最近NPM经常 down 掉, 这个还是很有用的哈哈

顾名思义，就是说nrm是一个管理npm的工具，如果你不知道npm是什么的话，请看下这里npm的是什么？。

## **二、nrm的安装**

*$* npm install -g nrm

## **三、nrm命令**

*$* nrm ls　　// 查看所有的支持源（有*号的表示当前所使用的源,以下[name]表示源的名称）

$ nrm use [name]　　// 将npm下载源切换成指定的源

$ nrm help　　// 查看nrm帮助

$ nrm home [name]　　// 跳转到指定源的官网

如果在你的网络不太理想或者在不能FQ的情况下，又或者收到其他网络限制导致不能使用npm原本的源进行下载时nrm就非常有用了，你只需要

　　$ nrm ls



![img](https://pic4.zhimg.com/80/v2-98b6c4f4ebe17bb119c740b7befbd6ef_720w.jpg)



　　$ nrm use [name]



![img](https://pic1.zhimg.com/80/v2-e5ef15254f3a97f913473850e978880c_720w.jpg)



即可轻松使用npm进行下载自己所需要的包及工具了。