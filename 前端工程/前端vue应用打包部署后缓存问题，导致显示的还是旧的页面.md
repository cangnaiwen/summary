# 前端vue应用打包部署后缓存问题，导致显示的还是旧的页面

这个缓存机制原理还不是很清楚，有时会出现有时不会出现，综合网上的多种方法尝试了一下：

方法一：前端打包时js和css文件加上版本号，如果版本号不同浏览器就会去拿新的文件：
在 vue.config.js 文件加入如下代码：



```js
const Version = new Date().getTime().toString().match(/.*(.{8})/)[1] // 截取时间戳后八位
module.exports = {
   configureWebpack:{
        output: {
           filename: `js/[name].${Version}.js`,
           chunkFilename: `js/[name].${Version}.js`
        }
  }
}
```

打包部署后的请求效果图：根据时间戳生成的版本号


方法二：在index.html入口文件加入如下代码：
<meta http-equiv="Expires" content="0">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-control" content="no-cache">
<meta http-equiv="Cache" content="no-cache">

方法三：nginx配置不缓存index.html入口文件（两种配置，针对的是nginx的）：
（1）方式一：


在nginx.conf页面添加如下代码：

```js
location = /index.html {
    add_header Cache-Control "no-cache, no-store";
}
```

（2）方式二：

# 在location /{} 中加入


```js
if ($request_filename ~* ^.*?.(html|htm)$) {
    add_header Cache-Control "private, no-store, no-cache, must-revalidate, proxy-revalidate";
}
```

