# 10分钟 搭建 NPM 私服

私服 npm 能做什么？

如果一个公司的项目非常多，而且有很多的方法、组件、api都是可以共用的。如果没有私服 npm，那么我们将不断的复制粘贴这些代码到各个项目中，一旦其中的某一处需要修改，那么所有的项目都需要改一遍。

更好地维护这些公共代码，比较好的处理方式是将这些公共代码封装成一个个 npm 包，但是我们又不能将公司的代码发布到外网的 npm 中，所以私服 npm 就能够很好地帮助我们解决这一问题。

 

参考文档：https://help.sonatype.com/repomanager3

1、要使用nexus服务需要安装jdk和maven

1.1、jdk下载地址：https://www.oracle.com/technetwork/java/javase/downloads/index.html

rpm -ivh jdk-8u221-linux-x64.rpm

 

vim /etc/profile

export JAVA_HOME=/usr/java/jdk1.8.0_221-amd64

export PATH=$JAVA_HOME/bin:$PATH

export CLASSPATH=.:$JAVA_HOME/lib/dt.jar:$JAVA_HOME/lib/tools.jar

 

source /etc/profile

 

java -version

 

1.2、maven下载地址：https://maven.apache.org/download.cgi

tar -zxvf apache-maven-3.6.1-bin.tar.gz

 

vim /etc/profile

export PATH=$PATH:/usr/local/maven/bin

 

source /etc/profile

 

mvn -v

 

2、安装nexus

下载地址：https://www.sonatype.com/nexus-repository-oss，点击首页大广告图，跳转后填写邮箱，点击，download，网页跳转后，选择 nexus repository manager oss 3.x - unix，下载最新安装包。

或者打开网址：https://my.sonatype.com/ 在 Latest Releases 标签下， 下载最新nexus repository安装包

wget https://download.sonatype.com/nexus/3/latest-unix.tar.gz

 

tar -zxvf nexus-3.18.1-01-unix.tar.gz

#解压后又2个目录

      #nexus-3.18.1-01：包含了 Nexus 运行所需要的文件。是 Nexus 运行必须的
    
      #sonatype-work：包含了 Nexus 生成的配置文件、日志文件、仓库文件等。当我们需要备份 Nexus 的时候默认备份此目录即可

 


#修改环境变量

vim /etc/profile

export NEXUS_HOME=/usr/local/nexus/nexus-3.18.1

export PATH=$PATH:$NEXUS_HOME/bin

 

source /etc/profile

 

#修改启动用户

vim /usr/local/nexus/nexus-3.18.1/bin/nexus.rc

#run_as_user=""                     #内容就这一行，放开注释，填写用户即可

 

#修改端口

vim /usr/local/nexus/nexus-3.18.1/etc/nexus-default.properties     #默认是8081

 

#最后启动nexus

cd /usr/local/nexus/nexus-3.18.1/bin

./nexus start

./nexus status

 

#访问http://ip:8081，登陆用户admin 密码存放在：/usr/local/nexus/sonatype-work/nexus3/admin.password 目录

 

#开机自启动

vim /etc/rc.d/rc.local

 

/usr/local/nexus/nexus-3.18.1/bin/nexus start       #添加这一行内容

 

chmod 755 /etc/rc.d/rc.local

打开登陆界面

打开设置 -> repositories 页面，点击 Create repository 按钮。

 

![](https://img-blog.csdnimg.cn/20200526100310382.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1RoZUZsc2Fo,size_16,color_FFFFFF,t_70)

先创建 npm(proxy) 仓库，即代理仓库
填入仓库名以及代理地址，代理地址可使用 npm 官方镜像地址 https://registry.npmjs.org

 

![](https://img-blog.csdnimg.cn/20200526100333261.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1RoZUZsc2Fo,size_16,color_FFFFFF,t_70)

完成后点击底部 Creaete repository 完成创建。

创建 npm(hosted) 仓库，即私服仓库

![](https://img-blog.csdnimg.cn/20200526100350826.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1RoZUZsc2Fo,size_16,color_FFFFFF,t_70)


输入仓库名即可点击底部 Creaete repository 完成创建。

创建 npm(group) 仓库，npm 组



![](https://img-blog.csdnimg.cn/20200526100406371.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1RoZUZsc2Fo,size_16,color_FFFFFF,t_70)


创建 npm(group) 需要填写仓库名，然后将 npm(proxy) 和 proxy(hosted) 设置为成员即可，点击底部 Creaete repository 完成创建。

 

 

原有的 npm 是可以不登录进行安装 npm 包的，所以我们的私服也需要改成允许匿名访问的
打开设置页面 Security -> Anonymous，勾选 Allow anonymous users to access the server 即可，点击 save 保存。

![](https://img-blog.csdnimg.cn/20200526100422653.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L1RoZUZsc2Fo,size_16,color_FFFFFF,t_70)




然后就可以登陆私服npm

npm login --registry=http://localhost:8081/repository/npm-hosted/

可以看到这次就安装成功了。

但是每次都需要指定 --registry 也太麻烦了，我们可以使用

npm config set registry http://localhost:8081/repository/npm-group/ 将 npm 默认的镜像改成我们私服的镜像。这样我们就不需要每次都指定镜像源了。

使用publish 命令发布

npm publish --registry=http://localhost:8081/repository/npm-hosted/
