# vite.config.js配置



```js
import vue from '@vitejs/plugin-vue'
import path from 'path'
import { ConfigEnv, defineConfig, loadEnv, UserConfigExport } from 'vite'
import { configCompressPlugin } from './src/plugins/config-compress-plugin'
import { configHtmlPlugin } from './src/plugins/config-html-plugin'
import { configMockPlugin } from './src/plugins/config-mock-plugin'
import { configStyleImportPlugin } from './src/plugins/config-style-import-plugin'
import { configSvgIconsPlugin } from './src/plugins/config-svg-icons-plugin'
import { wrapperEnv } from './src/utils/env'


const resolve = (dir: string) => path.join(__dirname, dir)

// 根据环境变量配置代理 https://blog.csdn.net/chendf__/article/details/115676683
// https://vitejs.dev/config/
export default ({ command, mode }: ConfigEnv): UserConfigExport => {
  const root = process.cwd()

  const env = loadEnv(mode, root)

  const isBuild = command === ('build' || 'test')

  // The boolean type read by loadEnv is a string. This function can be converted to boolean type
  const viteEnv = wrapperEnv(env)

  const {
    VITE_PORT,
    VITE_USE_MOCK,
    VITE_BUILD_COMPRESS,
    VITE_BUILD_COMPRESS_DELETE_ORIGIN_FILE,
    VITE_BASE_API
  } = viteEnv

  return defineConfig({
    base: './',
    plugins: [
      vue(),
      configSvgIconsPlugin(isBuild, 'src/assets/icons'), // svg 处理
      configStyleImportPlugin(isBuild), // element-plus 按需引入
      configHtmlPlugin(viteEnv, isBuild), //  EJS 标签处理
      configMockPlugin(VITE_USE_MOCK, isBuild), // mock 模拟请求
      configCompressPlugin(
        VITE_BUILD_COMPRESS,
        VITE_BUILD_COMPRESS_DELETE_ORIGIN_FILE
      ),// gzip 或者 brotli 来压缩资源
    ],
    resolve: {
      alias: {
        '@': resolve('src'),
        comps: resolve('src/components'),
        apis: resolve('src/apis'),
        views: resolve('src/views'),
        store: resolve('src/store'),
        routes: resolve('src/routes'),
        styles: resolve('src/styles')
      }
    },
    server: {
      //服务器主机名
      host: '',
      //端口号
      port: VITE_PORT,
      //设为 true 时若端口已被占用则会直接退出，而不是尝试下一个可用端口
      strictPort: false,
      //服务器启动时自动在浏览器中打开应用程序,当此值为字符串时，会被用作 URL 的路径名
      open: false,
      //自定义代理规则
      proxy: {
        // 选项写法
        ['http']: {
          target: VITE_BASE_API,
          // '/api': {
          //   target: 'http://192.168.1.139:8090/',
          changeOrigin: true,
          // rewrite: (path) => path.replace(/^\/api/, '')
        },
      }
    },
    build: {
      rollupOptions: {
        output: {
          manualChunks(id) { // 分包
            if (id.includes('node_modules')) {
              return id.toString().split('node_modules/')[1].split('/')[0].toString();
            }
          }
        }
      },
    }
  })
}

```

