# 最简单的创造npm包

找一个目录npm初始化一下

```js
mkdir thales-say-hello
cd thales-say-hello
npm init
```

写一个最简单的ind 

```js
exports.sayHello = () => {console.log('hello!')}
```

注意package.json

```js
{
    "name": "@rookie/say-hello",
    "version": "1.0.1",
    "description": "",
    "main": "index.js",
    "scripts": {"test": "echo \"Error: no test specified\" && exit 1"},
    "publishConfig": {"registry": "http://nexus.digitforce.com/repository/npm-hosted/"},
    "author": "thales",
    "license": "ISC"
}
```

main，就是引导的js，我们创建的index.js那就是这个，如果我们以后用的生成的dist/main.js那么就要改成这个目录

name就是组件的名字，@代表scpoe，我们可以把很多的组件放在一个scope里



登录私服

这一步很重要

```js
npm login --registry=http:``//nexus``.digitforce.com``/repository/npm-hosted/
```

我们的登录地址需要限定位http://nexus.digitforce.com/repository/npm-hosted/这一步，是咱们的显示内容和实际内容不一致，如果不懂无所谓，总之登录就行了，只登录一次就可以了。账号密码找我来创建



*对比之前的publishconfig也是这个地址，为了防止你不小心发到npm官网*



这时候，你只要执行一个publish，就可以发布了

```js
npm publish
```



这时候，我们就可以像安装别的npm包一样，安装这个了

```js
npm i -S @rookie``/say-hello
```



然后在main.js里面加载这个刚才创建的npm，运行一下就能看见结果了

```js
require(``'@rookie/say-hello'``).sayHello();
```