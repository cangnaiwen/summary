# Electron 自定义顶部菜单

如果什么都不操作，electron会有默认菜单，如果要自定义菜单，需要使用Menu模块，只能再主进程中使用

main.js

```js
// 引入Menu
const { app, BrowserWindow, Tray, Menu } = require('electron')
const path = require('path')
const Store = require('electron-store');
const store = new Store();

function createWindow() {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: true,
      contextIsolation: false,
    }
  })

  // 菜单模板设置
  let template = [
    {
      label: '用户',
      submenu: [
        {label: '切换用户'}
      ]
    },
    {
      label: '帮助',
      submenu: [
        {
          label: '控制台',
          click: () => {
            win.webContents.openDevTools({ mode: 'bottom' })
          }
        },
        {label: '关于'}
      ]
    }
  ]

  // 加载菜单
  let m = Menu.buildFromTemplate(template)
  Menu.setApplicationMenu(m)

  win.loadFile('index.html')
}

app.whenReady().then(() => {
  createWindow()

  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow()
    }
  })
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})
```