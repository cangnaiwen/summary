# electron-vue跨平台桌面应用开发实战教程（六）——打包

 前边几篇文章介绍了一些基本用法，最终都是要打包成可执行应用程序的，今天我们就讲讲怎么打包

### 1. 设置应用appId

在package.json 中增加

```json
"appId": "com.ipp.electronvue",
```

### 2. 增加vue.config.js

这个文件是用来配置打包工具electron-builder的参数，代码中有对应的注释，按照对应的配置修改为自己的图标就好。

```js
module.exports = {
  // 第三方插件配置
  pluginOptions: {
    // vue-cli-plugin-electron-builder 配置
    electronBuilder: {
      builderOptions: {
        // 设置打包之后的应用名称
        productName: 'electron-vue-demos',
        win: {
          icon: 'public/electron-icon/icon.ico',
          // 图标路径 windows系统中icon需要256*256的ico格式图片，更换应用图标亦在此处
          target: [{
            // 打包成一个独立的 exe 安装程序
            target: 'nsis',
            // 这个意思是打出来32 bit + 64 bit的包，但是要注意：这样打包出来的安装包体积比较大
            arch: [
              'x64'
              // 'ia32'
            ]
          }]
        },
        dmg: {
          contents: [
            {
              x: 410,
              y: 150,
              type: 'link',
              path: '/Applications'
            },
            {
              x: 130,
              y: 150,
              type: 'file'
            }
          ]
        },
        linux: {
          // 设置linux的图标
          icon: 'resources/ico/icon.png',
          target: 'AppImage'
        },
        mac: {
          icon: 'resources/ico/icon.icns'
        },
        files: ['**/*'],
        extraResources: {
          // 拷贝dll等静态文件到指定位置,否则打包之后回出现找不大dll的问题
          from: 'resources/',
          to: './'
        },
        asar: false,
        nsis: {
          // 是否一键安装，建议为 false，可以让用户点击下一步、下一步、下一步的形式安装程序，如果为true，当用户双击构建好的程序，自动安装程序并打开，即：一键安装（one-click installer）
          oneClick: false,
          // 允许请求提升。 如果为false，则用户必须使用提升的权限重新启动安装程序。
          allowElevation: true,
          // 允许修改安装目录，建议为 true，是否允许用户改变安装目录，默认是不允许
          allowToChangeInstallationDirectory: true,
          // 安装图标
          installerIcon: 'resources/ico/icon.ico',
          // 卸载图标
          uninstallerIcon: 'resources/ico/icon.ico',
          // 安装时头部图标
          installerHeaderIcon: 'resources/ico/icon.ico',
          // 创建桌面图标
          createDesktopShortcut: true,
          // 创建开始菜单图标
          createStartMenuShortcut: true
        }
      },
      chainWebpackMainProcess: config => {
        config.plugin('define').tap(args => {
          args[0].IS_ELECTRON = true
          return args
        })
      },
      chainWebpackRendererProcess: config => {
        config.plugin('define').tap(args => {
          args[0].IS_ELECTRON = true
          return args
        })
      }
    }
  }
}
```

在这里我们需要注意extraResources这个参数，由于打包后某些资源的路径会发生变化，例如托盘的图标，就会出现找不到的情况，所以我们把图片放在根目录的resources文件夹中，然后把resources文件夹中的资源文件打包后放到对应的位置，所以我们background.js需要做以下修改：

```js
// 设置托盘
tray = new Tray('resources/ico/icon.png')
```

### 3.执行打包命令

```text
run electron:build
```

### 4.修改系统通知的应用包名

打包之后的应用，在测试系统通知时展示 的是包名

![img](https://pic2.zhimg.com/80/v2-3cd10759b83e15dd1855b4b23bdff545_720w.webp)

我们这里把他改成应用的中文名称，只需要在background.js中增加

```js
app.setAppUserModelId('我的程序')
```



![img](https://pic2.zhimg.com/80/v2-a4a5f3f936c7a485ca271f91a1091009_720w.webp)

electron打包最主要的就是在vue.config.js 对electronBuilder打包工具的各种配置。下一篇我们讲解下怎么调用C#动态链接库dll文件。