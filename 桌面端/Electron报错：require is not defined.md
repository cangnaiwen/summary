# Electron报错：require is not defined

环境

```text
D:\study\electron-demo> electron -v
v12.0.4
```

## 错误描述

在Electron的render进程中如果直接使用ElectronAPI或者引入node包，如下

```js
<script type="module">
    const electron = require('electron')
</script>
```

就会报错

![img](https://pic4.zhimg.com/80/v2-5f9f578ca9a4271a5e03ffa9ba013c13_720w.webp)

## 解决方法

在main.js中，创建BrowserWindow时添加如下配置

```js
const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: true, // 是否启用Node integration. 默认值为 false
      contextIsolation: false, // 是否在独立 JavaScript 环境中运行 Electron API和指定的preload 脚本. 默认为 true
    }
 })
```

