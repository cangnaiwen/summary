# electron-vue跨平台桌面应用开发实战教程（四）——窗口样式&打开新窗口

本文我们讲解下怎么修改窗口样式，系统默认的窗口非常普通，通常与设计不符，所以我们要自定义，接下来我们讲解下怎么去掉原有样式，怎么实现实现窗口的最小化，最大化和关闭按钮。还有怎么打开一个新页面

前三篇文章传送门

[electron-vue跨平台桌面应用开发实战教程（一）——Hello World](https://link.zhihu.com/?target=https%3A//blog.csdn.net/David1025/article/details/104371208)

[electron-vue跨平台桌面应用开发实战教程（二）——主进程常用配置](https://link.zhihu.com/?target=https%3A//blog.csdn.net/David1025/article/details/104375882)

[electron-vue跨平台桌面应用开发实战教程（三）——动态修改窗口大小](https://link.zhihu.com/?target=https%3A//blog.csdn.net/David1025/article/details/104377605)

我们基于上一篇文章的代码（上一篇文章的git地址：[https://gitee.com/hedavid/electron-vue-demos](https://link.zhihu.com/?target=https%3A//gitee.com/hedavid/electron-vue-demos)）接着写。先看下上一篇文章实现的样式：

![img](https://pic3.zhimg.com/80/v2-17fa779cf00d25c92bfca3622ba2c0f6_720w.webp)

![img](https://pic2.zhimg.com/80/v2-59dea2ee72bd9b7beda0a2768687be45_720w.webp)

接下来我们就要开始今天的讲解了

### 1.去掉外边框

我们修改background.js，修改createWindow方法，创建BrowserWindow时增加
frame: false

```js
win = new BrowserWindow({
    width: 400,
    height: 550,
    frame: false,
    webPreferences: {
      nodeIntegration: true
    }
  })
```

这样窗口就会变成这样：

![img](https://pic2.zhimg.com/80/v2-442958b5a941247fdc938dd96d2ede3d_720w.webp)

但是现在又有一个新问题，应用没有关闭按钮，没有最小化按钮，也不能拖拽移动位置了，接下来我们为他创建一个工具条。

### 2.增加操作栏

（注意：在此处还涉及到其他页面的样式调整，在这里就不列举了，查看git上的代码即可） 我们修改Login.vue，增加以下代码（只展示关键代码，部分样式和html请查看git上的代码）：

```js
const { ipcRenderer } = require('electron')
// 点击最小化按钮调用的方法
minimize () {
    ipcRenderer.send('minimize')
},
// 点击关闭按钮调用的方法
close () {
    ipcRenderer.send('close')
},
```

窗口的拖动需要增加以下样式：

```css
-webkit-app-region: drag; // 可拖动
-webkit-app-region: no-drag; // 不可拖动
```

### 3.主线程中增加对应的最小化和关闭窗口的方法

```js
ipcMain.on('close', e =>
  win.close()
)

ipcMain.on('minimize', e =>
  win.minimize()
)
```

接下来我们改造一下主页面，来实现打开新窗口的功能

### 4. 打开新窗口

### 4.1 新建Calendar.vue

```js
<template>
  <div>
    <el-calendar v-model="value"></el-calendar>
  </div>
</template>

<script>
export default {
  name: 'Calendar',
  data () {
    return {
      value: new Date()
    }
  }
}
</script>
```

### 4.2 修改router

```js
{
    path: '/Calendar',
    name: 'Calendar',
    component: Calendar
},
```

### 4.3 修改background.js

```js
const winURL = process.env.NODE_ENV === 'development'
  ? 'http://localhost:8080'
  : `file://${__dirname}/index.html`
```

上边这段代码主要是用来解决vue路由访问页面的问题，端口号是vue启动的默认端口号，如果有需要可以自行修改

```js
win.loadURL(winURL)
```

上边这段代码主要是修改主窗口加载的url，修改我我们上边声明的那个

```js
// 定义calendar窗体
let calendarWin
// 创建calendar窗口方法
function openCalendarWindow () {
  calendarWin = new BrowserWindow({
    width: 400,
    height: 550,
    parent: win, // win是主窗口
    webPreferences: {
      nodeIntegration: true
    }
  })
  calendarWin.loadURL(winURL + '#/Calendar')
  calendarWin.on('closed', () => { calendarWin = null })
}
ipcMain.on('openCalendarWindow', e =>
  openCalendarWindow()
)
```

上边这段代码是打开新窗口的代码，calendarWin.loadURL(winURL + '#/Calendar') 是指向我们上边创建的页面。

### 4.4 在Home.vue页面中调用创建新窗口的方法

```js
openCalendarWindow () {
  ipcRenderer.send('openCalendarWindow')
}
```

接下来我们看下效果

![img](https://pic3.zhimg.com/80/v2-01672deaaa7fb1f31defa191b83f0bda_720w.webp)

以上就是今天的全部内容，下一篇我们讲一下怎么发系统通知，最小化到托盘，剪贴板