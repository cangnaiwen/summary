# Electron数据本地存储：electron-store

electron-store ：一款简单的数据持久化组件，它可以保存和加载用户首选项、应用程序状态和缓存等等

## 安装

```text
yarn add electron-store
```

## 基础使用

```js
const Store = require('electron-store');

const store = new Store();

store.set('unicorn', '123');
console.log(store.get('unicorn'));
//=> '123'

// Use dot-notation to access nested properties
store.set('foo.bar', true);
console.log(store.get('foo'));
//=> {bar: true}

store.delete('unicorn');
console.log(store.get('unicorn'));
//=> undefined

// 判断是否存在
store.has('unicorn')
//=> false
```

## 获取本地存储路径

electron-store 以json文件的形式将数据存储在本地文件，获取存储文件的目录如下：

```js
const { app } = require('electron')

// 在程序中获取electron-store文件路径
app.getPath('userData')
```