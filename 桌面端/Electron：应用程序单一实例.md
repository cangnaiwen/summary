# Electron：应用程序单一实例

所谓应用程序单一实例，就是只能有一个实例在运行。当你没有使用单一实例的时候，可以同一时间开启多个程序，不受限制，但是很多时候我们需要在同一时间只有一个程序实例运行。

要使用单一实例，核心使用如下方法

```js
app.requestSingleInstanceLock()
```

## 代码示例

main.js

```js
const { app, BrowserWindow } = require('electron')
const path = require('path')

let win = null

// 核心代码
const gotTheLock = app.requestSingleInstanceLock()

if (!gotTheLock) {
  app.quit()
} else {
  app.on('second-instance', (event, commandLine, workingDirectory) => {
    // 当运行第二个实例时,将会聚焦到myWindow这个窗口
    if (win) {
      if (win.isMinimized()) win.restore()
      win.focus()
    }
  })

  // 创建 myWindow, 加载应用的其余部分, etc...
  app.whenReady().then(() => {
    win = createWindow()
    app.on('activate', () => {
      if (BrowserWindow.getAllWindows().length === 0) {
        createWindow()
      }
    })
  })

  app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
      app.quit()
    }
  })

  
}


function createWindow() {
  win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: true,
      contextIsolation: false,
    }
  })
  

  win.loadFile('index.html')

  return win
}
```

## 其他方法

```js
// 请求单一实例锁
app.requestSingleInstanceLock()

// 此方法返回你的应用实例当前是否持有单例锁
app.hasSingleInstanceLock()

// 释放所有由 requestSingleInstanceLock 创建的锁
app.releaseSingleInstanceLock()
```