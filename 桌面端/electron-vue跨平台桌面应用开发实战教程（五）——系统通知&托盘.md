# electron-vue跨平台桌面应用开发实战教程（五）——系统通知&托盘



﻿前几篇文章，我们介绍了一些开发中经常用到的功能，这篇文章我们主要是讲解下怎么发送系统通知，设置托盘

### 1.系统通知

其实Electron的系统通知比较简单，是用HTML5的Notification实现的，但是在这儿需要注意的是，windows平台发送通知是需要为程序设置appId（下一篇文章我们讲解下Electron怎么打包，怎么设置应用程序图标）。

通知一共有两种调用方式

- HTML5 Notification API（渲染进程中使用）
- Notification模块（主进程中使用）

接下来，我们新建一个页面，来测试下渲染进程中怎么使用系统通知。

```js
<template>
  <div>
    <el-button @click="sendNotification">发送系统通知</el-button>
  </div>
</template>
<script>
export default {
  name: 'DemoTest',
  methods: {
    sendNotification () {
      const myNotification = new Notification('标题', {
        body: '通知正文内容'
      })
      myNotification.onclick = () => {
        console.log('通知被点击')
      }
    }
  }
}
</script>
```



![img](https://pic2.zhimg.com/80/v2-37f4291857b866888519ea74a6252b31_720w.webp)



### 2.设置托盘

托盘属于系统级的操作，所有这个也是在主进程中设置的。在这个需要注意的是，设置托盘必须在程序ready之后。

我们为程序设置了托盘图标和菜单。点击托盘图标重新展示主窗口。

```js
app.on('ready', async () => {
  if (isDevelopment && !process.env.IS_TEST) {
  }
  // 设置托盘
  const tray = new Tray('./src/assets/icon_tray.png')
  // 设置托盘菜单
  const trayContextMenu = Menu.buildFromTemplate([
    {
      label: '打开',
      click: () => {
        win.show()
      }
    }, {
      label: '退出',
      click: () => {
        app.quit()
      }
    }
  ])
  tray.setToolTip('myApp')
  tray.on('click', () => {
    win.show()
  })
  tray.on('right-click', () => {
    tray.popUpContextMenu(trayContextMenu)
  })
  // 创建渲染窗口
  createWindow()
})
```

还有一点需要注意的是，我们之前的文章中，点击自定义的关闭图标，调用的方法是

```js
app.close()
```

这样会导致我们直接关闭程序，点击托盘也会报错，所以我们这儿需要改为

```js
app.hide()
```

好了，本篇文章的所有内容就讲完了，下一篇文档来教大家怎么打包应用和设置应用图标