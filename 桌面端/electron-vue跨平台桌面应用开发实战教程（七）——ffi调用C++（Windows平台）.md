# electron-vue跨平台桌面应用开发实战教程（七）——ffi调用C++（Windows平台）

 electron功能很强大，但是有一些跟操作系统底层交互的功能，electron无法实现，这个时候我们就可以调用原生来配合完成对应功能，本文主要讲解在windows平台下，调用C++的dll文件

在开始之前我们要安装

### 1.node-gyp

```js
npm install node-gyp -g
```

### 2.windows-build-tools

```js
npm install windows-build-tools -g
```

这里需要确定的是python的版本必须是2.7

使用ffi-napi调用dll（c++）

### 1. 安装ffi-napi

执行

```text
npm install ffi-napi --save
```

### 2. 准备C++动态链接库DLL文件

dll文件请去gitee中获取，这里没办法上传 [electron-vue-demos](https://link.zhihu.com/?target=https%3A//gitee.com/hedavid/electron-vue-demos)

### 3. 调用dll文件中的方法

```js
const ffi = require('ffi-napi')
const path = require('path')
const Dll = ffi.Library(path.resolve('resources/dll/MyDLL.dll'), {
  Add: ['float', ['float', 'float']],
  Hello: ['string', []],
  StrLength: ['int', ['string']]
})
```

这个dll一共提供了3个方法，

- 第一个方法是计算加和
- 第二个方法是返回‘Hello’ 字符串
- 第三个方法是计算字符串的长度

具体方法调用

```js
callCppDll () {
      console.log('fii.Library Hello result:', Dll.Hello())
      console.log('fii.Library Add result:', Dll.Add(1, 2))
      console.log('fii.Library Add result:', Dll.StrLength('hello world'))
    }
```

调用dll其实很简单，只是需要安装node-gyp和C++编译工具比较麻烦。本文是基于windows平台进行开发的，C++编译出来的文件是dll，如果在Mac平台上，需要将C++编译成dylib，接下来我来一个mac平台的