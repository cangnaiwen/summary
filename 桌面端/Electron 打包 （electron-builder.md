# Electron 打包 （electron-builder

本文只测试了Windows 10 下打包Windows基础安装包



## 依赖安装

```text
yarn add electron-builder --dev
```

> 注意，一定要加dev，不然无法打包

## 配置

在 pacakge.json 文件中的build中配置打包信息

> 注意：如需复制粘贴，请删除json文件中的注释

```json
{
  "name": "electron-demo",
  "version": "1.0.0",
  "author": "your name",
  "description": "My Electron app",
  "main": "main.js",
  "scripts": {
    "start": "electron .",
    "dist": "electron-builder"    // 添加打包命令
  },
  "build": {
    "productName": "electron-demo",   // 安装包文件名
    "directories": {
      "output": "dist"  // 安装包生成目录
    },
    "nsis": {
      "oneClick": false,  // 是否一键安装
      "allowToChangeInstallationDirectory": true    // 允许用户选择安装目录
    },
    "mac": {
      "category": "your.app.category.type"
    },
    "win": {
      "icon": "build/icons/food.png",   // 安装包图标，必须为 256 * 256 像素图片
      "target": [
        {
          "target": "nsis"
        }
      ]
    }
  },
  "devDependencies": {
    "electron": "12.0.5",
    "electron-builder": "^22.10.5"
  }
}
```

## 打包

执行如下打包命令

```text
yarn dist
```

然后就会发现在根目录下的dist目录下生成了exe安装包，可直接安装。