# Electron: 获取安装路径

有时候我们需要在程序中获取软件的安装路径（比如下载数据默认存放在安装目录下）

## 实现方法

```js
const { app } = require("electron").remote
const path = require ('path');

// 获取安装目录（也就是文件安装目录中exe文件的目录）
let homeDir =  path.dirname(app.getPath('exe'))
```