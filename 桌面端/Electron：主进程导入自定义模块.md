# Electron：主进程导入自定义模块

## 自定义模块

menu.js

```js
const { Menu } = require('electron')

const myMenu = {
  setMenu: win => {
    // 菜单设置
    let template = [
      {
        label: '用户',
        submenu: [
          {
            label: '退出登陆',
            click: () => {
              console.log("logout");
            }
          },
          { label: '切换用户' },
          {
            label: '控制台',
            click: () => {
              win.webContents.openDevTools({ mode: 'bottom' })
            }
          }
        ]
      }
    ]

    let m = Menu.buildFromTemplate(template)
    Menu.setApplicationMenu(m)

  }
}
module.exports = myMenu;
```

## 主进程导入

main.js

```js
const { app, BrowserWindow } = require('electron')
const path = require('path')

// 导入自定义的模块
const myMenu = require('./src/main/menu')
const myTray = require('./src/main/tray')
const myLoad = require('./src/main/load')

let win = null

const gotTheLock = app.requestSingleInstanceLock()

if (!gotTheLock) {
  app.quit()
} else {
  app.on('second-instance', (event, commandLine, workingDirectory) => {
    if (win) {
      if (win.isMinimized()) win.restore()
      win.focus()
    }
  })
  app.whenReady().then(() => {
    win = createWindow()
    // 使用导入的模块
    myMenu.setMenu(win)
    myLoad.loadFile(win)
    // myTray.setTray(win)
  
    app.on('activate', () => {
      if (BrowserWindow.getAllWindows().length === 0) {
        win = createWindow()
        myMenu.setMenu(win)
        myLoad.loadFile(win)
        // myTray.setTray(win)
      }
    })
  })

  app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
      app.quit()
    }
  })
}

function createWindow() {
  win = new BrowserWindow({
    width: 850,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, 'src', 'js', 'preload.js'),
      nodeIntegration: true,  
      contextIsolation: false,
      webviewTag: true,   
      enableRemoteModule: true 
    }
  })
  return win
}
```