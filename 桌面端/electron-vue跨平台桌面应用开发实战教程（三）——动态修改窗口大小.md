# electron-vue跨平台桌面应用开发实战教程（三）——动态修改窗口大小

前边两篇文字简单介绍了一下怎么搭建electron-vue项目，主进程的基本配置，主进程和渲染进程之间的通讯。本文我们主要来讲解下如何动态的修改窗口大小。

通常很多桌面应用，初次打开都需要登录，登录窗口比较小，登录成功之后展示一个更大的窗口，展示登录后的信息。例如QQ，钉钉，有道云笔记这些应用。

那么本文就来演示下如果做到这个功能，我们先做一下准备工作，我们会开发一个简单的小应用来给大家展示这个功能。

这里我们选用的技术为：

1. UI框架：element-ui
2. json数据库：lowdb

我们在第一篇文章的代码基础上，再安装这两个依赖

### 安装element-ui

```bash
npm i element-ui -S
```

### 安装lowdb

```bash
npm install lowdb
```

### 配置element-ui

修改main.js

```js
import Vue from 'vue';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import App from './App.vue';

Vue.use(ElementUI);

new Vue({
  el: '#app',
  render: h => h(App)
});
```

准备工作已经完成了，接下来就进入正式的开发了

### 1.修改窗口大小

通常登录窗口比较小，这个我们将登录窗口大小设置为宽：400，高：550 background.js

```js
win = new BrowserWindow({
    width: 400,
    height: 550,
    webPreferences: {
      nodeIntegration: true
    }
  })
```

### 2.绘制一个登录界面

我们再src/views 文件夹下新建Login.vue文件，给登录按钮加上点击事件，让他跳转到Home页。(增加了登录成功失败的小逻辑)

```js
<template>
  <div class="main">
    <div class="avatar">
      <el-avatar :size="60" src="https://cube.elemecdn.com/0/88/03b0d39583f48206768a7534e55bcpng.png"></el-avatar>
    </div>
    <div class="item">
      <el-input placeholder="请输入账号" v-model="account" clearable prefix-icon="el-icon-user"></el-input>
    </div>
    <div class="item">
      <el-input placeholder="请输入密码" v-model="password" show-password prefix-icon="el-icon-lock"></el-input>
    </div>
    <div class="item">
      <el-button type="primary" round @click="login">登录</el-button>
    </div>
  </div>
</template>
<script>
export default {
  name: 'Login',
  data () {
    return {
      account: '',
      password: ''
    }
  },
  methods: {
    login () {
      if (this.account === 'admin' && this.password === '123456') {
        this.$router.push('Home')
      } else {
        this.$message.error('用户名或密码错误')
      }
    }
  }
}
</script>
<style lang="stylus" scoped>
.main
  margin-left 30px
  margin-right 30px
.avatar
  margin-top 40px
  margin-bottom 40px
button
  width 100%
.item
  margin-top 20px
</style>
```

### 3.修改路由

src/router/index.js

```js
import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Login from '../views/Login.vue'
Vue.use(VueRouter)
const routes = [
  {
    path: '/Home',
    name: 'Home',
    component: Home
  },
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/about',
    name: 'About',
    component: () => import('../views/About.vue')
  }
]
const router = new VueRouter({
  routes
})
export default router
```

### 4.修改App.vue

```js
<template>
  <div id="app">
    <router-view/>
  </div>
</template>
<style lang="stylus">
#app
  font-family Avenir, Helvetica, Arial, sans-serif
  -webkit-font-smoothing antialiased
  -moz-osx-font-smoothing grayscale
  text-align center
  color #2c3e50
  margin-top 60px
</style>
```

### 5.修改Home.vue

进入Home页面后，我们要将窗口的大小，调整为正常窗口大小，我们设置宽：1050，高：700；通过第二篇文章，我们知道改变窗口大小是需要再主进程中才能操作，我们Home页面是渲染进程，所以我们这时候要用到进程间通讯。

主进程（background.js）增加以下代码

```js
import { app, protocol, BrowserWindow, ipcMain } from 'electron'
ipcMain.on('changWindowSize', e =>
  win.setSize(1050, 700)
)
```

Home.vue

```js
<template>
  <div class="home">
    <img alt="Vue logo" src="../assets/logo.png">
  </div>
</template>
<script>
const { ipcRenderer } = require('electron')
export default {
  name: 'Home',
  mounted () {
    this.changeWindowSize()
  },
  methods: {
    changeWindowSize () {
      ipcRenderer.send('changWindowSize')
    }
  }
}
</script>
```

动态修改窗口到这儿就讲完了，代码稍后会上传到gitee。下一篇我们讲解以下怎么去掉窗口自带的外边框，怎么自己实现最小化，最大化，关闭，还有新开一个窗口。