# electron-vue跨平台桌面应用开发实战教程（八）——edgejs调用C# dll



> ﻿本文来介绍下怎么使用electron-edge-js来调用C#动态链接库，由于是调用C#动态链接库，所以也只能在windows平台上使用，这一点需要注意



## **重要！！！2021年4月2日更新**

好多朋友在使用的时候，按照我的步骤去做了，但是还是报下边的错，这个问题是vue-cli-plugin-electron-builder 版本导致的，需要将版本号改为

```text
"vue-cli-plugin-electron-builder": "~1.4.5",
```

然后重新npm install就可以了

------



在开始之前，同样需要安装node-gyp和windows-build-tools，具体安装方法请参照上一篇

### 1.安装electron-edge-js

```bash
npm install electron-edge-js --save
```

### 2.准备C# dll文件

dll文件请去gitee中获取，这里没办法上传 [electron-vue-demos](https://link.zhihu.com/?target=https%3A//gitee.com/hedavid/electron-vue-demos)

### 3.调用dll中方法

因为是windows专属功能，这里我们要放到windows的判断中

```js
// 只在windows平台下加载
  edge = require('electron-edge-js')
  invoke = edge.func({
    assemblyFile: path.resolve('resources/dll/electronedge.dll'),
    typeName: 'electronedge.Class1',
    methodName: 'Invoke'
  })
```

具体调用方法

```js
if (process.platform === 'win32') {
        invoke('这是自定义字符串', function (err, val) {
          if (err) throw err
          console.log(val)
          this.$message({
            message: 'dll返回的内容为：' + val,
            type: 'success'
          })
        })
      } else {
        this.$notify.error({
          title: '错误',
          message: '此功能为windows专属功能，mac无法使用'
        })
      }
```

但是这个时候我们运行程序就会报这个错

![img](https://pic4.zhimg.com/80/v2-1f13c4f4f1ba44dc75bd3d550c3d0003_720w.webp)

这个时候我们需要做以下处理： 在vue.config.js文件中增加

```js
externals: ['electron-edge-js']
```

这个时候我们程序就运行正常了