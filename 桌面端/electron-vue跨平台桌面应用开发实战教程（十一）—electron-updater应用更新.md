# electron-vue跨平台桌面应用开发实战教程（十一）—electron-updater应用更新



> ﻿本文主要讲解electron如何执行使用electron-updater更新应用

### 1.安装electron-updater

```powershell
npm install electron-updater --save-dev
```

### 2.编写更新代码

```js
const { autoUpdater } = require('electron-updater')

ipcMain.on('checkForUpdate', e =>
  updateHandle()
)

// 检测更新，在你想要检查更新的时候执行，renderer事件触发后的操作自行编写
function updateHandle () {
  autoUpdater.checkForUpdates()
  const message = {
    error: '检查更新出错',
    checking: '正在检查更新……',
    updateAva: '检测到新版本，正在下载……',
    updateNotAva: '现在使用的就是最新版本，不用更新'
  }
  autoUpdater.setFeedURL('http://127.0.0.1/exe')
  // eslint-disable-next-line handle-callback-err
  autoUpdater.on('error', function (error) {
    sendUpdateError(JSON.stringify(error))
    sendUpdateMessage(message.error)
  })
  autoUpdater.on('checking-for-update', function () {
    sendUpdateMessage(message.checking)
  })
  autoUpdater.on('update-available', function (info) {
    console.log(info)
    sendUpdateMessage(message.updateAva)
  })
  autoUpdater.on('update-not-available', function (info) {
    console.log(info)
    sendUpdateMessage(message.updateNotAva)
  })

  // 更新下载进度事件
  autoUpdater.on('download-progress', function (progressObj) {
    console.log(progressObj.percent)
    win.webContents.send('downloadProgress', progressObj)
  })
  autoUpdater.on('update-downloaded', function (event, releaseNotes, releaseName, releaseDate, updateUrl, quitAndUpdate) {
    ipcMain.on('isUpdateNow', (e, arg) => {
      console.log(arg)
      console.log('开始更新')
      // some code here to handle event
      autoUpdater.quitAndInstall()
    })

    win.webContents.send('isUpdateNow')
  })
}

// 通过main进程发送事件给renderer进程，提示更新信息
function sendUpdateMessage (text) {
  win.webContents.send('message', text)
}
```

autoUpdater.checkForUpdates() 是启动更新检查

autoUpdater.setFeedURL('http://127.0.0.1/exe') 是用来配置当前的更新包再服务器的地址，这里我是用nginx搭了一个服务器。这里需要注意的是，打完包之后需要将打好包的exe和latest.yml一起放到服务器上。如果下载100之后提示更新失败，多半是exe和latest.yml的信息不匹配。

测试时直接修改package.json 中version字段即可

### 3.渲染进程中可以监听更新进度

手动检查更新

```js
ipcRenderer.send('checkForUpdate')
```

监听更新进度，再这里可以结合界面为用户提供下载进度的展示，progressObj.percent就是下载的百分比

```js
ipcRenderer.on('message', (event, text) => {
  console.log(text)
})
// 注意：“downloadProgress”事件可能存在无法触发的问题，只需要限制一下下载网速就好了
ipcRenderer.on('downloadProgress', (event, progressObj) => {
  console.log(progressObj.percent || 0)
})
ipcRenderer.on('isUpdateNow', () => {
  ipcRenderer.send('isUpdateNow')
})
```