# electron-vue跨平台桌面应用开发实战教程（七）——ffi调用C++（macOS平台）

electron功能很强大，但是有一些跟操作系统底层交互的功能，electron无法实现，这个时候我们就可以调用原生来配合完成对应功能，本文主要讲解在macOS平台下，调用C++的dylib文件

在开始之前我们要安装

### 1.node-gyp

```js
npm install node-gyp -g
```

使用ffi-napi调用dll（c++）

### 1. 安装ffi-napi

执行

```text
npm install ffi-napi --save
```

### 2. 准备C++动态链接库libdemo.dylib文件

libdemo.dylib文件可以去gitee中获取，也可以自己编译

```bash
gcc -g -shared  demo.cpp -o libdemo.dylib
```

[electron-vue-demos gitee 地址](https://link.zhihu.com/?target=https%3A//gitee.com/hedavid/electron-vue-demos)

### 3. 调用libdemo.dylib文件中的方法

这里dll是找的别的写好的，dylib是我自己写的

```js
let cpplib
if (process.platform === 'darwin') {
  // 在使用libdemo的时候最好自己编译下cpp，命令gcc -g -shared  demo.cpp -o libdemo.dylib
  let libPath
  // 在这儿需要判断下是开发环境还是打包环境，在mac上这两个环境用的路径不一样
  if (process.env.NODE_ENV === 'development') {
    libPath = path.resolve('resources/dll/libdemo')
  } else {
    libPath = path.join(__dirname, '..') + '/dll/libdemo'
  }

  cpplib = ffi.Library(libPath, {
    Add: ['double', ['double', 'double']]
  })
} else if (process.platform === 'win32') {
  cpplib = ffi.Library(path.resolve('resources/dll/MyDLL'), {
    Add: ['float', ['float', 'float']],
    Hello: ['string', []],
    StrLength: ['int', ['string']]
  })
}
```

这里需要注意的点就是mac获取dylib的路径，开发环境和打包环境的路径是不一样的，在这儿需要特殊处理下

调用方法

```js
// dll和dylib提供了不同的方法，所以在这儿不能通用，但是写了各自的调用方法
      if (process.platform === 'win32') {
        console.log('fii.Library Hello result:', cpplib.Hello())
        console.log('fii.Library Add result:', cpplib.Add(1, 2))
        console.log('fii.Library Add result:', cpplib.StrLength('hello world'))
      }
      if (process.platform === 'darwin') {
        console.log(cpplib.Add(12345, 54321))
      }
```