# Electron 右下角托盘显示

## 需求

当我们点击桌面程序右上角的关闭按钮，想让程序隐藏到右下角而不是直接退出程序，程序仍然后台运行。

## 分析

1. 添加右下角托盘显示
2. 当点击关闭按钮时隐藏主程序窗口

## 实现

main.js

```js
const { app, BrowserWindow, Tray, Menu } = require('electron')
const path = require('path')

let tray = null  // 在外面创建tray变量，防止被自动删除，导致图标自动消失
function createWindow () {
  const win = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js')
    }
  })

  win.loadFile('index.html')

  // 当点击关闭按钮
  win.on('close', (e) => {
    e.preventDefault();  // 阻止退出程序
    win.setSkipTaskbar(true)   // 取消任务栏显示
    win.hide();    // 隐藏主程序窗口
  })

  // 创建任务栏图标
  tray = new Tray(path.join(__dirname, 'icons', 'small.ico'))

  // 自定义托盘图标的内容菜单
  const contextMenu = Menu.buildFromTemplate([
    {
      // 点击退出菜单退出程序
      label: '退出', click: function () {
        console.log(123);
        win.destroy()
        app.quit()

      }
    }
  ])

  tray.setToolTip('demo')  // 设置鼠标指针在托盘图标上悬停时显示的文本
  tray.setContextMenu(contextMenu)  // 设置图标的内容菜单
  // 点击托盘图标，显示主窗口
  tray.on("click", () => {
    win.show();
  })
}

app.whenReady().then(() => {
  createWindow()

  app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow()
    }
  })
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})
```

这样就实现了一个简单的系统托盘区域的功能，当打开软件，在系统托盘区显示，点击关闭按钮，仍旧在后台运行，点击系统托盘图标显示窗口，右键托盘可退出程序。