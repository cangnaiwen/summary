# electron-vue跨平台桌面应用开发实战教程（二）——主进程常用配置



﻿在上一篇文章中我们搭建了一个electron和vue集成的项目，本文主要是对electron的background.js也就是主进程做下介绍，同时也介绍下渲染进程。

electron 主要分为主进程（background.js）、渲染进程（也就是vue开发的页面）和GPU进程等，但是我们主要使用的就是主进程和渲染进程，其他进程暂时不做深入研究。

主进程：

- 一个应用中有且只有一个主进程
- 创建窗口等所有系统事件都要在主进程中进行

渲染进程：

- 每创建一个web页面都会创建一个渲染进程
- 每个web页面运行在它自己的渲染进程中
- 每个渲染进程是独立的, 它只关心它所运行的页面

主进程的主要主要作用：

1. 创建渲染进程
2. 管理应用程序的生命周期
3. 与系统底层交互

在此处主要讲解可以在主进程中设置进行哪些最常用的配置配置。

### 1.设置渲染进程的大小、外观

```js
win = new BrowserWindow({
    width: 1200, // 设置窗口的宽
    height: 620, // 设置窗口的高
    webPreferences: {
      webSecurity: false, // 是否禁用浏览器的跨域安全特性
      nodeIntegration: true // 是否完整支持node
    }
  })
```

此处只设置了几个基本属性，更多属性请参考：[https://www.w3cschool.cn/electronmanual/electronmanual-browser-window.html](https://link.zhihu.com/?target=https%3A//www.w3cschool.cn/electronmanual/electronmanual-browser-window.html)

### 2.设置菜单

```js
function createMenu () {
  // darwin表示macOS，针对macOS的设置
  if (process.platform === 'darwin') {
    const template = [
      {
        label: 'App Demo',
        submenu: [
          {
            role: 'about'
          },
          {
            role: 'quit'
          }]
      }]
    const menu = Menu.buildFromTemplate(template)
    Menu.setApplicationMenu(menu)
  } else {
    // windows及linux系统
    Menu.setApplicationMenu(null)
  }
}
```

在这里要注意，MacOS和windows及Linux的处理是不一样的

### 3.当应用启动后（初始化完成）要做的一些事情

```js
app.on('ready', async () => {
  if (isDevelopment && !process.env.IS_TEST) {
    if (isDevelopment && !process.env.IS_TEST) {
      // Install Vue Devtools
      try {
        await installVueDevtools()
      } catch (e) {
        console.error('Vue Devtools failed to install:', e.toString())
      }
    }
  }
  globalShortcut.register('CommandOrControl+Shift+i', function () {
    win.webContents.openDevTools()
  })
  createWindow()
})
```

app模块的ready方法执行完之后就可以创建渲染进程了。该方法默认是如果是开发环境则自动安装VueDevTools方便开发者调试。同时也在全局注册了使用Ctrl + Shift + i 呼出VueDevTool，在设置完这些插件之后，再创建渲染进程。

在此处通常做一些应用初始化的工作，例如：提前加载一些数据，等到渲染进程渲染完页面之后直接调用，加快应用加载速度等。

### 4. 当应用所有窗口关闭要做的事情

```js
app.on('window-all-closed', () => {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})
```

当应用所有窗口关闭调用此方法，在此方法中可以增加释放资源，或者删除一些临时文件

### 5.与渲染进程进行通讯

官方有多种通讯方式，我们这里介绍最常用的一种ipcRenderer（渲染进程中使用的对象）和ipcMain（主进程中使用的对象）。比如渲染进程让主进程关闭当前窗口 渲染进程

```js
const { ipcRenderer } = require('electron')
ipcRenderer.send('close');
```

主进程

```js
import { ipcMain } from 'electron'
ipcMain.on('close', e => win.close());
```

本文先介绍常用配置，等到在实战项目中用到跟高级的用法，会在实战项目中说明。