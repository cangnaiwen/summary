# electron-vue跨平台桌面应用开发实战教程（一）——Hello World



Electron是一个基于Chromium和 Node.js，可以使用 HTML、CSS和JavaScript构建跨平台应用的技术框架，兼容 Mac、Windows 和 Linux。

在开始搭建项目之前，需要安装以下工具

1. node.js

### 1 创建项目

### 1.1 安装vue-cli

先查看是否已经安装了vue-cli，vue-cli的版本是什么

```text
vue -V
```

如果版本叫老，可以直接卸载，再安装最新版本

卸载

```text
npm uninstall vue-cli -g
```

安装

```text
install @vue/cli -g
```

### 1.2 使用vue-cli创建项目

选取一个项目存放的路径，然后开始创建项目 例如：

```text
vue create electron-vue-helloworld
```

这里将项目名定为：electron-vue-helloworld
输入完上述命令之后进入vue项目的创建过程。出现以下内容

```text
Vue CLI v3.8.4
? Please pick a preset: (Use arrow keys)
  default (babel, eslint) 
> Manually select features
```

第一个选项是 “default” 默认，只包含babel和eslint
第二个选项是 “Manually select features”自定义安装

选择自定义安装，进入下一步选择

```text
? Check the features needed for your project: (Press <space> to select, <a> to t
oggle all, <i> to invert selection)
❯◉ Babel
 ◯ TypeScript
 ◯ Progressive Web App (PWA) Support
 ◉ Router
 ◉ Vuex
 ◉ CSS Pre-processors
 ◉ Linter / Formatter
 ◯ Unit Testing
 ◯ E2E Testing
```

这里我们选择 1. babel（高级的语法转换为 低级的语法）
\2. Router（路由）
\3. Vuex（状态管理器）
\4. CSS Pre-processors（css预处理器）
\5. Linter / Formatter（代码风格、格式校验）

然后进入下一步

```text
? Use history mode for router? (Requires proper server setup for index fallback 
in production) (Y/n)  n
```

这一步是设置router是否使用history模式，这里我们选n，接着进入下一步

```text
? Pick a CSS pre-processor (PostCSS, Autoprefixer and CSS Modules are supported 
by default): (Use arrow keys)
  Sass/SCSS (with dart-sass) 
  Sass/SCSS (with node-sass) 
  Less 
❯ Stylus
```

这里是设置css预处理模块，在这里我要强调一下，不需要乱选，选择我们熟悉的一种，在这里我们选择 Stylus ，然后进入下一步

```text
? Pick a linter / formatter config: (Use arrow keys)
  ESLint with error prevention only 
  ESLint + Airbnb config 
❯ ESLint + Standard config 
  ESLint + Prettier
```

这一步是选择ESLint代码检查工具的配置，这里我们选择标准配置“ESLint + Standard config”，然后进入下一步

```text
? Pick additional lint features: (Press <space> to select, <a> to toggle all, <i
> to invert selection)
❯◉ Lint on save
 ◯ Lint and fix on commit
```

这一步是选择什么时候执行ESLint检查，这里我们选择保存时检查“Lint on save”,然后进入下一步

```text
? Where do you prefer placing config for Babel, PostCSS, ESLint, etc.? 
  In dedicated config files 
❯ In package.json
```

这一步是询问 babel, postcss, eslint这些配置是单独的配置文件还是放在package.json 文件中，这里我们选择“In package.json”，然后进入下一步

```text
? Save this as a preset for future projects? (y/N) N
```

这一步是询问时候以后创建项目是否也采用同样的配置，这里我们选N。到目前为止，vue项目是创建完成了，我们等待项目下载依赖包，等项目构建完毕我们开始集成electron

### 1.3 使用electron-builder集成electron

进入项目根目录（electron-vue-helloworld），然后执行下列命令：

```text
vue add electron-builder
```

这个时候会安装electron-builder的依赖，可能比较耗费时间，请大家耐心等待，安装完成后会出现以下选型：

```text
? Choose Electron Version (Use arrow keys)
  ^4.0.0 
  ^5.0.0 
❯ ^6.0.0
```

这一步是选择Electron的版本，我们这里选择最新版本6.0.0，等待安装完成即可。安装完成后会在src目录下生成background.js，并在package.json 文件中修main为"main": "background.js"

至此，所有的安装都已经完成了，接下来我们就可以运行程序看效果了。

### 1.4 运行程序

执行以下命令，运行程序

```text
npm run electron:serve
```

在启动的时候，会启动很久，并出现以下信息

```text
INFO  Launching Electron...
Failed to fetch extension, trying 4 more times
Failed to fetch extension, trying 3 more times
Failed to fetch extension, trying 2 more times
Failed to fetch extension, trying 1 more times
```

这是在安装vuejs devtools，由于网络问题，一直安装不上。重试5次之后就会自动跳过并且启动程序。

编译成功后，会自动弹出一个桌面程序，如下图所示



![img](https://pic1.zhimg.com/80/v2-b4225f26690798fca8093ac2b151bd14_720w.webp)



下一期我们将详细的讲一下background.js，里边的一些参数都是干什么用的。