# electron-vue跨平台桌面应用开发实战教程（九）——集成sqlite3



> ﻿本文主要讲解集成及使用sqlite3

在开始之前，同样需要安装node-gyp和windows-build-tools，具体安装方法请参照：[https://blog.csdn.net/David1025/article/details/104461723](https://link.zhihu.com/?target=https%3A//blog.csdn.net/David1025/article/details/104461723)

### 1. 安装sqlite3依赖

```bash
npm install sqlite3 --save
```

安装完成之后，需要再运行一下（否则会出现找不到sqlite3.node）

```bash
npm install
```

### 2.使用

```js
// src/renderer/utils/db.js
// 建表脚本，导出db对象供之后使用
import sq3 from 'sqlite3'
const pathUtil = require('../utils/pathUtil.js')
// import { docDir } from './settings';
// 将数据存至系统用户目录，防止用户误删程序
export const dbPath = pathUtil.getAppResourcePath('db/data.sqlite1')

const sqlite3 = sq3.verbose()
const db = new sqlite3.Database(dbPath)
// 初始化
db.serialize(() => {
  db.run('create table test(name varchar(15))', function () {
    db.run("insert into test values('hello,word')", function () {
      db.all('select * from test', function (err, res) {
        if (!err) {
          console.log(JSON.stringify(res))
        } else {
          console.log(err)
        }
      })
    })
  })
})
export default db
```

使用

```js
db.all('select * from test', function (err, res) {
  if (!err) {
    console.log(JSON.stringify(res))
  } else {
    console.log(err)
  }
})
```