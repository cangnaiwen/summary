# 如何在 axios 中设置 responseType 类型



在 axios 中，你可以使用 `responseType` 选项来设置[响应数据](https://www.zhihu.com/search?q=响应数据&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A2913102230})的类型。以下是一些常见的 `responseType` 类型及其用法示例：

1. 默认值：`'json'`，会自动解析响应数据并返回一个 JavaScript 对象。

```js
axios.get('/api/data').then(response => {
  console.log(response.data); // 返回解析后的 JavaScript 对象
});
```

2.`'arraybuffer'`：返回一个 ArrayBuffer 对象，适用于处理[二进制](https://www.zhihu.com/search?q=二进制&search_source=Entity&hybrid_search_source=Entity&hybrid_search_extra={"sourceType"%3A"answer"%2C"sourceId"%3A2913102230})数据。

```js
axios.get('/api/data', {
  responseType: 'arraybuffer'
}).then(response => {
  console.log(response.data); // 返回 ArrayBuffer 对象
});
```

3.`'blob'`：返回一个 Blob 对象，适用于处理图像等二进制数据。

```js
axios.get('/api/image', {
  responseType: 'blob'
}).then(response => {
  console.log(response.data); // 返回 Blob 对象
});
```

4.`'document'`：返回一个 Document 对象，适用于处理 HTML/XML 数据。

```js
axios.get('/api/data', {
  responseType: 'document'
}).then(response => {
  console.log(response.data); // 返回 Document 对象
});
```

5.`'text'`：返回一个字符串，适用于处理纯文本数据。

```js
axios.get('/api/data', {
  responseType: 'text'
}).then(response => {
  console.log(response.data); // 返回字符串
});
```

你可以根据你的需求选择不同的 `responseType` 类型，以便更好地处理响应数据。