# 透过分析 webpack 面试题，构建 webpack5.x 知识体系<进阶篇>

我们将向“能优化”的方向前进 ，除了配置上的优化外，我们也要学习如何自己开发 Loader 和 Plugin

基础篇请查看：[葡萄zi：透过分析 webpack 面试题，构建 webpack5.x 知识体系<基础篇>](https://zhuanlan.zhihu.com/p/454926184)

## 优化构建速度

### 构建费时分析

这里我们需要使用插件 [speed-measure-webpack-plugin](https://link.zhihu.com/?target=https%3A//link.juejin.cn/%3Ftarget%3Dhttps%3A%2F%2Fwww.npmjs.com%2Fpackage%2Fspeed-measure-webpack-plugin)，我们参考文档配置一下

1、首先安装一下

```abap
$ npm i -D speed-measure-webpack-plugin
```

2、修改我们的配置文件 webpack.config.js

```js
...
// 费时分析
const SpeedMeasurePlugin = require("speed-measure-webpack-plugin");
const smp = new SpeedMeasurePlugin();
...

const config = {...}

module.exports = (env, argv) => {
  // 这里可以通过不同的模式修改 config 配置


  return smp.wrap(config);
}
```

3、执行打包

![img](https://pic2.zhimg.com/80/v2-d65e2a4e6cea518519dc323ef702786d_1440w.webp)

报错了 ‍♂️，这里就暴露了使用这个插件的一个弊端，就是：**有些 Loader 或者 Plugin 新版本会不兼容，需要进行降级处理。**这里我们对 `mini-css-extract-plugin` 进行一下降级处理: `^2.1.0` -&a`mp;amp;gt; ^1.3.6`，重新安装一下依赖，再次执行打包

![img](https://pic3.zhimg.com/80/v2-1a8519958237cda5f74e011c953fab66_1440w.webp)

降了版本之后，还是报错，根据提示信息，我们给配置加上 `publicPath: './'`

```js
output: {
    filename: 'bundle.js', // 输出文件名
    path: path.join(__dirname, 'dist'), // 输出文件目录
    publicPath: './'
  },
```

在尝试一次

![img](https://pic1.zhimg.com/80/v2-4f10eb634ef92c1c66c75f9212d1cf38_1440w.webp)

成功了！

> **注意：在 webpack5.x 中为了使用费时分析去对插件进行降级或者修改配置写法是非常不划算的**，这里因为演示需要，我后面会继续使用，但是在平时开发中，建议还是不要使用。

### 优化 resolve 配置

**1、alias**

alias 用的创建 `import` 或 `require` 的别名，用来简化模块引用，项目中基本都需要进行配置。

```js
const path = require('path')
...
// 路径处理方法
function resolve(dir){
  return path.join(__dirname, dir);
}

 const config  = {
  ...
  resolve:{
    // 配置别名
    alias: {
      '~': resolve('src'),
      '@': resolve('src'),
      'components': resolve('src/components'),
    }
  }
};
```

配置完成之后，我们在项目中就可以

```abap
// 使用 src 别名 ~ 
import '~/fonts/iconfont.css'

// 使用 src 别名 @ 
import '@/fonts/iconfont.css'

// 使用 components 别名
import footer from "components/footer";
```

**2、extensions**

webpack 默认配置

```js
const config = {
  //...
  resolve: {
    extensions: ['.js', '.json', '.wasm'],
  },
};
```

如果用户引入模块时不带扩展名，例如

```abap
import file from '../path/to/file';
```

那么 webpack 就会按照 extensions 配置的数组从左到右的顺序去尝试解析模块

需要注意的是：

1. 高频文件后缀名放前面；
2. 手动配置后，默认配置会被覆盖

如果想保留默认配置，可以用 `...` 扩展运算符代表默认配置，例如

```abap
const config = {
  //...
  resolve: {
    extensions: ['.ts', '...'], 
  },
};
```

**3、modules**

告诉 webpack 解析模块时应该搜索的目录，常见配置如下

```js
const path = require('path');

// 路径处理方法
function resolve(dir){
  return path.join(__dirname, dir);
}

const config = {
  //...
  resolve: {
     modules: [resolve('src'), 'node_modules'],
  },
};
```

告诉 webpack 优先 src 目录下查找需要解析的文件，会大大节省查找时间

**4、resolveLoader**

resolveLoader 与上面的 resolve 对象的属性集合相同， 但仅用于解析 webpack 的 [loader](https://link.zhihu.com/?target=https%3A//link.juejin.cn/%3Ftarget%3Dhttps%3A%2F%2Fwebpack.docschina.org%2Fconcepts%2Floaders) 包。

**一般情况下保持默认配置就可以了，但如果你有自定义的 Loader 就需要配置一下**，不配可能会因为找不到 loader 报错。例如：我们在 loader 文件夹下面，放着我们自己写的 loader。我们就可以怎么配置

```js
const path = require('path');

// 路径处理方法
function resolve(dir){
  return path.join(__dirname, dir);
}

const config = {
  //...
  resolveLoader: {
    modules: ['node_modules',resolve('loader')]
  },
};
```

**5、externals**

`externals` 配置选项提供了「**从输出的 bundle 中排除依赖**」的方法。此功能通常对 **library 开发人员**来说是最有用的，然而也会有各种各样的应用程序用到它。例如，从 CDN 引入 jQuery，而不是把它打包：

1、引入链接

```js
<script
  src="https://code.jquery.com/jquery-3.1.0.js"
  integrity="sha256-slogkvB1K3VOkzAI8QITxV3VzpOnkeNVsKvtkYLMjfk="
  crossorigin="anonymous"
></script>
```

2、配置 externals

```js
const config = {
  //...
  externals: {
    jquery: 'jQuery',
  },
};
```

3、使用 jQuery

```abap
import $ from 'jquery';

$('.my-element').animate(/* ... */);
```

我们可以用这样的方法来剥离不需要改动的一些依赖，大大节省打包构建的时间。

### 缩小范围

在配置 loader 的时候，我们需要更精确的去指定 loader 的作用目录或者需要排除的目录，通过使用 `include` 和 `exclude` 两个配置项，可以实现这个功能，常见的例如：

- **`include`**：符合条件的模块进行解析
- **`exclude`**：排除符合条件的模块，不解析
- **`exclude`** 优先级更高

例如在配置 babel 的时候

```js
const path = require('path');

// 路径处理方法
function resolve(dir){
  return path.join(__dirname, dir);
}

const config = {
  //...
  module: { 
    noParse: /jquery|lodash/,
    rules: [
      {
        test: /\.js$/i,
        include: resolve('src'),
        exclude: /node_modules/,
        use: [
          'babel-loader',
        ]
      },
      // ...
    ]
  }
};
```

### noParse

- 不需要解析依赖的第三方大型类库等，可以通过这个字段进行配置，以提高构建速度
- 使用 noParse 进行忽略的模块文件中不会解析 `import`、`require` 等语法

```js
const config = {
  //...
  module: { 
    noParse: /jquery|lodash/,
    rules:[...]
  }
};
```

### IgnorePlugin

防止在 `import` 或 `require` 调用时，生成以下正则表达式匹配的模块：

- `requestRegExp` 匹配(test)资源请求路径的正则表达式。
- `contextRegExp` 匹配(test)资源上下文（目录）的正则表达式

```abap
new webpack.IgnorePlugin({ resourceRegExp, contextRegExp });
```

以下示例演示了此插件的几种用法。

1、安装 moment 插件（时间处理库）

```abap
$ npm i -S moment
```

2、配置 IgnorePlugin

```abap
// 引入 webpack
const webpack = require('webpack')

const config = {
  ...
  plugins:[ // 配置插件
    ...
    new webpack.IgnorePlugin({
      resourceRegExp: /^\.\/locale$/,
      contextRegExp: /moment$/,
    }),
  ]  
};
```

目的是将插件中的非中文语音排除掉，这样就可以大大节省打包的体积了

### 多进程配置

> **注意**：实际上在小型项目中，开启多进程打包反而会增加时间成本，因为启动进程和进程间通信都会有一定开销。

**1、thread-loader**

配置在 [thread-loader](https://link.zhihu.com/?target=https%3A//link.juejin.cn/%3Ftarget%3Dhttps%3A%2F%2Fwebpack.docschina.org%2Floaders%2Fthread-loader%2F%23root) 之后的 loader 都会在一个单独的 worker 池（worker pool）中运行

1、安装

```text
$ npm i -D  thread-loader
```

2、配置

```abap
const path = require('path');

// 路径处理方法
function resolve(dir){
  return path.join(__dirname, dir);
}

const config = {
  //...
  module: { 
    noParse: /jquery|lodash/,
    rules: [
      {
        test: /\.js$/i,
        include: resolve('src'),
        exclude: /node_modules/,
        use: [
          {
            loader: 'thread-loader', // 开启多进程打包
            options: {
              worker: 3,
            }
          },
          'babel-loader',
        ]
      },
      // ...
    ]
  }
};
```

**2、 happypack ❌**

同样为开启多进程打包的工具，webpack5 已弃用。

### 利用缓存

利用缓存可以大幅提升重复构建的速度

**babel-loader 开启缓存**

- babel 在转译 js 过程中时间开销比价大，将 babel-loader 的执行结果缓存起来，重新打包的时候，直接读取缓存
- 缓存位置： `node_modules/.cache/babel-loader`

具体配置如下：

```abap
const config = {
 module: { 
    noParse: /jquery|lodash/,
    rules: [
      {
        test: /\.js$/i,
        include: resolve('src'),
        exclude: /node_modules/,
        use: [
          // ...
          {
            loader: 'babel-loader',
            options: {
              cacheDirectory: true // 启用缓存
            }
          },
        ]
      },
      // ...
    ]
  }
}
```

那其他的 loader 如何将结果缓存呢？[cache-loader](https://link.zhihu.com/?target=https%3A//link.juejin.cn/%3Ftarget%3Dhttps%3A%2F%2Fwww.npmjs.com%2Fpackage%2Fcache-loader) 就可以帮我们完成这件事情

### cache-loader

- 缓存一些性能开销比较大的 loader 的处理结果
- 缓存位置：`node_modules/.cache/cache-loader`

1、安装

```abap
$ npm i -D cache-loader
```

2、配置 cache-loader

```abap
const config = {
 module: { 
    // ...
    rules: [
      {
        test: /\.(s[ac]|c)ss$/i, //匹配所有的 sass/scss/css 文件
        use: [
          // 'style-loader',
          MiniCssExtractPlugin.loader,
          'cache-loader', // 获取前面 loader 转换的结果
          'css-loader',
          'postcss-loader',
          'sass-loader', 
        ]
      }, 
      // ...
    ]
  }
}
```

**hard-source-webpack-plugin**

[hard-source-webpack-plugin](https://link.zhihu.com/?target=https%3A//link.juejin.cn/%3Ftarget%3Dhttps%3A%2F%2Fgithub.com%2Fmzgoddard%2Fhard-source-webpack-plugin) 为模块提供了中间缓存，重复构建时间大约可以减少 80%，但是在 **webpack5 中已经内置了模块缓存，不需要再使用此插件**

**dll ❌**

在 webpack5.x 中已经不建议使用这种方式进行模块缓存，因为其已经内置了更好体验的 cache 方法

**cache 持久化缓存**

通过配置[cache](https://link.zhihu.com/?target=https%3A//link.juejin.cn/%3Ftarget%3Dhttps%3A%2F%2Fwebpack.docschina.org%2Fconfiguration%2Fcache%2F%23root)缓存生成的 webpack 模块和 chunk，来改善构建速度。

```abap
const config = {
  cache: {
    type: 'filesystem',
  },
};
```

## 优化构建结果

### 构建结果分析

借助插件[webpack-bundle-analyzer](https://link.zhihu.com/?target=https%3A//link.juejin.cn/%3Ftarget%3Dhttps%3A%2F%2Fwww.npmjs.com%2Fpackage%2Fwebpack-bundle-analyzer)我们可以直观的看到打包结果中，文件的体积大小、各模块依赖关系、文件是够重复等问题，极大的方便我们在进行项目优化的时候，进行问题诊断。

1、安装

```abap
$ npm i -D webpack-bundle-analyzer
```

2、配置插件

```abap
// 引入插件
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

const config = {
  // ...
  plugins:[ 
    // ...
    // 配置插件 
    new BundleAnalyzerPlugin({
      // analyzerMode: 'disabled',  // 不启动展示打包报告的http服务器
      // generateStatsFile: true, // 是否生成stats.json文件
    })
  ],
};
```

3、修改启动命令

```text
 "scripts": {
    // ...
    "analyzer": "cross-env NODE_ENV=prod webpack --progress --mode production"
  },
```

4、执行编译命令 `npm run analyzer`

打包结束后，会自行启动地址为 `http://127.0.0.1:8888` 的 web 服务，访问地址就可以看到

![img](https://pic2.zhimg.com/80/v2-4ea2f3b71f57d680e71dd27f703e3ff1_1440w.webp)

如果，我们只想保留数据不想启动 web 服务，这个时候，我们可以加上两个配置

```abap
new BundleAnalyzerPlugin({
   analyzerMode: 'disabled',  // 不启动展示打包报告的http服务器
   generateStatsFile: true, // 是否生成stats.json文件
})
```

这样再次执行打包的时候就只会产生 state.json 的文件了

### 压缩 CSS

1、安装 `optimize-css-assets-webpack-plugin`

```abap
$ npm install -D optimize-css-assets-webpack-plugin 
```

2、修改 `webapck.config.js` 配置

```abap
// ...
// 压缩css
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')
// ...

const config = {
  // ...
  optimization: {
    minimize: true,
    minimizer: [
      // 添加 css 压缩配置
      new OptimizeCssAssetsPlugin({}),
    ]
  },
 // ...
}

// ...
```

3、查看打包结果

![img](https://pic4.zhimg.com/80/v2-6c0c130c06f345799ca78c6e0f8a1863_1440w.webp)

### 压缩 JS

> 在生成环境下打包默认会开启 js 压缩，但是当我们手动配置`optimization`选项之后，就不再默认对 js 进行压缩，需要我们手动去配置。

因为 webpack5 内置了[terser-webpack-plugin](https://link.zhihu.com/?target=https%3A//link.juejin.cn/%3Ftarget%3Dhttps%3A%2F%2Fwww.npmjs.com%2Fpackage%2Fterser-webpack-plugin)插件，所以我们不需重复安装，直接引用就可以了，具体配置如下

```abap
const TerserPlugin = require('terser-webpack-plugin');

const config = {
  // ...
  optimization: {
    minimize: true, // 开启最小化
    minimizer: [
      // ...
      new TerserPlugin({})
    ]
  },
  // ...
}
```

### 清除无用的 CSS

[purgecss-webpack-plugin](https://link.zhihu.com/?target=https%3A//link.juejin.cn/%3Ftarget%3Dhttps%3A%2F%2Fwww.purgecss.cn%2Fplugins%2Fwebpack.html%23%25E7%2594%25A8%25E6%25B3%2595) 会单独提取 CSS 并清除用不到的 CSS

1、安装插件

```abap
$ npm i -D purgecss-webpack-plugin
```

2、添加配置

```abap
// ...
const PurgecssWebpackPlugin = require('purgecss-webpack-plugin')
const glob = require('glob'); // 文件匹配模式
// ...

function resolve(dir){
  return path.join(__dirname, dir);
}

const PATHS = {
  src: resolve('src')
}

const config = {
  plugins:[ // 配置插件
    // ...
    new PurgecssPlugin({
      paths: glob.sync(`${PATHS.src}/**/*`, {nodir: true})
    }),
  ]
}
```

3、index.html 新增节点

```abap
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>ITEM</title>
</head>
<body>
  <p></p>
  <!-- 使用字体图标文件 -->
  <i class="iconfont icon-member"></i>
  <div id="imgBox"></div>
  
   <!-- 新增 div，设置 class 为 used -->
  <div class="used"></div>
</body>
</html>
```

4、在 sass.scss 中添加样式

```abap
.used {
  width: 200px;
  height: 200px;
  background: #ccc;
}

.unused {
  background: chocolate;
}
```

5、执行一下打包

![img](https://pic4.zhimg.com/80/v2-07eaa7254c1a4b0df3f7194452a0e023_1440w.webp)

我们可以看到只有 `.used` 被保存下来

如何证明是这个插件的作用呢？注释掉再打包就可以看到，`.unused` 也会被打包进去，由此可证...

### Tree-shaking

Tree-shaking 作用是剔除没有使用的代码，以降低包的体积

**webpack 默认支持，需要在 .bablerc 里面设置 `model：false`，即可在生产环境下默认开启**

了解更多 Tree-shaking 知识，推荐阅读 [从过去到现在，聊聊 Tree-shaking](https://link.zhihu.com/?target=https%3A//link.juejin.cn/%3Ftarget%3Dhttps%3A%2F%2Fmp.weixin.qq.com%2Fs%2FTNXO2ifPymaTxIqzBAmkSQ)

```abap
module.exports = {
  presets: [
    [
      "@babel/preset-env",
      {
        module: false,
        useBuiltIns: "entry",
        corejs: "3.9.1",
        targets: {
          chrome: "58",
          ie: "11",
        },
      },
    ],
  ],
  plugins: [    
    ["@babel/plugin-proposal-decorators", { legacy: true }],
    ["@babel/plugin-proposal-class-properties", { loose: true }],
  ]
};
```

### Scope Hoisting

Scope Hoisting 即作用域提升，原理是将多个模块放在同一个作用域下，并重命名防止命名冲突，**通过这种方式可以减少函数声明和内存开销**。

- webpack 默认支持，在生产环境下默认开启
- 只支持 es6 代码

## 优化运行时体验

运行时优化的核心就是提升首屏的加载速度，主要的方式就是：**降低首屏加载文件体积，首屏不需要的文件进行预加载或者按需加载**

### 入口点分割

配置多个打包入口，多页打包，这里不过多介绍

### splitChunks 分包配置

optimization.splitChunks 是基于 [SplitChunksPlugin](https://link.zhihu.com/?target=https%3A//link.juejin.cn/%3Ftarget%3Dhttps%3A%2F%2Fwebpack.docschina.org%2Fplugins%2Fsplit-chunks-plugin%2F) 插件实现的。默认情况下，它只会影响到按需加载的 chunks，因为修改 initial chunks 会影响到项目的 HTML 文件中的脚本标签。

webpack 将根据以下条件自动拆分 chunks：

- 新的 chunk 可以被共享，或者模块来自于 `node_modules` 文件夹
- 新的 chunk 体积大于 20kb（在进行 min+gz 之前的体积）
- 当按需加载 chunks 时，并行请求的最大数量小于或等于 30
- 当加载初始化页面时，并发请求的最大数量小于或等于 30

1、默认配置介绍

```abap
module.exports = {
  //...
  optimization: {
    splitChunks: {
      chunks: 'async', // 有效值为 `all`，`async` 和 `initial`
      minSize: 20000, // 生成 chunk 的最小体积（≈ 20kb)
      minRemainingSize: 0, // 确保拆分后剩余的最小 chunk 体积超过限制来避免大小为零的模块
      minChunks: 1, // 拆分前必须共享模块的最小 chunks 数。
      maxAsyncRequests: 30, // 最大的按需(异步)加载次数
      maxInitialRequests: 30, // 打包后的入口文件加载时，还能同时加载js文件的数量（包括入口文件）
      enforceSizeThreshold: 50000,
      cacheGroups: { // 配置提取模块的方案
        defaultVendors: {
          test: /[\/]node_modules[\/]/,
          priority: -10,
          reuseExistingChunk: true,
        },
        default: {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true,
        },
      },
    },
  },
};
```

2、项目中的使用

```abap
const config = {
  //...
  optimization: {
    splitChunks: {
      cacheGroups: { // 配置提取模块的方案
        default: false,
        styles: {
            name: 'styles',
            test: /\.(s?css|less|sass)$/,
            chunks: 'all',
            enforce: true,
            priority: 10,
          },
          common: {
            name: 'chunk-common',
            chunks: 'all',
            minChunks: 2,
            maxInitialRequests: 5,
            minSize: 0,
            priority: 1,
            enforce: true,
            reuseExistingChunk: true,
          },
          vendors: {
            name: 'chunk-vendors',
            test: /[\\/]node_modules[\\/]/,
            chunks: 'all',
            priority: 2,
            enforce: true,
            reuseExistingChunk: true,
          },
         // ... 根据不同项目再细化拆分内容
      },
    },
  },
}
```

### 代码懒加载

针对首屏加载不太需要的一些资源，我们可以通过懒加载的方式去实现，下面看一个小 。

需求：点击图片给图片加一个描述

1、新建图片描述信息 **desc.js**

```abap
const ele = document.createElement('div')
ele.innerHTML = '我是图片描述'
module.exports = ele
```

2、点击图片引入描述 **index.js**

```abap
import './main.css';
import './sass.scss'
import logo from '../public/avatar.png'

import '@/fonts/iconfont.css'

const a = 'Hello ITEM'
console.log(a)

const img = new Image()
img.src = logo

document.getElementById('imgBox').appendChild(img)

// 按需加载
img.addEventListener('click', () => {
  import('./desc').then(({ default: element }) => {
    console.log(element)
    document.body.appendChild(element)
  })
})
```

点击前

![img](https://pic3.zhimg.com/80/v2-b3c3b10dbada8e5cc7566264413df912_1440w.webp)

![img](https://pic1.zhimg.com/80/v2-3d0df8ec35b5db185b1b66a2abfb6618_1440w.webp)

点击后

![img](https://pic1.zhimg.com/80/v2-c47fe2155132bac055627e436e335100_1440w.webp)

![img](https://pic4.zhimg.com/80/v2-e5a12974ab8cd077d1fa0339f99c8b3b_1440w.webp)

### prefetch 与 preload

上面我们使用异步加载的方式引入图片的描述，但是如果需要异步加载的文件比较大时，在点击的时候去加载也会影响到我们的体验，这个时候我们就可以考虑使用 prefetch 来进行预拉取

**prefetch**

> **prefetch** (预获取)：浏览器空闲的时候进行资源的拉取

改造一下上面的代码

```abap
// 按需加载
img.addEventListener('click', () => {
  import( /* webpackPrefetch: true */ './desc').then(({ default: element }) => {
    console.log(element)
    document.body.appendChild(element)
  })
})
```

**preload**

- **preload** (预加载)：提前加载后面会用到的关键资源
- ⚠️ 因为会提前拉取资源，如果不是特殊需要，谨慎使用

官网示例：

```abap
import(/* webpackPreload: true */ 'ChartingLibrary');
```