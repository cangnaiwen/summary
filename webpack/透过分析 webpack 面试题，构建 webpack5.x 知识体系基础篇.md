# 透过分析 webpack 面试题，构建 webpack5.x 知识体系<基础篇>

本文介绍 Webpack 基础篇， 让你学会配置， 先了解 Webpack 简单配置以及简单配置会涉及到的面试题。

[葡萄zi：透过分析 webpack 面试题，构建 webpack5.x 知识体系<进阶篇>](https://zhuanlan.zhihu.com/p/454945287)

## 1、简单配置

该部分需要掌握：

1. Webpack 常规配置项有哪些？
2. 常用 Loader 有哪些？如何配置？
3. 常用插件（Plugin）有哪些？如何的配置？
4. Babel 的如何配置？Babel 插件如何使用？

### **1.1 安装依赖**

毫无疑问，先本地安装一下 `webpack` 以及 `webpack-cli`

```bash
$ npm install webpack webpack-cli -D # 安装到本地依赖
```

安装完成 ✅

```abap
+ webpack-cli@4.7.2
+ webpack@5.44.0
```

### **1.2 工作模式**

webpack 在 4 以后就支持 0 配置打包，我们可以测试一下

1、新建 `./src/index.js` 文件，写一段简单的代码

```js
const a = 'Hello ITEM'
console.log(a)
module.exports = a;
```

此时目录结构

```js
webpack_work                  
├─ src                
│  └─ index.js         
└─ package.json       
```

2、直接运行 `npx webpack`，启动打包

![img](https://pic1.zhimg.com/80/v2-228b99400e14c492b3749d112e0a4fd0_1440w.webp)

打包完成，我们看到日志上面有一段提示：`The 'mode' option has not been set,...`

意思就是，我们没有配置 mode（模式），这里提醒我们配置一下

> **模式：**供 mode 配置选项，告知 webpack 使用相应模式的内置优化，默认值为`production`，另外还有`development`、`none`，他们的区别如下

| development | 开发模式，打包更加快速，省了代码优化步骤              |
| ----------- | ----------------------------------------------------- |
| production  | 生产模式，打包比较慢，会开启 tree-shaking 和 压缩代码 |
| none        | 不使用任何默认优化选项                                |

怎么配置呢？很简单

1、只需在配置对象中提供 mode 选项：

```js
module.exports = {
  mode: 'development',
};
```

2、从 CLI 参数中传递：

```bash
$ webpack --mode=development
```

### **1.3 配置文件**

虽然有 0 配置打包，但是实际工作中，我们还是需要使用配置文件的方式，来满足不同项目的需求

1. 根路径下新建一个配置文件 `webpack.config.js`
2. 新增基本配置信息

```js
const path = require('path')

module.exports = {
  mode: 'development', // 模式
  entry: './src/index.js', // 打包入口地址
  output: {
    filename: 'bundle.js', // 输出文件名
    path: path.join(__dirname, 'dist') // 输出文件目录
  }
}
```

这个就不过多说了，最基本的配置

### **1.4 Loader**

这里我们把入口改成 CSS 文件，可能打包结果会如何

1、新增 `./src/main.css`

```css
body {
  margin: 0 auto;
  padding: 0 20px;
  max-width: 800px;
  background: #f4f8fb;
}
```

2、修改 entry 配置

```js
const path = require('path')

module.exports = {
  mode: 'development', // 模式
  entry: './src/main.css', // 打包入口地址
  output: {
    filename: 'bundle.css', // 输出文件名
    path: path.join(__dirname, 'dist') // 输出文件目录
  }
}
```

3、运行打包命令：`npx webpack`

![img](https://pic2.zhimg.com/80/v2-4e1244c4ce212dfbcc75533b82a65859_1440w.webp)

这里就报错了！

这是因为：**webpack 默认支持处理 JS 与 JSON 文件，其他类型都处理不了，这里必须借助 Loader 来对不同类型的文件的进行处理。**

4、安装 `css-loader` 来处理 CSS

```text
npn install css-loader -D
```

5、配置资源加载模块

```js
const path = require('path')

module.exports = {
  mode: 'development', // 模式
  entry: './src/main.css', // 打包入口地址
  output: {
    filename: 'bundle.css', // 输出文件名
    path: path.join(__dirname, 'dist') // 输出文件目录
  },
  module: { 
    rules: [ // 转换规则
      {
        test: /\.css$/, //匹配所有的 css 文件
        use: 'css-loader' // use: 对应的 Loader 名称
      }
    ]
  }
}
```

6、重新运行打包命令 `npx webpack`

![img](https://pic4.zhimg.com/80/v2-4ee61aa5bcc6fc4c88bfa52f16591673_1440w.webp)

哎嘿，可以打包了

```text
dist           
└─ bundle.css  # 打包得到的结果 
```

![img](https://pic2.zhimg.com/80/v2-ae79ce36a0e974592eed308d870e2075_1440w.webp)

这里这是尝试，入口文件还是需要改回 `./src/index.js`

这里我们可以得到一个结论：**Loader 就是将 Webpack 不认识的内容转化为认识的内容**

### **1.5 插件（plugin）**

与 Loader 用于转换特定类型的文件不同，**插件（Plugin）可以贯穿 Webpack 打包的生命周期，执行不同的任务**

下面来看一个使用的列子：

1、新建`./src/index.html`文件

```js
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>ITEM</title>
</head>
<body>
  
</body>
</html>
```

如果我想打包后的资源文件，例如：js 或者 css 文件可以自动引入到 Html 中，就需要使用插件 [html-webpack-plugin](https://link.zhihu.com/?target=https%3A//link.juejin.cn/%3Ftarget%3Dhttps%3A%2F%2Fwww.npmjs.com%2Fpackage%2Fhtml-webpack-plugin)来帮助你完成这个操作

2、本地安装`html-webpack-plugin`

```js
npm install html-webpack-plugin -D 
```

3、配置插件

```js
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

module.exports = {
  mode: 'development', // 模式
  entry: './src/index.js', // 打包入口地址
  output: {
    filename: 'bundle.js', // 输出文件名
    path: path.join(__dirname, 'dist') // 输出文件目录
  },
  module: { 
    rules: [
      {
        test: /\.css$/, //匹配所有的 css 文件
        use: 'css-loader' // use: 对应的 Loader 名称
      }
    ]
  },
  plugins:[ // 配置插件
    new HtmlWebpackPlugin({
      template: './src/index.html'
    })
  ]
}
```

运行一下打包，打开 dist 目录下生成的 index.html 文件

```js
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>ITEM</title>
<script defer src="bundle.js"></script></head>
<body>
  
</body>
</html>
```

可以看到它自动的引入了打包好的 bundle.js ，非常方便实用

### **1.6 自动清空打包目录**

每次打包的时候，打包目录都会遗留上次打包的文件，为了保持打包目录的纯净，我们需要在打包前将打包目录清空

这里我们可以使用插件 [clean-webpack-plugin](https://link.zhihu.com/?target=https%3A//link.juejin.cn/%3Ftarget%3Dhttps%3A%2F%2Fwww.npmjs.com%2Fpackage%2Fclean-webpack-plugin) 来实现

1、安装

```js
$ npm install clean-webpack-plugin -D
```

2、配置

```js
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
// 引入插件
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

module.exports = {
  // ...
  plugins:[ // 配置插件
    new HtmlWebpackPlugin({
      template: './src/index.html'
    }),
    new CleanWebpackPlugin() // 引入插件
  ]
}
```

### **1.7 区分环境**

本地开发和部署线上，肯定是有不同的需求

**本地环境：**

- 需要更快的构建速度
- 需要打印 debug 信息
- 需要 live reload 或 hot reload 功能
- 需要 sourcemap 方便定位问题
- ...

**生产环境：**

- 需要更小的包体积，代码压缩+tree-shaking
- 需要进行代码分割
- 需要压缩图片体积
- ...

针对不同的需求，首先要做的就是做好环境的区分

1、本地安装 cross-env [[文档地址](https://link.zhihu.com/?target=https%3A//link.juejin.cn/%3Ftarget%3Dhttps%3A%2F%2Fwww.npmjs.com%2Fpackage%2Fcross-env)]

```js
npm install cross-env -D
```

2、配置启动命令

打开 `./package.json`

```bash
"scripts": {
    "dev": "cross-env NODE_ENV=dev webpack serve --mode development", 
    "test": "cross-env NODE_ENV=test webpack --mode production",
    "build": "cross-env NODE_ENV=prod webpack --mode production"
  },
```

3、在 Webpack 配置文件中获取环境变量

```js
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')

console.log('process.env.NODE_ENV=', process.env.NODE_ENV) // 打印环境变量

const config = {
  entry: './src/index.js', // 打包入口地址
  output: {
    filename: 'bundle.js', // 输出文件名
    path: path.join(__dirname, 'dist') // 输出文件目录
  },
  module: { 
    rules: [
      {
        test: /\.css$/, //匹配所有的 css 文件
        use: 'css-loader' // use: 对应的 Loader 名称
      }
    ]
  },
  plugins:[ // 配置插件
    new HtmlWebpackPlugin({
      template: './src/index.html'
    })
  ]
}

module.exports = (env, argv) => {
  console.log('argv.mode=',argv.mode) // 打印 mode(模式) 值
  // 这里可以通过不同的模式修改 config 配置
  return config;
}
```

4、测试一下看看

a、执行 `npm run build`

```js
process.env.NODE_ENV= prod
argv.mode= production 
```

b、执行 `npm run test`

```js
process.env.NODE_ENV= test
argv.mode= production
```

c、执行 `npm run dev`

```js
process.env.NODE_ENV= dev
argv.mode= development
```

这样我们就可以不同的环境来动态修改 Webpack 的配置

### **1.8 启动 devServer**

1、安装[webpack-dev-server](https://link.zhihu.com/?target=https%3A//link.juejin.cn/%3Ftarget%3Dhttps%3A%2F%2Fwebpack.docschina.org%2Fconfiguration%2Fdev-server%2F%23devserver)

```text
npm intall webpack-dev-server@3.11.2 -D
```

> ⚠️注意：本文使用的 `webpack-dev-server` 版本是 `3.11.2`，当版本 `version >= 4.0.0` 时，需要使用 [devServer.static](https://link.zhihu.com/?target=https%3A//link.juejin.cn/%3Ftarget%3Dhttps%3A%2F%2Fwebpack.docschina.org%2Fconfiguration%2Fdev-server%2F%23devserverstatic) 进行配置，不再有 `devServer.contentBase` 配置项。

2、配置本地服务

```js
// webpack.config.js
const config = {
  // ...
  devServer: {
    contentBase: path.resolve(__dirname, 'public'), // 静态文件目录
    compress: true, //是否启动压缩 gzip
    port: 8080, // 端口号
    // open:true  // 是否自动打开浏览器
  },
 // ...
}
module.exports = (env, argv) => {
  console.log('argv.mode=',argv.mode) // 打印 mode(模式) 值
  // 这里可以通过不同的模式修改 config 配置
  return config;
}
```

**为什么要配置 contentBase ？**
因为 webpack 在进行打包的时候，对静态文件的处理，例如图片，都是直接 copy 到 dist 目录下面。但是对于本地开发来说，这个过程太费时，也没有必要，所以在设置 contentBase 之后，就直接到对应的静态目录下面去读取文件，而不需对文件做任何移动，节省了时间和性能开销。

3、启动本地服务

```js
$ npm run dev
```

为了看到效果，我在 html 中添加了一段文字，并在 public 下面放入了一张图片 logo.png

```js
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>ITEM</title>
</head>
<body>
  <p>ITEM</p>
</body>
</html>
```

结果

```js
public       
└─ logo.png  
```

打开地址 `http://localhost:8080/`

![img](https://pic3.zhimg.com/80/v2-d9af70c3702d58948761294e725f8dae_1440w.webp)

接着访问 `http://localhost:8080/logo.png`

![img](https://pic4.zhimg.com/80/v2-27f0307b6db619bd640ae6980efeafe7_1440w.webp)

OK，没问题

### **1.9 引入 CSS**

上面，我们在 Loader 里面讲到了使用 css-loader 来处理 css，但是单靠 css-loader 是没有办法将样式加载到页面上。这个时候，我们需要再安装一个 style-loader 来完成这个功能

style-loader 就是将处理好的 css 通过 style 标签的形式添加到页面上

1、安装 `style-loader` [[文档地址](https://link.zhihu.com/?target=https%3A//link.juejin.cn/%3Ftarget%3Dhttps%3A%2F%2Fwww.npmjs.com%2Fpackage%2Fstyle-loader)]

```js
npm install style-loader -D
```

2、配置 Loader

```js
const config = {
  // ...
  module: { 
    rules: [
      {
        test: /\.css$/, //匹配所有的 css 文件
        use: ['style-loader','css-loader']
      }
    ]
  },
  // ...
}
```

> **⚠️注意：** Loader 的执行顺序是固定从后往前，即按 `css-loader --> style-loader` 的顺序执行

3、引用样式文件

在入口文件 `./src/index.js` 引入样式文件 `./src/main.css`

```js
// ./src/index.js

import './main.css';

const a = 'Hello ITEM'
console.log(a)
module.exports = a;
```



```js
/* ./src/main.css */ 
body {
  margin: 10px auto;
  background: cyan;
  max-width: 800px;
}
```

3、重启一下本地服务，访问 `http://localhost:8080/`

![img](https://pic2.zhimg.com/80/v2-482fde655831aef844cdf728a52af42d_1440w.webp)

这样样式就起作用了，继续修改一下样式

```js
body {
  margin: 10px auto;
  background: cyan;
  max-width: 800px;
  /* 新增 */
  font-size: 46px;
  font-weight: 600;
  color: white;
  position: fixed;
  left: 50%;
  transform: translateX(-50%);
}
```

保存之后，样式就自动修改完成了

![img](https://pic1.zhimg.com/80/v2-d2ca4bf7f6876a0f402329a6ab8224c0_1440w.webp)

style-loader 核心逻辑相当于：

```js
const content = `${样式内容}`
const style = document.createElement('style');
style.innerHTML = content;
document.head.appendChild(style);
```

### **1.10 CSS 兼容性**

使用 [postcss-loader](https://link.zhihu.com/?target=https%3A//link.juejin.cn/%3Ftarget%3Dhttps%3A%2F%2Fwebpack.docschina.org%2Floaders%2Fpostcss-loader%2F)，自动添加 CSS3 部分属性的浏览器前缀

上面我们用到的 `transform: translateX(-50%);`，需要加上不同的浏览器前缀，这个我们可以使用 postcss-loader 来帮助我们完成

```js
npm install postcss-loader postcss -D
```



```js
const config = {
  // ...
  module: { 
    rules: [
      {
        test: /\.css$/, //匹配所有的 css 文件
        use: ['style-loader','css-loader', 'postcss-loader']
      }
    ]
  },
  // ...
}
```

> ⚠️ 这里有个很大的坑点：**参考文档配置好后，运行的时候会报错**

```js
Error: Loading PostCSS "postcss-import" plugin failed: 
Cannot find module 'postcss-import'
```

后面尝试安装插件的集合 `postcss-preset-env` ，然后修改配置为

```js
// webpack.config.js
// 失败配置
{
    loader: 'postcss-loader',
    options: {
      postcssOptions: {
        plugins: [
          [
            'postcss-preset-env', 
            {
              // 其他选项
            },
          ],
        ],
      },
    },
},
```

运行之后依然会报错，在查阅资料后，终于找到了正确的打开方式，我们重新来一遍

```js
npm install postcss postcss-loader postcss-preset-env -D
```

添加 postcss-loader 加载器

```js
const config = {
  // ...
  module: { 
    rules: [
      {
        test: /\.css$/, //匹配所有的 css 文件
        use: [
          'style-loader',
          'css-loader', 
          'postcss-loader'
        ]
      }
    ]
  }, 
  // ...
}
```

创建 postcss 配置文件 `postcss.config.js`

```js
// postcss.config.js
module.exports = {
  plugins: [require('postcss-preset-env')]
}
```

创建 postcss-preset-env 配置文件 `.browserslistrc`

```js
# 换行相当于 and
last 2 versions # 回退两个浏览器版本
> 0.5% # 全球超过0.5%人使用的浏览器，可以通过 caniuse.com 查看不同浏览器不同版本占有率
IE 10 # 兼容IE 10
```

再尝试运行一下

![img](https://pic2.zhimg.com/80/v2-fcb09f2d2ec757de1565dff46b56ec05_1440w.webp)

前缀自动加上了

如果你对 `.browserslistrc` 不同配置产生的效果感兴趣，可以使用 [autoprefixer](https://link.zhihu.com/?target=https%3A//link.juejin.cn/%3Ftarget%3Dhttp%3A%2F%2Fautoprefixer.github.io%2F) 进行在线转化查看效果

![img](https://pic2.zhimg.com/80/v2-e0f9ad13f63b38e66d842c8b933116dd_1440w.webp)

### **1.11 引入 Less 或者 Sass**

less 和 sass 同样是 Webpack 无法识别的，需要使用对应的 Loader 来处理一下

| Less | less-loader                        |
| ---- | ---------------------------------- |
| Sass | sass-loader node-sass 或 dart-sass |

Less 处理相对比较简单，直接添加对应的 Loader 就好了

Sass 不光需要安装 `sass-loader` 还得搭配一个 `node-sass`，这里 `node-sass` 建议用淘宝镜像来安装，npm 安装成功的概率太小了

这里我们就使用 Sass 来做案例

1、安装

```js
$ npm install sass-loader -D
# 淘宝镜像
$ npm i node-sass --sass_binary_site=https://npm.taobao.org/mirrors/node-sass/
```

2、新建 `./src/sass.scss`

Sass 文件的后缀可以是 `.scss(常用)` 或者 `.sass`

```js
$color: rgb(190, 23, 168);

body {
  p {
    background-color: $color;
    width: 300px;
    height: 300px;
    display: block;
    text-align: center;
    line-height: 300px;
  }
}
```

3、引入 Sass 文件

```js
import './main.css';
import './sass.scss' // 引入 Sass 文件


const a = 'Hello ITEM'
console.log(a)
module.exports = a;
```

4、修改配置

```js
const config = {
   // ...
   rules: [
      {
        test: /\.(s[ac]|c)ss$/i, //匹配所有的 sass/scss/css 文件
        use: [
          'style-loader',
          'css-loader',
          'postcss-loader',
          'sass-loader', 
        ]
      },
    ]
  },
  // ...
}
```

来看一下执行结果

![img](https://pic1.zhimg.com/80/v2-1827256bf1bd1eea49b7ffd5b8c47acc_1440w.webp)

成功

### **1.12 分离样式文件**

前面，我们都是依赖 `style-loader` 将样式通过 style 标签的形式添加到页面上

但是，更多时候，我们都希望可以通过 CSS 文件的形式引入到页面上

1、安装 `mini-css-extract-plugin`

```js
$ npm install mini-css-extract-plugin -D 
```

2、修改 `webpack.config.js` 配置

```js
// ...
// 引入插件
const MiniCssExtractPlugin = require('mini-css-extract-plugin')


const config = {
  // ...
  module: { 
    rules: [
      // ...
      {
        test: /\.(s[ac]|c)ss$/i, //匹配所有的 sass/scss/css 文件
        use: [
          // 'style-loader',
          MiniCssExtractPlugin.loader, // 添加 loader
          'css-loader',
          'postcss-loader',
          'sass-loader', 
        ] 
      },
    ]
  },
  // ...
  plugins:[ // 配置插件
    // ...
    new MiniCssExtractPlugin({ // 添加插件
      filename: '[name].[hash:8].css'
    }),
    // ...
  ]
}

// ...
```

3、查看打包结果

```js
dist                    
├─ avatar.d4d42d52.png  
├─ bundle.js            
├─ index.html           
├─ logo.56482c77.png    
└─ main.3bcbae64.css # 生成的样式文件  
```

![img](https://pic3.zhimg.com/80/v2-50dea47bdbdb2d2e81bde8edaeef7b02_1440w.webp)

### **1.13 图片和字体文件**

虽然上面在配置开发环境的时候，我们可以通过设置`contentBase`去直接读取图片类的静态文件，看一下下面这两种图片使用情况

1、页面直接引入

```js
<!-- 本地可以访问，生产环境会找不到图片 -->
<img src="/logo.png" alt="">
```

2、背景图引入

```js
<div id="imgBox"></div>

/* ./src/main.css */
...
#imgBox {
  height: 400px;
  width: 400px;
  background: url('../public/logo.png');
  background-size: contain;
}
```

直接会报错

![img](https://pic3.zhimg.com/80/v2-1df2685d16d5dc9fc2665770efc5900e_1440w.webp)

所以实际上，Webpack 无法识别图片文件，需要在打包的时候处理一下

常用的处理图片文件的 Loader 包含：

1. file-loader：解决图片引入问题，并将图片 copy 到指定目录，默认为 dist
2. url-loader：解依赖 file-loader，当图片小于 limit 值的时候，会将图片转为 base64 编码，大于 limit 值的时候依然是使用 file-loader 进行拷贝
3. img-loader：压缩图片

1、安装 `file-loader`

```js
npm install file-loader -D
```

2、修改配置

```js
const config = {
  //...
  module: { 
    rules: [
      {
         // ...
      }, 
      {
        test: /\.(jpe?g|png|gif)$/i, // 匹配图片文件
        use:[
          'file-loader' // 使用 file-loader
        ]
      }
    ]
  },
  // ...
}
```

3、引入图片

```js
<!-- ./src/index.html -->
<!DOCTYPE html>
<html lang="en">
...
<body>
  <p></p>
  <div id="imgBox"></div>
</body>
</html>
```

样式文件中引入

```js
/* ./src/sass.scss */

$color: rgb(190, 23, 168);

body {
  p {
    width: 300px;
    height: 300px;
    display: block;
    text-align: center;
    line-height: 300px;
    background: url('../public/logo.png');
    background-size: contain;
  }
}
```

js 文件中引入

```js
import './main.css';
import './sass.scss'
import logo from '../public/avatar.png'

const a = 'Hello ITEM'
console.log(a)

const img = new Image()
img.src = logo

document.getElementById('imgBox').appendChild(img)
```

启动服务，我们看一下效果

![img](https://pic2.zhimg.com/80/v2-ea4da2917a5c5da4f00335c6a5d4a185_1440w.webp)

显示正常 ✌️

我们可以看到图片文件的名字都已经变了，并且带上了 hash 值，然后我看一下打包目录

```js
dist                                     
├─ 56482c77280b3c4ad2f083b727dfcbf9.png  
├─ bundle.js                             
├─ d4d42d529da4b5120ac85878f6f69694.png  
└─ index.html                            
```

dist 目录下面多了两个文件，这正是 file-loader 拷贝过来的

如果想要修改一下名称，可以加个配置

```js
const config = {
  //...
  module: { 
    rules: [
      {
         // ...
      }, 
      {
        test: /\.(jpe?g|png|gif)$/i,
        use:[
          {
            loader: 'file-loader',
            options: {
              name: '[name][hash:8].[ext]'
            }
          }
        ]
      },
      {
        loader: 'file-loader',
        options: {
          name: '[name][hash:8].[ext]'
        }
      }
    ]
  },
  // ...
}
```

再次打包看一下

```js
dist                   
├─ avatard4d42d52.png  
├─ bundle.js           
├─ index.html          
└─ logo56482c77.png    
```

再看一下 url-loader

4、安装 `url-loader`

```js
$ npm install url-loader -D 
```

5、配置 `url-loader`

配置和 file-loader 类似，多了一个 limit 的配置

```js
const config = {
  //...
  module: { 
    rules: [
      {
         // ...
      }, 
      {
        test: /\.(jpe?g|png|gif)$/i,
        use:[
          {
            loader: 'url-loader',
            options: {
              name: '[name][hash:8].[ext]',
              // 文件小于 50k 会转换为 base64，大于则拷贝文件
              limit: 50 * 1024
            }
          }
        ]
      },
    ]
  },
  // ...
}
```

看一下，我们两个图片文件的体积

```js
public         
├─ avatar.png # 167kb
└─ logo.png   # 43kb 
```

我们打包看一下效果

![img](https://pic1.zhimg.com/80/v2-a61b8e7dbda43f2c1067f8965eeb5f70_1440w.webp)

很明显可以看到 logo.png 文件已经转为 base64 了

再看字体文件的处理

6、配置字体文件

首先，从[iconfont.cn](https://link.zhihu.com/?target=https%3A//link.juejin.cn/%3Ftarget%3Dhttps%3A%2F%2Fwww.iconfont.cn%2Fhome%2Findex%3Fspm%3Da313x.7781069.1998910419.2)下载字体文件到本地

![img](https://pic1.zhimg.com/80/v2-717ac5442cd1b7c34117c2e9ba31a3b8_1440w.webp)

在项目中，新建 `./src/fonts` 文件夹来存放字体文件

然后，引入到入口文件

```js
// ./src/index.js

import './main.css';
import './sass.scss'
import logo from '../public/avatar.png'

// 引入字体图标文件
import './fonts/iconfont.css'

const a = 'Hello ITEM'
console.log(a)

const img = new Image()
img.src = logo

document.getElementById('imgBox').appendChild(img)
```

接着，在 `./src/index.html` 中使用

```js
<!DOCTYPE html>
<html lang="en">
...
<body>
  <p></p>
  <!-- 使用字体图标文件 -->
  <!-- 1）iconfont 对应 font-family 设置的值-->
  <!-- 2）icon-member 图标 class 名称可以在 iconfont.cn 中查找-->
  <i class="iconfont icon-member"></i>
  <div id="imgBox"></div>
</body>
</html>
```

最后，增加字体文件的配置

```js
const config = {
  // ...
  {
    test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/i,  // 匹配字体文件
    use: [
      {
        loader: 'url-loader',
        options: {
          name: 'fonts/[name][hash:8].[ext]', // 体积大于 10KB 打包到 fonts 目录下 
          limit: 10 * 1024,
        } 
      }
    ]
  },
  // ...
}
```

打包一下，看看效果

![img](https://pic4.zhimg.com/80/v2-a51a3be85daae812a8b4bfecaef8cd1f_1440w.webp)

但是在 webpack5，内置了资源处理模块，`file-loader` 和 `url-loader` 都可以不用安装

### **1.14 资源模块的使用**

> webpack5 新增资源模块(asset module)，允许使用资源文件（字体，图标等）而无需配置额外的 loader。

资源模块支持以下四个配置：

1. `asset/resource` 将资源分割为单独的文件，并导出 url，类似之前的 file-loader 的功能.
2. `asset/inline` 将资源导出为 dataUrl 的形式，类似之前的 url-loader 的小于 limit 参数时功能.
3. `asset/source` 将资源导出为源码（source code）. 类似的 raw-loader 功能.
4. `asset` 会根据文件大小来选择使用哪种类型，当文件小于 8 KB（默认） 的时候会使用 asset/inline，否则会使用 asset/resource

贴一下修改后的完整代码

```js
// ./src/index.js

const config = {
  // ...
  module: { 
    rules: [
      // ... 
      {
        test: /\.(jpe?g|png|gif)$/i,
        type: 'asset',
        generator: {
          // 输出文件位置以及文件名
          // [ext] 自带 "." 这个与 url-loader 配置不同
          filename: "[name][hash:8][ext]"
        },
        parser: {
          dataUrlCondition: {
            maxSize: 50 * 1024 //超过50kb不转 base64
          }
        }
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/i,
        type: 'asset',
        generator: {
          // 输出文件位置以及文件名
          filename: "[name][hash:8][ext]"
        },
        parser: {
          dataUrlCondition: {
            maxSize: 10 * 1024 // 超过100kb不转 base64
          }
        }
      },
    ]
  },
  // ...
}

module.exports = (env, argv) => {
  console.log('argv.mode=',argv.mode) // 打印 mode(模式) 值
  // 这里可以通过不同的模式修改 config 配置
  return config;
}
```

执行打包，结果和之前一样

### **1.15 JS 兼容性（Babel）**

在开发中我们想使用最新的 Js 特性，但是有些新特性的浏览器支持并不是很好，所以 Js 也需要做兼容处理，常见的就是将 ES6 语法转化为 ES5。

这里将登场的“全场最靓的仔” -- Babel

1、未配置 Babel

我们写点 ES6 的东西

```js
// ./src/index.js

import './main.css';
import './sass.scss'
import logo from '../public/avatar.png'

import './fonts/iconfont.css'

// ...

class Author {
  name = 'ITEM'
  age = 18
  email = 'lxp_work@163.com'

  info =  () => {
    return {
      name: this.name,
      age: this.age,
      email: this.email
    }
  }
}


module.exports = Author
```

为了方便看源码，我们把 mode 换成 `development`

接着执行打包命令

打包完成之后，打开 `bundle.js` 查看打包后的结果

![img](https://pic2.zhimg.com/80/v2-883d27ecc58af53dad10bbf9b849a23d_1440w.webp)

![img](https://pic1.zhimg.com/80/v2-c6ac6fbf9fe82694d041f14e06f02ad8_1440w.webp)

虽然我们还是可以找打我们的代码，但是阅读起来比较不直观，我们先设置 mode 为`none`，以最原始的形式打包，再看一下打包结果

![img](https://pic1.zhimg.com/80/v2-08527be2c9b16b9b1e5947d376ffa53c_1440w.webp)

打包后的代码变化不大，只是对图片地址做了替换，接下来看看配置 babel 后的打包结果会有什么变化

2、安装依赖

```js
$ npm install babel-loader @babel/core @babel/preset-env -D
```

- `babel-loader` 使用 Babel 加载 ES2015+ 代码并将其转换为 ES5
- `@babel/core` Babel 编译的核心包
- `@babel/preset-env` Babel 编译的预设，可以理解为 Babel 插件的超集

3、配置 Babel 预设

```js
// webpack.config.js
// ...
const config = {
  entry: './src/index.js', // 打包入口地址
  output: {
    filename: 'bundle.js', // 输出文件名
    path: path.join(__dirname, 'dist'), // 输出文件目录
  },
  module: { 
    rules: [
      {
        test: /\.js$/i,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                '@babel/preset-env'
              ],
            }
          }
        ]
      },
    // ...
    ]
  },
  //...
}
// ...
```

配置完成之后执行一下打包

![img](https://pic1.zhimg.com/80/v2-88b5a67b00da15fd8693f743139e8bc4_1440w.webp)

刚才写的 ES6 class 写法 已经转换为了 ES5 的构造函数形式

尽然是做兼容处理，我们自然也可以指定到底要兼容哪些浏览器

为了避免 `webpack.config.js` 太臃肿，建议将 Babel 配置文件提取出来

根目录下新增 `.babelrc.js`

```js
// ./babelrc.js

module.exports = {
  presets: [
    [
      "@babel/preset-env",
      {
        // useBuiltIns: false 默认值，无视浏览器兼容配置，引入所有 polyfill
        // useBuiltIns: entry 根据配置的浏览器兼容，引入浏览器不兼容的 polyfill
        // useBuiltIns: usage 会根据配置的浏览器兼容，以及你代码中用到的 API 来进行 polyfill，实现了按需添加
        useBuiltIns: "entry",
        corejs: "3.9.1", // 是 core-js 版本号
        targets: {
          chrome: "58",
          ie: "11",
        },
      },
    ],
  ],
};
```

好了，这里一个简单的 Babel 预设就配置完了

常见 Babel 预设还有：

- `@babel/preset-flow`
- `@babel/preset-react`
- `@babel/preset-typescript`

感兴趣的可以自己去了解一下，这里不做扩展了，下面再说说插件的使用

4、配置 Babel 插件

对于正在提案中，还未进入 ECMA 规范中的新特性，Babel 是无法进行处理的，必须要安装对应的插件，例如：

```js
// ./ index.js

import './main.css';
import './sass.scss'
import logo from '../public/avatar.png'

import './fonts/iconfont.css'

const a = 'Hello ITEM'
console.log(a)

const img = new Image()
img.src = logo

document.getElementById('imgBox').appendChild(img)

// 新增装饰器的使用
@log('hi')
class MyClass { }

function log(text) {
  return function(target) {
    target.prototype.logger = () => `${text}，${target.name}`
  }
}

const test = new MyClass()
test.logger()
```

执行一下打包

![img](https://pic4.zhimg.com/80/v2-f62ddc9465bcb8b55107a68b5c37b64b_1440w.webp)

不出所料，识别不了 ‍♀️

怎么才能使用呢？Babel 其实提供了对应的插件：

- `@babel/plugin-proposal-decorators`
- `@babel/plugin-proposal-class-properties`

安装一下：

```text
$ npm install babel/plugin-proposal-decorators @babel/plugin-proposal-class-properties -D
```

打开 `.babelrc.js` 加上插件的配置

```js
module.exports = {
  presets: [
    [
      "@babel/preset-env",
      {
        useBuiltIns: "entry",
        corejs: "3.9.1",
        targets: {
          chrome: "58",
          ie: "11",
        },
      },
    ],
  ],
  plugins: [    
    ["@babel/plugin-proposal-decorators", { legacy: true }],
    ["@babel/plugin-proposal-class-properties", { loose: true }],
  ]
};
```

这样就可以打包了，在 `bundle.js` 中已经转化为浏览器支持的 Js 代码

![img](https://pic4.zhimg.com/80/v2-72f0a9222f6d3af2ee6ee9f46cadd7bb_1440w.webp)

同理，我们可以根据自己的实际需求，搭配不同的插件进行使用

## 2、SourceMap 配置选择

SourceMap 是一种映射关系，当项目运行后，如果出现错误，我们可以利用 SourceMap 反向定位到源码位置

### **2.1 devtool 配置**

```js
const config = {
  entry: './src/index.js', // 打包入口地址
  output: {
    filename: 'bundle.js', // 输出文件名
    path: path.join(__dirname, 'dist'), // 输出文件目录
  },
  devtool: 'source-map',
  module: { 
     // ...
  }
  // ...
```

执行打包后，dist 目录下会生成以 `.map` 结尾的 SourceMap 文件

```js
dist                   
├─ avatard4d42d52.png  
├─ bundle.js           
├─ bundle.js.map     
└─ index.html          
```

除了 `source-map` 这种类型之外，还有很多种类型可以用，例如：

- `eval`
- `eval-source-map`
- `cheap-source-map`
- `inline-source-map`
- `cheap-module-source-map`
- `inline-cheap-source-map`
- `cheap-module-eval-source-map`
- `inline-cheap-module-source-map`
- `hidden-source-map`
- `nosources-source-map`

这么多种，到底都有什么区别？如何选择呢？

2.2 配置项差异

1、为了方便比较它们的不同，我们**新建一个项目**

```js
webpack_source_map                                             
├─ src                                      
│  ├─ Author.js                            
│  └─ index.js                               
├─ package.json                             
└─ webpack.config.js                        
```

2、打开 `./src/Author.js`

```js
class Author {
  name = 'ITEM'
  age = 18
  email = 'lxp_work@163.com'

  info =  () => {
    return {
      name: this.name,
      age: this.age,
      email: this.email
    }
  }
}

module.exports = Author
```

3、打开 `./src/index.js`

```js
import Author from './Author'

const a = 'Hello ITEM'
console.log(a)

const img = new Image()
img.src = logo

document.getElementById('imgBox').appendChild(img)

const author = new Author();

console.log(author.info)
```

4、打开 `package.json`

```js
 {
  "name": "webpack-source-map",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "license": "MIT",
  "scripts": {
    "build": "webpack"
  },
  "devDependencies": {
    "@babel/core": "^7.6.4",
    "@babel/preset-env": "^7.6.3",
    "babel-loader": "^8.0.6",
    "html-webpack-plugin": "^3.2.0",
    "webpack": "^5.44.0",
    "webpack-cli": "^4.7.2"
  }
}
```

5、打开 `webpack.config.js`

```abap
// 多入口打包
module.exports = [
  {
    entry: './src/index.js',
    output: {
      filename: 'a.js'
    }
  },
  {
    entry: './src/index.js',
    output: {
      filename: 'b.js'
    }
  }
]
 
```

执行打包命令 `npm run build`，看一下结果

```text
dist     
├─ a.js  
└─ b.js  
 
```

不用关心打包结果 `a.js b.js` 里面是什么，到这步的目的是测试多入口打包

改造成多入口的目的是方便我们后面进行比较

6、不同配置项使用单独的打包入口，打开 `webpack.config.js` 修改

```js
const HtmlWebpackPlugin = require('html-webpack-plugin')

// 1）定义不同的打包类型
const allModes = [
  'eval',
  'source-map',
  'eval-source-map',
  'cheap-source-map',
  'inline-source-map',
  'cheap-eval-source-map',
  'cheap-module-source-map',
  'inline-cheap-source-map',
  'cheap-module-eval-source-map',
  'inline-cheap-module-source-map',
  'hidden-source-map',
  'nosources-source-map'
]

// 2）循环不同 SourceMap 模式，生成多个打包入口
module.exports = allModes.map(item => {
  return {
    devtool: item,
    mode: 'none',
    entry: './src/main.js',
    output: {
      filename: `js/${item}.js`
    },
    module: {
      rules: [
        {
          test: /.js$/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env']
            }
          }
        }
      ]
    },
    plugins: [
      3）输出到不同的页面
      new HtmlWebpackPlugin({
        filename: `${item}.html`
      })
    ]
  }
}
```

7、模拟代码错误

```text
// ./src/index.js

import Author from './Author'

const a = 'Hello ITEM'
// 故意使用了错误的 console.log 
console.log11(a)
 
const img = new Image()
img.src = logo

document.getElementById('imgBox').appendChild(img)

const author = new Author();

console.log(author.info)
```

8、尝试打包

报错了!!

![img](https://pic2.zhimg.com/80/v2-56c0343215470c84656a94b374d25229_1440w.webp)


提示有SourceMap 模式的名称不对，原来它们的拼接是有规律和意义的

我们按照校验规则 `^(inline-|hidden-|eval-)?(nosources-)?(cheap-(module-)?)?source-map$` 检查一下

`cheap-eval-source-map` 和 `cheap-module-eval-source-map` 好像有问题，`eval` 跑后面去了，改一下

```js
// 修改之后
const allModes = [
  'eval',
  'source-map',
  'eval-source-map',
  'cheap-source-map',
  'inline-source-map',
  'eval-cheap-source-map',
  'cheap-module-source-map',
  'inline-cheap-source-map',
  'eval-cheap-module-source-map',
  'inline-cheap-module-source-map',
  'hidden-source-map',
  'nosources-source-map'
]
```

再执行一下打包

还是有报错！！接着改

![img](https://pic3.zhimg.com/80/v2-89af0ca0d05529a970e4544263e541d6_1440w.webp)

错误信息我查了一下，大概率是`html-webpack-pugin`的版本太老，不兼容 webpack5，我们升级一下版本至`"html-webpack-plugin": "^5.3.2"`再尝试一下，OK 了

```js
dist                                     
├─ js                                    
│  ├─ cheap-module-source-map.js #............ 有对应的 .map 文件        
│  ├─ cheap-module-source-map.js.map     
│  ├─ cheap-source-map.js #................... 有              
│  ├─ cheap-source-map.js.map            
│  ├─ eval-cheap-module-source-map.js #....... 无  
│  ├─ eval-cheap-source-map.js #.............. 无          
│  ├─ eval-source-map.js #.................... 无               
│  ├─ eval.js #............................... 无                           
│  ├─ hidden-source-map.js #.................. 有              
│  ├─ hidden-source-map.js.map           
│  ├─ inline-cheap-module-source-map.js #..... 无  
│  ├─ inline-cheap-source-map.js #............ 无         
│  ├─ inline-source-map.js #.................. 无               
│  ├─ nosources-source-map.js #............... 有           
│  ├─ nosources-source-map.js.map        
│  ├─ source-map.js #......................... 有                     
│  └─ source-map.js.map                  
├─ cheap-module-source-map.html          
├─ cheap-source-map.html                 
├─ eval-cheap-module-source-map.html     
├─ eval-cheap-source-map.html            
├─ eval-source-map.html                  
├─ eval.html                             
├─ hidden-source-map.html                
├─ inline-cheap-module-source-map.html   
├─ inline-cheap-source-map.html          
├─ inline-source-map.html                
├─ nosources-source-map.html             
└─ source-map.html                       
```

从目录结构我们可以很容易看出来，含 `eval` 和 `inline` 模式的都没有对应的`.map` 文件，具体为什么，下面接着分析

接着，我们在 dist 目录起一个服务，在浏览器打开

![img](https://pic4.zhimg.com/80/v2-8b7273851e3f8ed60ac3c904204e0647_1440w.webp)

然后，我们一个个来分析

**`eval`** **模式**

1、生成代码通过 **eval** 执行

![img](https://pic1.zhimg.com/80/v2-03f22fe2467e5ed2feaf6155ab50fee8_1440w.webp)

2、源代码位置通过 **@sourceURL** 注明

![img](https://pic3.zhimg.com/80/v2-db2a4aebed1669302a899c2b2d47726a_1440w.webp)

3、无法定位到错误位置，只能定位到**某个文件**

**4、**不用生成 SourceMap 文件，**打包速度快**

**source-map** **模式：**

1. 生成了对应的 SourceMap 文件，**打包速度慢**
2. 在**源代码**中定位到错误所在**行列**信息

![img](https://pic1.zhimg.com/80/v2-895308882e9132103d7174b38dc3d848_1440w.webp)

**eval-source-map模式：**

**1、**生成代码通过 **eval** **执行**

![img](https://pic1.zhimg.com/80/v2-2f5002a59778ab6f5ed7076dd92110a0_1440w.webp)

2、包含**dataUrl**形式的 SourceMap 文件

![img](https://pic4.zhimg.com/80/v2-94232b676395a6681c48e8b57fe3385f_1440w.webp)

3、可以在**编译后**的代码中定位到错误所在**行列**信息

![img](https://pic2.zhimg.com/80/v2-a02e10f2f80f6b20acb1772a1ef9b635_1440w.webp)

4、生成 dataUrl 形式的 SourceMap，打包速度慢

**eval-cheap-source-map 模式：**

1. 生成代码通过 **eval 执行**
2. 包含 **dataUrl** 形式的 SourceMap 文件
3. 可以在**编译后**的代码中定位到错误所在**行**信息
4. 不需要定位列信息，打包**速度较快**

**eval-cheap-module-source-map 模式：**

1. 生成代码通过 **eval** 执行
2. 包含 **dataUrl** 形式的 SourceMap 文件
3. 可以在**编译后**的代码中定位到错误所在**行**信息
4. 不需要定位列信息，打包**速度较快**
5. 在**源代码**中定位到错误所在**行**信息

![img](https://pic4.zhimg.com/80/v2-ed9c19c8310fe76b95ff914f18e112d3_1440w.webp)

**inline-source-map** **模式：**

1、通过 **dataUrl** 的形式引入 SourceMap 文件

![img](https://pic2.zhimg.com/80/v2-1ece70cc8ba581828063fc06c66ae52d_1440w.webp)

... 余下和 source-map 模式一样

**hidden-source-map** **模式：**

1. 看不到 SourceMap 效果，但是生成了 SourceMap 文件

**nosources-source-map** **模式：**

1、能看到错误出现的位置

![img](https://pic3.zhimg.com/80/v2-a246fa45d92e409c5fc9115f8c17d28a_1440w.webp)

2、但是没有办法现实对应的源码

![img](https://pic2.zhimg.com/80/v2-a184bd3f5349e50a1fb6416fce250365_1440w.webp)

接下来，我们稍微总结一下：

![img](https://pic1.zhimg.com/80/v2-ba6ab0c570c90e0e2ed852f28e59e634_1440w.webp)

对照一下校验规则^(inline-|hidden-|eval-)?(nosources-)?(cheap-(module-)?)?source-map$分析一下关键字

![img](https://pic2.zhimg.com/80/v2-3903f6bde20f049de64d0f49d506ed31_1440w.webp)

好了，到这里 SourceMap 就分析完了

### **2.3 推荐配置**

1. 本地开发：

推荐：eval-cheap-module-source-map

理由：

- 本地开发首次打包慢点没关系，因为 eval 缓存的原因，rebuild 会很快
- 开发中，我们每行代码不会写的太长，只需要定位到行就行，所以加上 cheap
- 我们希望能够找到源代码的错误，而不是打包后的，所以需要加上 modele

1. 生产环境：

推荐：(none)

理由：

就是不想别人看到我的源代码

## 3. 三种 hash 值

Webpack 文件指纹策略是将文件名后面加上 hash 值。特别在使用 CDN 的时候，缓存是它的特点与优势，但如果打包的文件名，没有 hash 后缀的话，你肯定会被缓存折磨的够呛

例如我们在基础配置中用到的：filename: "[name][hash:8][ext]"

这里里面 [] 包起来的，就叫占位符，它们都是什么意思呢？请看下面这个表

![img](https://pic2.zhimg.com/80/v2-18c92088e096ecc297b3ec876e697d9d_1440w.webp)

表格里面的 hash、chunkhash、contenthash 你可能还是不清楚差别在哪

- **hash** ：任何一个文件改动，整个项目的构建 hash 值都会改变；
- **chunkhash**：文件的改动只会影响其所在 chunk 的 hash 值；
- **contenthash**：每个文件都有单独的 hash 值，文件的改动只会影响自身的 hash 值；