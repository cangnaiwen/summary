# 精通CSS专题 使用的 flex 布局 - 简洁纯享版





### Flex布局

![img](https://pic4.zhimg.com/80/v2-8f8bc57a26ab41af8c748a001d0b83cf_720w.webp)

Flex布局

### 容器属性

1. flex声明

```css
.container
{
    display: flex;
}
```

\2. 主轴方向

```css
flex-direction: column;
```

row（默认）
主轴水平方向，起点在左边

row-reverse
主轴水平方向，起点在右边

column
主轴垂直方向，起点在上边

column-reverse
主轴垂直方向，起点在下边

![img](https://pic3.zhimg.com/80/v2-5fc5320bf7c4940f85b49259ca8565aa_720w.webp)

\3. 换行

```css
flex-wrap: wrap;
```

nowrap
不换行

wrap
换行，第一行在上面

wrap-reverse
换行，第一行在下面

![img](https://pic4.zhimg.com/80/v2-7bfefc8dd8e0eb34b9eb1d8ddf71022f_720w.webp)

\3. 主轴方向 + 换行 合并写法

```css
flex-flow: <flex-direction> <flex-wrap>;
```

\4. 主轴对齐方式

```css
justify-content: center;
```

start
从行首开始排列。每行第一个元素与行首对齐，同时所有后续的元素与前一个对齐。

end
从行尾开始排列。每行最后一个元素与行尾对齐，同时所有前面的元素与后一个对齐。

flex-start
元素紧密地排列在弹性容器的主轴起始侧。仅应用于弹性布局的项目。对于不是弹性容器里的元素，此值将被视为 start。

flex-end
元素紧密地排列在弹性容器的主轴结束侧。仅应用于弹性布局的元素。对于不是弹性容器里的元素，此值将被视为 end。

center
伸缩元素向每行中点排列。每行第一个元素到行首的距离将与每行最后一个元素到行尾的距离相同。

left
伸缩元素一个挨一个在对齐容器得左边缘，如果属性的轴与内联轴不平行，则 left 的行为类似于 start。

right
元素以容器右边缘为基准，一个挨着一个对齐，如果属性轴与内联轴不平行，则 right 的行为类似于 end。

space-between
在每行上均匀分配弹性元素。相邻元素间距离相同。每行第一个元素与行首对齐，每行最后一个元素与行尾对齐。

space-around
在每行上均匀分配弹性元素。相邻元素间距离相同。每行第一个元素到行首的距离和每行最后一个元素到行尾的距离将会是相邻元素之间距离的一半。

space-evenly
flex 项都沿着主轴均匀分布在指定的对齐容器中。相邻 flex 项之间的间距，主轴起始位置到第一个 flex 项的间距，主轴结束位置到最后一个 flex 项的间距，都完全一样。

stretch
如果元素沿主轴的组合尺寸小于对齐容器的尺寸，任何尺寸设置为 auto 的元素都会等比例地增加其尺寸（而不是按比例增加），同时仍然遵守由 max-height/max-width（或相应功能）施加的约束，以便沿主轴完全填充对齐容器的组合尺寸。

![img](https://pic2.zhimg.com/80/v2-64345046f218024f2d36101309cab235_720w.webp)

\5. 交叉轴对齐方式

```css
align-items: flex-start;
```

flex-start
只在 flex 布局中使用，将元素与 flex 容器的主轴起点或交叉轴起点对齐。

flex-end
只在 flex 布局中使用，将元素与 flex 容器的主轴末端或交叉轴末端对齐。

center
flex 元素的外边距框在交叉轴上居中对齐。如果元素的交叉轴尺寸大于 flex 容器，它将在两个方向上等距溢出。

start
将元素与容器的主轴起点或交叉轴起点对齐。

end
将元素与容器的主轴末端或交叉轴末端对齐。

self-start
将元素与容器的主轴起点或交叉轴起点对齐，轴起点的方向对应于元素的起始方向。

self-end
将元素与容器的主轴末端或交叉轴末端对齐，轴末端的方向对应于元素的结尾方向。

baseline、first baseline、last baseline
所有 flex 元素都对齐，以使它们的 flex 容器基线 对齐。距离其交叉轴起点和基线之间最远的元素与行的交叉轴起点对齐。

stretch
如果（多个）元素的组合大小小于容器的大小，其中自动调整大小的元素将等量增大，以填满容器，同时这些元素仍然保持其宽高比例的约束。

### 项目属性

1. 排序

order：值越小，越靠前

flex-grow：放大比例

flex-shrink：缩小比例

flex-basis：主轴方向的初始大小

align-self：覆盖 align-items 属性

flex（合并写法）：<flex-grow> <flex-shrink> || <flex-basis>