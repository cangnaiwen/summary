# Jest 组件单元测试

#### Jest - Vue 组件的单元测试

单元测试就是对一个函数的输入和输出进行测试。

使用断言的方式，根据输入判断实际的输出和预测的输出是否相同。

使用单元测试的目的，是用来发现模块内部可能存在的各种错误。

组件的单元测试指的是使用单元测试工具，对组件的各种状态和行为进行测试，确保组件发布之后在项目中使用组件的过程中不会导致程序出现错误。

组件的单元测试的好处

​	提供描述组件行为的文档
​	对组件的单元测试，其实就是一种组件使用方式的文档
​	节省手动测试的时间
​	减少研发新特性时产生的 bug
​	改进设计
​	促进重构

#### 配置组件单元测试的环境

##### 安装依赖

​	Vue Test Utils Vue 组件单元测试的官方库。（Github）
​		它需要结合一个单元测试框架一起使用，例如 Jest
​	Jest Facebook 出的单元测试框架（官网）
​		它和Vue的结合比较方便，配置最少
​		它可以进行单元测试，但是并不支持单文件组件，所以还需要一个预处理器，把vue单文件组件编译之后的结果（JS代码）交给 Jest 处理。
​	vue-jest Vue 官方提供的一个为 Jest 服务的预处理器
​		它支持单文件组件的大多数功能
​	babel-jest 对测试代码中的ES6语法进行降级处理

```js
# 在工作区根目录安装开发依赖

yarn add jest @vue/test-utils vue-jest babel-jest -D -W
```


当前项目示例使用了 yarn workspaces 所以要使用 -W 指定依赖安装到 workspace root，否则会报错。

#### 配置测试脚本

项目根目录 package.json

```js
"scripts": {
  "test": "jest",
  // ...
}
```



#### Jest 配置文件

项目根目录创建 jest.config.js

```js
module.exports = {
  // 运行 jest 的时候寻找测试文件的路径
  testMatch: ['**/__tests__/**/*.[jt]s?(x)'],
  // 测试文件中导入文件的后缀，配置.vue
  moduleFileExtensions: [
    'js',
    'json',
    // 告诉 Jest 处理 `*.vue` 文件
    'vue'
  ],
  // 转换：通过正则配置匹配的文件，交给哪个模块处理
  transform: {
    // 用 `vue-jest` 处理 `*.vue` 文件
    '.*\\.(vue)$': 'vue-jest',
    // 用 `babel-jest` 处理 js
    '.*\\.(js)$': 'babel-jest'
  }
}
```


当前项目示例每个组件包中的测试目录是 __test__ ，需要调整为 __tests__。

#### Babel 配置文件

项目根目录创建 babel.config.js

```js
module.exports = {
  presets: [
    ['@babel/preset-env']
  ]
}
```



#### Babel 桥接

当前使用 storybook 初始化的项目中安装了 babel7。

但是 vue-jest 依赖的是 babel6。

此时运行测试的时候会提示找不到 babel。

解决这个问题需要安装 babel 的桥接：

```js
yarn add babel-core@bridge -D -W
```


vue-jest 和 Vue Test Utils 文档中有相关的介绍：

​	vue-jest - Usage with Babel 7
​	Vue Test Utils - Using with Babel

#### Jest 和 Vue Test Utils 常用 API

Jest 常用 API

全局函数
	describe(name, fn) 把相关测试组合在一起
	test(name, fn) 测试方法
	expect(value) 断言
匹配器
	toBe(value) 判断值是否相等
	toEqual(obj) 判断对象是否相等
	toContain(value) 判断数组或字符串中是否包含value
	…
快照
	toMatchSnapshot() 记录快照
		第一次调用会把 expect 中的值以字符串的形式存储到一个.snap文件中
		之后再运行快照，会对比当前 expect 函数中的值 和 快照文件中的结果
		如果相同，测试通过，如果不同，测试失败。
		重新生成快照文件 yarn jest -u
Vue Test Utils 常用 API

​	mount() 创建一个包含被挂载和渲染的 Vue 组件的 Wrapper。
​	Wrapper 是一个包裹器，除了记录组件生成的实例外，还记录了很多对组件中 DOM 元素操作的方法，方便测试组件生成的内容。
​	vm wrapper 包裹的组件实例
​	props() 返回Vue实例的 props 对象，或指定prop的值，方便测试组件的状态
​	html() 组件生成的 HTML 标签（字符串）
​	find() 通过选择器返回匹配到的组件中的 DOM 元素
​	trigger() 触发 DOM 原生事件。触发自定义事件使用 wrapper.vm.$emit()
​	…

#### 编写测试文件

假设测试 Input 组件。

目录结构：

```js
|- packages
|  |- input
|  |  |- __tests__
|  |  |  |- input.test.js # 约定测试文件都已 .test.js 结尾
|  |  |- src
|  |  |  |- input.vue
|  |  |- index.js
|  |  |- package.json
#... 其他文件：stories 版权 md文档等
```



```js
// input.vue

<template>
  <div>
    <input :type="type" v-bind="$attrs" @input="handleInput" />
  </div>
</template>


<script>
export default {
  name: 'MyInput',
  inheritAttrs: false,
  model: {
    prop: 'value',
    event: 'input'
  },
  props: {
    value: {},
    type: {
      type: String,
      default: 'text'
    }
  },
  methods: {
    handleInput($event) {
      this.$emit('input', $event.target.value)
    }
  }
}
</script>


```


编写测试文件：

```js
// input.test.js
// 导入组件
import input from '../src/input.vue'
// 导入 Vue Test Utils 中的 API 挂在组件
import { mount } from '@vue/test-utils'

// 通过 describe 创建一个代码块
// 把 input 的相关测试都放到这个代码块中
describe('my-input', () => {

  // 测试1：测试 input 组件是否生成的是预期的文本框
  test('input-text', () => {
    // 在内存中挂在组件，并获取返回的包裹器
    const wrapper = mount(input)
    // 断言：包裹器获取的html字符串中包含指定内容
    expect(wrapper.html()).toContain('input type="text"')
  })

  // 测试2：测试密码文本框
  test('input-password', () => {
    // propsData 设置组件的props属性
    const wrapper = mount(input, {
      propsData: {
        type: 'password'
      }
    })

    expect(wrapper.html()).toContain('input type="password"')

  })

  // 测试3：测试组件状态（value值）
  test('input-value', () => {
    const wrapper = mount(input, {
      propsData: {
        type: 'password',
        value: 'admin'
      }
    })

    expect(wrapper.props('value')).toBe('admin')

  })

  // 测试4：快照对比
  test('input-snapshot', () => {
    const wrapper = mount(input, {
      propsData: {
        type: 'password',
        value: 'admin'
      }
    })

    // 首次执行该测试，会在当前test.js所在目录创建 __snapshots__ 目录
    // 目录中存放的就是快照文件 input.test.js.snap
    // 当前测试用例存储的快照内容是组件实例的 DOM
    
    // 之后再执行该测试，就会用 expect 传入的值，对比快照文件中的内容
    // 如果一样，测试成功，如果不一样，测试失败
    // 可以修改 propsData 的参数查看对比结果
    // 重新生成快照： `yarn jest -u`
    expect(wrapper.vm.$el).toMatchSnapshot()

  })

})
```


