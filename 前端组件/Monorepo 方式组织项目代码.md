# Monorepo 方式组织项目代码

#### Monorepo 方式组织项目代码

开发一个组件库，其中可能会有很多的组件。

当组件库开发完毕后，还要发布到NPM或私有仓库供其他人使用。

在使用 ElementUI 的时候，可以完整的引入，也可以根据需要按需引入部分组件以减少打包的体积。

按需引入还需要安装Babel的插件babel-plugin-component，配置.babelrc，比较麻烦。

组件库为了让用户使用方便，可以把每一个组件作为一个单独的包发布到 NPM 上，用户在使用的时候，可以只下载它需要的组件。

当然也可以把所有的组件打包到一起发布。

#### 项目的组织方式

Multirepo(Multiple Repository) - 多仓库，每一个包创建一个单独的项目
	每个组件都需要一个仓库，都有自己的脚手架package.json，都要下载和管理自己的依赖，都需要单独进行发布。
	并且多个组件可能会有相同的依赖，避免不了重复下载这些依赖，还会占用硬盘空间
Monorepo(Monolithic Repository) - 一个项目仓库中管理多个模块/包
	这种方式只创建一个项目，根目录以脚手架为主
	主要内容（所有组件）都放在根目录下的一个统一的目录下，一般命名为 packages。
	每个组件在这个目录中配置一个子文件夹
	所有的组件都可以放在根目录下的一个统一的目录下，这个目录可以叫做 packages。
		使用monorepo的时候，要把所有的包（组件）放到根目录下一个指定的文件夹下
		这个文件夹一般命名 packages，也可以叫其他名字
	每一个组件在这个目录中配置一个子文件夹，设置为包。
	因为所有组件的管理方式都比较类似，相关的配置可以放在根目录。
	不同的组件可能会有相同的依赖，这时只需下载一份。
	将来的测试、打包、发布也可以在当前项目进行统一的管理。
	这样管理项目，对组件库开发来说更方便。
	通过monorepo的方式组织代码的结构，可以让每一个包，都可以单独测试，单独发布，以统一管理它们的依赖。

#### Monorepo 介绍

Monorepo 是项目代码的一种管理方式。

这种方式在一个项目仓库（repo）中管理多个模块/包（package）。

根目录以脚手架为主，主要内容（所有开发的模块，对组件库来说就是组件）都放在根目录下一个统一的目录下，一般命名为 packages。

每个组件在这个目录中配置一个子文件夹，作为一个包，它们遵循比较统一的目录结构。

将来可以通过一些工具管理这些包，还能管理其他工作，例如打包、发布、测试等。

因为所有组件的管理方式都比较类似，相关的配置可以放在根目录。

​	配合 lerna 或 yarn workspaces 可以管理每个组件的依赖，相同依赖避免重复安装。

​	将来的测试、打包、发布也可以在当前项目进行统一管理。

​	并且可以让每一个包都可以单独测试、单独发布，以统一管理它们的依赖。

这个方式管理项目，对组件库开发来说更方便。

很多知名的开源库都是使用这种方式来管理项目的，例如 Vue3、React、Babel 等。

查看这些项目，根目录的内容以脚手架为主，主要内容都在 packages 目录中，分多个 package 进行管理。

```js
|- packages
|  |- pkg1
|  |  |- package.json
|  |- pkg2
|  |  |- package.json
|- package.json
```



#### vue3 源码结构

vue-next

##### package.json

```js
{
  // 禁止把当前的包发布到NPM，Vue3 发布的是packages下的所有包
  "private": true,

  // yarn workspaces
  workspaces: [
    "packages/*"
  ]
  // ...
}
```



##### packages

packages 目录下的内容就是 Vue3 的源码，每一个文件夹就是一个包。

Vue3 把不同功能的代码放到不同的包中进行管理。

compiler-xxx 存储和编译器相关的代码
reactivity 响应式相关代码
runtime-xxx 运行时相关代码
…
这些包可以单独发布，使用者可以单独去下载使用。

##### 查看单独的包

随便查看一个包的结构（例如 packages/compiler-core）：

__tests_ 存储测试相关的代码
src 当前包的源码
LICENSE 版权，内容是开源协议的描述
README.md
api-extractor.json 配置文件
index.js 打包时候的入口
package.json

```js
// package.json
{
  // 将来发布到NPM展示的名字
  "name": "@vue/compiler-core",
  // 版本号
  "version": "3.0.0-rc.10",
  // 描述
  "description": "@vue/compiler-core",
  // 包的入口
  "main": "index.js",
  // ...
}
```


其他的包的目录结构都一样。

使用monorepo管理项目的时候，应尽可能让每一个包的目录结构都是相同的，这样方便将来的管理。

#### React 源码结构

##### packge.json

```js

与Vue3中的package.json一样，都包含这两个属性：

{
  // 禁止把当前的包发布到NPM，Vue3 发布的是packages下的所有包
  "private": true,

  // yarn workspaces
  workspaces: [
    "packages/*"
  ]
  // ...
}
```



##### packages

React也是把不同的功能放到不同的包，也可以单独发布到NPM使用。

##### 包的结构

例如查看 packages/create-subscription

npm
src 源码
README.md
index.js 打包入口
package.json

#### 按照Monorepo的方式组织代码结构

在项目根目录创建 packages 文件夹，这个文件夹下存放所有要开发的组件。

每一个组件对应一个文件夹，每一个文件夹就是一个可以单独发布到NPM的包。

包的结构：

packages
	button - 组件名
		__test__ - 存放测试相关代码
		dist - 打包的目录
		src - 存放源码（.vue文件）
		index.js - 打包的入口文件
			定义组件的install方法，内容是注册组件
			导出组件
			这样就提供了插件安装和单独注册两个方式去使用这个组件
		LICENSE - 版权信息（MIT）
		package.json - 组件的描述信息
		README.md - 组件的文档
最终效果如：

![](https://img-blog.csdnimg.cn/20200921104922605.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTI5NjE0MTk=,size_16,color_FFFFFF,t_70#pic_center)

之后可以使用plop基于模板快速生成包结构。
