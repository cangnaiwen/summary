# vsCode插件开发流程入门

1、工具准备

　　需要安装一个生产插件代码的东西，也就是Yeoman和VS Code Extension generator

```
npm install -g yo generator-code
```

2、接着在项目目录下面生成项目结构

```
yo code
　在os系统上可以通过用上下键来选择要创建哪种类型的项目，在win可以通过输入1、2、3这样的数字然后按回车来选择。
　　生成过程中，需要做一些初始化的设置，如下
```

1. 输入你扩展的名称
2. 输入一个标志（项目创建的文件名称用这个）
3. 输入对这个扩展的描述
4. 输入以后要发布用到的一名称（和以后再发布时候有一个名字是对应上的）
5. 是问你要不要创建一个git仓库用于版本管理

　　看一下目录结构

```
.
├── .gitignore                  //配置不需要加入版本管理的文件
├── .vscode                     // VS Code的整合
│   ├── launch.json
│   ├── settings.json
│   └── tasks.json
├── .vscodeignore                //配置不需要加入最终发布到拓展中的文件
├── README.md
├── src                         // 源文件
│   └── extension.ts            // 如果我们使用js来开发拓展，则该文件的后缀为.js
├── test                        // test文件夹
│   ├── extension.test.ts       // 如果我们使用js来开发拓展，则该文件的后缀为.js
│   └── index.ts                // 如果我们使用js来开发拓展，则该文件的后缀为.js
├── node_modules
│   ├── vscode                  // vscode对typescript的语言支持。
│   └── typescript              // TypeScript的编译器
├── out                         // 编译之后的输出文件夹(只有TypeScript需要，JS无需)
│   ├── src
│   |   ├── extension.js
│   |   └── extension.js.map
│   └── test
│       ├── extension.test.js
│       ├── extension.test.js.map
│       ├── index.js
│       └── index.js.map
├── package.json                // 该拓展的资源配置文件
├── tsconfig.json               // 
├── typings                     // 类型定义文件夹
│   ├── node.d.ts               // 和Node.js关联的类型定义
│   └── vscode-typings.d.ts     // 和VS Code关联的类型定义
└── vsc-extension-quickstart.md 
```

3、运行调试

　　用vscode打开项目目录，点击调试按钮或者使用快捷键F5，在调试运行起来之后,会新打开一个vsCode窗口，在新vscode窗口标题栏中显示[Extension Development Host]

　　![img](https://img2020.cnblogs.com/blog/1182611/202003/1182611-20200323165842094-2073256326.png)

 　　然后在当前窗口中，运行命令，快捷键ctrl+shift+p打开命令行，会展示出vscode的命令列表，选择对应命令并执行hello world命令；

　　　可以看到vscode窗口中会弹出hello world，这样一个插件就运行起来了

4、开发扩展插件

　　在开发之前看两个重要的文件内容，一遍后续顺利开发，一个是package.json，这里面是配置命令的，即commands

　　另一个是extension.ts，这个文件是用于封装插件逻辑，并注册插件的。

　　基本整个插件编写都是围绕着这两个文件来开发的

　　首先来看一下package.json配置文件：

```
   {
    "name": "testDemo",              //插件扩展名称（对应创建项目时候的输入）
    "displayName": "testDemo",
    "description": "message tips",  //插件扩展的描述（对应创建项目时候的输入）
    "version": "0.0.1",
    "publisher": "ssx",             //发布时候的一个名称（对应创建项目时候的输入,在插件打包和发布的时候必须的，所以不能少）
    "engines": {
        "vscode": "^0.10.10"
    },
    "categories": [
        "Other"
    ],
    "activationEvents": [              //这是触发插件执行的配置，通过名称来执行extension.ts里面的插件代码
        "onCommand:extension.sayHello" //这种是通过输入命令来触发执行的
    ],
    "main": "./out/src/extension",    //这个是配置TypeScript编译成js的输出目录
    "contributes": {
        "commands": [{               //title 和 command是一个对应关系的
            "command": "extension.sayHello", //这个是对应上面那个命令触发的，在代码里面也要用到
            "title": "Hello World"   //这个是我们在vscode里面输入的命令
        }]
    },
    "scripts": {                     //是在发布打包，或者其他运行时候，要执行的一些脚本命令
        "vscode:prepublish": "node ./node_modules/vscode/bin/compile",
        "compile": "node ./node_modules/vscode/bin/compile -watch -p ./",
        "postinstall": "node ./node_modules/vscode/bin/install"
    },
    "devDependencies": {           //这是开发的依赖包，如果有其他的依赖包，并要打包的话，需要把dev去掉
        "typescript": "^1.8.5",
        "vscode": "^0.11.0"
    }
   }
```

　　接着看一下extension.ts代码文件

```
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

    // Use the console to output diagnostic information (console.log) and errors (console.error)
    // This line of code will only be executed once when your extension is activated
    console.log('Congratulations, your extension "testdemo" is now active!');

    // The command has been defined in the package.json file
    // Now provide the implementation of the command with registerCommand
    // The commandId parameter must match the command field in package.json
　　 //这是注册扩展的API，第一个参数是扩展的ID，必须跟package.json保持一致，第二个参数是封装插件逻辑的回调函数，所有的插件逻辑在这里编写

    let disposable = vscode.commands.registerCommand('extension.helloWorld', () => {
        // The code you place here will be executed every time your command is executed
　　　　　//这里就是编写插件逻辑的地方
        // Display a message box to the user
        vscode.window.showInformationMessage('Hello World!自定义的提示');
    });
　　//通过subscriptions订阅插件
    context.subscriptions.push(disposable);

    let heDemo = vscode.commands.registerCommand('extension.helloDemo',() => {
        vscode.window.showInformationMessage('自定义的提示');
    })
    context.subscriptions.push(heDemo);

}

// this method is called when your extension is deactivated
export function deactivate() {}
```

 5、打包和发布

　　需要安装一个打包工具vsce

```
npm install -g vsce
```

　　安装完成后可以用命令窗口 cd 到你的项目目录下去，然后执行命令，进行打包

```
vsce package
```

　　执行完成后会打包出一个.vsix格式的文件插件，这就是插件安装包了

```
vsce publish
```

   来发布到marketplace.visualstudio.com上面去。发布成功后可以在vscode里面用 `ext install` 来按这个插件

6、从VSIX安装扩展插件

　　通过以下命令行进行安装

```
code --install-extension myextension.vsix
```

　　最后的参数为扩展插件的路径文件名称，安装完成就可以使用了