# Plop 基于模板生成组件基本结构

#### Plop 基于模板生成组件基本结构

当每个包的结构确定之后，就可以基于模板生成一个组件的基本结构，不需要每次手动配置相应的设置。

可以使用 Plop 实现这个功能。

安装 plop

```js
yarn add plop -D -W
```


编写模板

在根目录创建 plop_template/component 文件夹存放组件模板文件。

复制一份写好的组件到这个目录进行修改：

```js
|- __tests__
|  |- component.test.hbs
|- src
|  |- component.hbs
|- stories
|  |- component.stories.hbs
|- index.hbs
|- LICENSE # 固定的版权内容
|- package.hbs
|- README.hbs
```


src/component.hbs：

properCase： name 首字母 大写

```js
<template>
  <div>
  </div>
</template>


<script>
export default {
  name: 'MyEl{{properCase name}}',
  props: {
  }
}
</script>


<style></style>
```


__tests__/component.test.hbs：

```js
import input from '../src/{{name}}.vue'
import { mount } from '@vue/test-utils'

describe('my-el-{{properCase name}}', () => {
})
```


stories/component.stories.hbs：

```js
import MyEl{{properCase name}} from '../src/{{name}}.vue'

export default {
  title: 'MyEl{{properCase name}}',
  component: MyEl{{properCase name}}
}

export const {{properCase name}} = () => ({
  components: { MyEl{{properCase name}} },
  template: `
    <div>
      <my-el-{{name}}></my-el-{{name}}>
    </div>
  `,
})
```


index.hbs：

```js
import MyEl{{properCase name}} from './src/{{name}}.vue'

MyEl{{properCase name}}.install = Vue => {
  Vue.components(MyEl{{properCase name}}.name, MyEl{{properCase name}})
}

export default MyEl{{properCase name}}
```


package.hbs：

```js
{
  "name": "my-el-{{name}}",
  "version": "0.0.3",
  "description": "my-el-{{name}} component",
  "main": "dist/cjs/index.js",
  "module": "dist/es/index.js",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "MIT"
}
```


README.hbs：

```js
# MyEl{{properCase name}}
```

plop 配置文件
在根目录创建 plopfile.js

```js
module.exports = plop => {
  // 创建生成器
  plop.setGenerator('component', {
    // 生成器描述
    description: 'create a custom component',
    // 询问
    prompts: [
      // 组件名称
      {
        type: 'input',
        name: 'name',
        message: 'component name',
        default: 'MyComponent'
      }
    ],
    // 把基于模板创建的文件，放到指定的目录
    actions: [
      {
        type: 'add',
        path: 'packages/{{name}}/src/{{name}}.vue',
        templateFile: 'plop-template/component/src/component.hbs'
      },
      {
        type: 'add',
        path: 'packages/{{name}}/__tests__/{{name}}.test.js',
        templateFile: 'plop-template/component/__tests__/component.test.hbs'
      },
      {
        type: 'add',
        path: 'packages/{{name}}/stories/{{name}}.stories.js',
        templateFile: 'plop-template/component/stories/component.stories.hbs'
      },
      {
        type: 'add',
        path: 'packages/{{name}}/index.js',
        templateFile: 'plop-template/component/index.hbs'
      },
      {
        type: 'add',
        path: 'packages/{{name}}/LICENSE',
        templateFile: 'plop-template/component/LICENSE'
      },
      {
        type: 'add',
        path: 'packages/{{name}}/package.json',
        templateFile: 'plop-template/component/package.hbs'
      },
      {
        type: 'add',
        path: 'packages/{{name}}/README.md',
        templateFile: 'plop-template/component/README.hbs'
      }
    ]
  })
}
```

根目录 package.json 中配置 plop 脚本

```js
// /package.json
"scripts": {,
	"plop": "plop",
	//...
}
```


执行脚本，输入要创建的组件的名字，完成创建。
