# 用vue styleguidist 写组件文档

官方文档：

Documenting components | Vue Styleguidist
https://vue-styleguidist.github.io/docs/Documenting.html#code-comments

#### 示例代码地址

vue-styleguidist-demo

参考博文：

使用vue styleguidist编写组件文档_「已注销」的博客-CSDN博客_styleguidist vue

#### 安装与基本配置

npm包安装

```javascript
npm install --save-dev vue-styleguidist
```

package.json 加入styleguide相关命令

```json
{
  "scripts": {
    "styleguide": "vue-styleguidist server",
    "styleguide:build": "vue-styleguidist build"
  }
}
```

vue-styleguidist server: 运行开发服务器。
vue-styleguidist build：生成静态 HTML 样式指南。
选项	描述
–config	指定配置文件的路径
–open	在默认浏览器中打开 Styleguidist
–verbose	打印调试信息

#### 基本配置

默认情况下，Styleguidist 将使用此glob 模式搜索组件: src/components/**/*.vue

```javascript
// styleguide.config.js
module.exports = {
    title: 'Vue Styleguidist 示例',
    //components: 'src/components/**/[A-Z]*.vue',
    //自定义要展示的组件
    sections: [
      {
        name: '企业基本信息',
        components: 'src/views/qyxxgl/Qyjbxx/index.vue',
        description: '企业基本信息页面'
      }
    ],
    version: '1.1.1',
    styleguideDir: 'styleguide-dist',
    // 在编辑器的右上角添加一个小按钮，用于将编辑器的内容复制到剪贴板
    copyCodeButton:true,
    // 是否每个章节是一个独立的页面. 默认:false
    pagePerSection: true,
    // props/events/slot的说明默认是展开还是收缩: expand / collapse / hide
    usageMode: 'expand',
    // 左侧导航默认是展开还是收缩: expand / collapse / hide
    tocMode: 'expand',
    // 显示 prop、事件、槽或方法是否来自当前文件或在 mixin 或扩展组件中配置。如果它是外部的，它会显示组件的名称，并在悬停时显示文件的相对路径。
    displayOrigins: true
}
```

#### 章节配置

如果不进行额外的配置章节, 那么每个组件会是一个章节

name:  章节标题
content:  包含概览内容的 Markdown 文件的位置
components:  glob 模式字符串、组件路径数组或 glob 模式字符串，或返回组件列表或 glob 模式字符串的函数。相同的规则适用于 root components选项
sections:  子章节数组（可以嵌套）
description:  本章节的简短说明
sectionDepth:  单页的子节数，仅在启用pagePerSection 时可用。
exampleMode:  代码示例选项卡的初始状态，使用exampleMode。
usageMode:  道具和方法选项卡的初始状态，使用usageMode。
ignore:  不应包含在该部分中的字符串/glob 数组。
href:  要导航到的 URL，而不是导航到部分内容
external:  如果设置，链接将在新窗口中打开

```javascript
module.exports = {
  sections: [
    {
      name: 'Introduction',
      content: 'docs/introduction.md'
    },
    {
      name: 'Documentation',
      sections: [
        {
          name: 'Installation',
          content: 'docs/installation.md',
          description: 'The description for the installation section'
        },
        {
          name: 'Configuration',
          content: 'docs/configuration.md'
        },
        {
          name: 'Live Demo',
          external: true,
          href: 'http://example.com'
        }
      ]
    },
    {
      name: 'UI Components',
      content: 'docs/ui.md',
      components: 'lib/components/ui/*.vue'
    }
  ]
}
```

编写文档
@displayName修改组件在文档中的显示名称
要放到 export default 上面

<script>
  /**
   * The only true button.
   * @displayName Best Button
   */
  export default {
@model标注自定义的v-model
如果你想标注自定义的v-model, 可以使用@model注解

<script>
  export default {
    name: 'my-checkbox',
    props: {
      /**
       * @model
       */
      value: String
    }
  }
</script>
@values标注可选值

```javascript
export default = {
    props: {
      /**
       * The size of the button
       * @values small, normal, large
       */
       size: {
         type: String,
         default: 'normal'
       }
     }
}
```

@example标注使用示例

```javascript
/**
* @example
* multiply(3, 2);
*/
```

@deprecated标记为已弃用

```javascript
/**
 * An example-less button.
 * @deprecated Use the [only true button component](#button) instead
 */
```

@see @link
@see标签表示可以参考另一个标识符的说明文档，或者一个外部资源。您可以提供一个标识符的namepath或自由格式的文本。如果你提供了一个namepath，JSDoc的默认模板会自动将namepath转换成链接。

```javascript
/**
 * Both of these will link to the bar function.
 * @see {@link bar}
 * @see bar
 */
    function foo() {}
// Use the inline {@link} tag to include a link within a free-form description.
/**
 * @see {@link foo} for further information.
 * @see {@link http://github.com|GitHub}
 */
    function bar() {}
```

@author
@author <name> [<emailAddress>]

```javascript
/**
 * @author Jane Smith <jsmith@example.com>
 */
    function MyClass() {}
```

@since
@since标签标明一个类，方法，或其它标识符是在哪个特定版本开始添加进来的。

```javascript
/**
 * Provides access to user information.
 * @since 1.0.1
 */
    function UserRecord() {}
```

@ignore 忽略某些props
默认情况下，您的组件拥有的所有 props 都被认为是公开的并已发布。在极少数情况下，您可能希望从文档中删除一个 prop，同时将其保留在代码中。该@ignore标签允许您执行此操作

```javascript
props: {
    /**
    * @ignore
    */
    color: {
      type: String,
      default: '#333'
    }
}
```

给自定义事件(emit)写注释

```javascript
/**
 * Success event.
 */
   this.$emit('success')
/**
 * Success event.
 *
 * @event success
 */
   this.$emit(EVENTS.success)
```

如果您的事件返回参数/属性，请使用@property @arg @param标记来描述它们
```javascript
/**
 * Triggers when the number changes
 *
 * @property {number} newValue new value set
 * @property {number} oldValue value that was set before the change
 */
   this.$emit('change', newValue, oldValue)
```

如果自定义事件写在了模板中

<div>
  <!--
    triggered on click
    @event click
    @property {object} demo - example
    @property {number} called - test called
  -->
  <button @click="$emit('click', test)"></button>
</div>
给插槽(slot)写注释
<template>
  <div class="modal">
    <div class="modal-container">
      <div class="modal-head">
        <!-- @slot Use this slot header -->
        <slot name="head"></slot>
      </div>
      <div class="modal-body">
        <!-- @slot Use this slot body -->
        <slot name="body"></slot>
      </div>
    </div>
  </div>
</template>
<div slot-scope="row" class="list-item1">
  {{row.item.text}}
  <!--
  	@slot Menu Item footer
		@binding {object} icon icon of the menu item
		@binding {string} text text of the menu item
	-->
  <slot name="test" :icon="row.item.icon" :text="row.item.text" />
</div>
```javascript
export default {
  render(createElement) {
    return createElement('div', [
      /**
       * @slot The header
       * binding {object} menuItem the menu item
        */
           this.$scopedSlots.default({
             menuItem: this.message
           })
         ])
       }
     }
export default {
  functional: true,
  render: function (createElement, { data, children: cld }) {
    /* slot describe destructured default */
    return createElement('div', data, cld)
  }
}
```


如果 vue-styleguidist没有检测到您的插槽，您可以在渲染函数之前使用注释块明确告诉它：

```javascript
export default {
  /**
   * Place the content of your menuitem here,
   * the value of index and content will be passed down to you
   * @slot menuContent
   * @binding {number} index the index in the list
   * @binding {string} content text of the item
   */
       render: function () {

    // ...

  }
}
```

如果您有多个插槽，请一个接一个放置多个块：

```javascript
export default {
  /**
   * @slot content
   */
  /**
   * @slot icon
     */
       render: function () {

    // ...

  }
}

```

Mixins和Extends
他们会自动添加到你的组件文档中

组件使用示例和Readme
vue-styleguidist会自动查找组件目录下的Readme.md和组件名.md作为示例插入到生成的文档中.

如果您想忽略某个组件的自述文件，请使用@example [none]doclet。当同一文件夹中的多个组件共享一个ReadMe文件时使用此选项。这将防止示例被多次渲染。

md编写示例

Vue component example:

```jsx
    <Button size="large">Push Me</Button>
```

One more with generic code fence:

```
<Button size="large">Push Me</Button>
```

You can disable an editor by passing a `noeditor` modifier:

```jsx noeditor
<Button>Push Me</Button>
```

To render an example as highlighted source code add a `static` modifier:

```jsx static
<Button>Push Me</Button>
```

You can also initialize vue to construct more complex examples in two ways:

1. Create a new Vue instance

```js
const names = require('dog-names').all;
 
new Vue({
  data(){
    return {
      list: names
    }
  },
  template: `
    <div>
      <RandomButton :variants="list" />
    </div>
  `
})
```

2. Single-file components with a language tag of vue (supports <style scoped>)

```vue
  <template>
    <div class="wrapper">
      <Button id="dog-name-button" @click.native="pushButton">Push Me</Button>
      <hr />
      <p class="text-name">Next Dog Name: </p>
    </div>
  </template>
 
  <script>
    const dogNames = require('dog-names').all;
 
    // You can also use 'exports.default = {}' style module exports.
    export default {
      data() {
        return { numClicks: 0, dogName: dogNames[0] };
      },
      methods: {
        pushButton() {
          this.numClicks += 1;
          this.dogName = dogNames[this.numClicks];
        }
      }
    }
  </script>
 
  <style scoped>
    .wrapper {
      background: blue;
    }
    .text-name {
      color: red;
    }
  </style>
```

Examples with all other languages are rendered only as highlighted source code, not an actual component:

```html
<Button size="large">Push Me</Button>
```

Any [Markdown](http://daringfireball.net/projects/markdown/) is **allowed** _here_.

使用@example doclet 将其他示例文件与组件相关联
以下组件还将从extra.examples.md文件中加载一个示例：

```javascript
/**

 * Component is described here.
   *
 * @example ./extra.examples.md
   */
   export default {
     name: 'Button'
     // ...
   }
```

@example [none] 忽略示例文件
@examples的doclet也可以用于忽略所连接的ReadMe文件。使用它可以避免多次渲染示例。

```javascript
/**

 * Component is described here.
   *
 * @example [none]
   */
   export default {
     name: 'Button'
     // ...
   }
```

@public标注公共方法
默认情况下，您的组件拥有的任何方法都被认为是私有的，不会被发布。用 JSDoc 标记你的公共方法[@public 标记以将它们发布在文档中：

```javascript
/**

 * Insert text at cursor position.
   *
 * @param {string} text
 * @public
   */
   insertAtCursor(text) {
     // ...
   }
```



<template>
  <!-- -->
</template>

<script>
  /**
   * The only true button.
   * @version 1.0.1
   */
  export default {
    name: 'Button',
    props: {
      /**
       * The color for the button.
       * @see See [Wikipedia](https://en.wikipedia.org/wiki/Web_colors#HTML_color_names)
       * @see See [MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value) for a list of color names
       */
      color: {
        type: String,
        default: '#333'
      },
      /**
       * The size of the button
       * `small, normal, large`
       * @since Version 1.0.1
       */
      size: {
        type: String,
        default: 'normal'
      },
      /**
       * Gets called when the user clicks on the button
       */
      onClick: {
        type: Function,
        default: event => {
          console.log('You have clicked me!', event.target)
        }
      }
    },
    methods: {
      /**
       * Gets called when the user clicks on the button
       *
       * @param {SyntheticEvent} event The react `SyntheticEvent`
       * @param {Number} num Numbers of examples
       * @public This is a public method
       */
      launch(event, num) {
        /* ... */
      },
      // ...
      ignoreMethod() {
        /**
         * Success event.
         *
         * @event success
         * @type {object}
         */
        this.$emit('success', {})
      }
    }
    /* ... */
  }
</script>

可组合组件
当组件是列表或表格时，使用组合 API 编写它会更容易。

例如，以这种方式阅读下拉元素会更容易

<DropDown>
  <Choice val="1">value 1</Choice>
  <Choice val="2">value 2</Choice>
</DropDown>
比用下面这种方式更好

<DropDown
  :choices="[{val:1,text:'value 1'}, {val:2,text:'value 2'}]"
/>
以下是 Vue Styleguidist 如何帮助记录此模式：请将@requires doclets添加到主要组件。

在前面的示例中，我们有一个DropDown需要Choice组件才能正确呈现的组件。下面是 DropDown的例子。

<template>
  <select>
    <slot />
  </select>
</template>
<script>
/**
 * @requires ./Choice.vue
 */
export default {
  name: 'DropDown'
}
</script>
注意：现在Choice将仅作为DropDown文档的一部分. 它不会有自己的页面或自己的示例。

为TypeScript、Flow 和Class组件写注释

```javascript
import { Component, Prop, Vue } from 'vue-property-decorator'

@Component({
  name: 'ClassButton'
})
export default class MyComponent extends Vue {
  aHiddenData: string = ''

  /**

   * prop typed through the decorators arguments
     */
       @Prop({ type: String })
       propNoType = ''

  /**

   * prop typed through an annotation
     */
       @Prop() propA: number = 0

  /**

   * prop with a default value
     */
       @Prop({ default: 'default value' })
       propB: string = 'hello'

  /**

   * prop with a hybrid type
     */
       @Prop() propC: string | boolean = false

  /**

   * method testing
   * @public
     */
       onClick(a: string) {

    /**
     * Success event when we click
     */
    this.$emit('success', a)

  }
}
```

为JSX写注释

```vue
export default {
  render() {
    return (
      <div>
        {/** @slot Use this slot to have a header */}
        <slot name="header" />
        {this.contentText}
      </div>
    )
  }
}
```

编写示例代码
直接在markdown中编写示例代码
Markdown 中的代码示例使用 ES6 语法。他们可以使用全局变量访问样式指南的所有组件：

```vue
    <Panel>
      <p>
        Using the Button component in the example of the Panel component:
      </p>
      <Button>Push Me</Button>
    </Panel>
```
还可以import从 Markdown 示例中的其他模块（例如，您在单元测试中使用的模拟数据）：

```vue
const mockData = require('./mocks');
<Message :content="mockData.hello" />
```

**注意：**如果您需要更复杂的演示，通常最好在单独的 JavaScript 文件中定义它并import在 Markdown 中定义它。如果组件文件与 markdown 位于同一文件夹中，请编写import { myExample as exam } from './myExample';您然后可以在示例中使用此导入的设置对象。注意，import进来的组件本身的代码, 不会出现在文档中。

在markdown中导入外部示例文件
为了使自动完成和语法突出显示实用，还可以从外部文件导入示例。在下面的示例./myExample.vue中将用作示例。

```[import](./myExample.vue)
Text typed here will be entirely ignored. You can use it to describe the example imported for maintenance purposes
```
这会有两个效果:

示例会被渲染出来
示例文件的源代码也会被显示出来