# Rollup 打包

#### Rollup 打包

在把项目发布出去之前，还需要打包处理，可以选择 Rollup 打包。

​	Rollup 是一个模块打包器
​	Rollup 支持 Tree-shaking
​		静态分析代码中的 import ，排除未使用的代码
​		webpack 实现 Tree-shaking 需要配置
​	打包的结果比 Webpack 小
​	开发框架/组件库的时候使用 Rollup 更合适

#### 安装依赖

因为管理的所有包都需要打包，所以 rollup 以及它的插件，都要安装到工作区的根目录。

​	rollup
​	rollup-plugin-terser - 代码压缩
​	rollup-plugin-vue@5.1.9 - 将单文件组件编译成 JS 代码
​		注意要指定版本，因为这个插件的最新版本编译 Vue3.0 的组件
​	vue-template-compiler - rollup-plugin-vue 编译 SFC 需要的依赖

```js
yarn add rollup rollup-plugin-terser rollup-plugin-vue@5.1.9 vue-template-compiler -D -W
```



#### Rollup 配置

根目录创建 rollup.config.js，类似 webpack。

先尝试打包一个组件：

在包（例如 packages/button）目录下创建配置文件 rollup.config.js

```js
// /packages/button/rollup.config.js
import vue from 'rollup-plugin-vue'
import { terser } from 'rollup-plugin-terser'

module.exports = [
  {
    // 入口
    input: 'index.js',
    // 出口
    output: [
      {
        file: 'dist/index.js',
        // 配置打包模块化的方式 es:ESM  cjs:CommonJS
        format: 'es'
      }
    ],
    // 插件
    plugins: [
      vue({
        // 把单文件组件中的样式，插入到html中的style标签
        css: true,
        // 把组件转换成 render 函数
        compileTemplate: true
      }),
      // 代码压缩
      terser()
    ]
  }
]
```



#### 配置scripts脚本

在 button 目录下的 package.json 中添加打包的脚本：

```js
// /packages/button/package.json
"scripts": {
  // -c 使用配置文件打包，未指定的话，默认为 ./rollup.config.js
  "build": "rollup -c"
}
```



执行 build 需要进入button目录。

也可以使用 yarn workspace 执行工作区的脚本。

```js
# button 是 package.json 中定义的 name，不是文件夹名称

yarn workspace button run build
```



### 一个命令打包所有组件

#### 安装依赖

还需要安装一些依赖

​	@rollup/plugin-json - 使rollup可以把json文件作为模块加载，配置文件中会用到
​	rollup-plugin-postcss - rollup的postcss插件
​	@rollup/plugin-node-resolve - 打包的过程中把依赖的第三方包打包进来
​		例如打包 formitem 的时候需要把它依赖的校验工具 async-validator 打包进来

```js
yarn add @rollup/plugin-json rollup-plugin-postcss @rollup/plugin-node-resolve -D -W

```



#### 配置文件

因为当前项目下的每个包的打包方式都是一样的，所以可以为所有包动态生成rollup的配置文件。

在项目根目录创建配置文件。

这个配置文件本质上是一个node程序，它的作用是为 packages 目录下的所有包生成 rollup的配置。

这个文件会导出一个数组，数组中的每一个元素就是对应包的配置。

```js
// /rollup.config.js
import fs from 'fs'
import path from 'path'
import json from '@rollup/plugin-json'
import vue from 'rollup-plugin-vue'
import postcss from 'rollup-plugin-postcss'
import { terser } from 'rollup-plugin-terser'
import { nodeResolve } from '@rollup/plugin-node-resolve'

// 判断环境，生产环境会开启代码压缩
const isDev = process.env.NODE_ENV !== 'production'

// 公共插件配置
const plugins = [
  vue({
    // Dynamically inject css as a <style> tag
    css: true,
    // Explicitly convert template to render function
    compileTemplate: true
  }),
  json(),
  nodeResolve(),
  postcss({
    // 把 css 插入到 style 中
    // inject: true,
    // 把 css 放到和js同一目录
    extract: true
  })
]

// 如果不是开发环境，开启压缩
isDev || plugins.push(terser())

// 获取 packages 文件夹路径，作为处理的根路径
const root = path.resolve(__dirname, 'packages')

// 读取 packages 中的所有包
module.exports = fs.readdirSync(root)
  // 过滤，只保留文件夹
  .filter(item => fs.statSync(path.resolve(root, item)).isDirectory())
  // 为每一个文件夹创建对应的配置
  .map(item => {
    // 获取包的 package.json 文件
    // @rollup/plugin-json 使 rollup 可以使用 require 的方式将json文件作为模块加载
    // 它返回json对象
    const pkg = require(path.resolve(root, item, 'package.json'))

    // 返回rollup的配置对象
    return {
      // 打包入口：拼接绝对路径
      input: path.resolve(root, item, 'index.js'),
      // 配置打包出口
      // 分别打包两种模块类型 cjs 和 es
      // 路径使用 package.json 中配置的 main 和 module
      output: [
        {
          exports: 'auto',
          file: path.resolve(root, item, pkg.main),
          format: 'cjs'
        },
        {
          exports: 'auto',
          file: path.join(root, item, pkg.module),
          format: 'es'
        },
      ],
      // 配置插件
      plugins: plugins
    }

  })
```



#### 配置scripts脚本

在根目录的 package.json 中配置 build 脚本

```js
// /package.json
"scripts": {
  "build": "rollup -c",
  // ...
}
```



然后给 packages 中每一个包的 package.json 设置 main和 module。

这是打包的出口，也是使用者在使用这个包的时候的入口。

以 button 为例：

```js
// /packages/button/package.json
{
  // main 对应 CommonJS 模块，通过module-exports导出
  "main": "dist/cjs/index.js",
  // module 对应 ESM，通过export default 导出
  "module": "dist/es/index.js",
  //...
}
```


所有包都修改完后，根目录下执行 yarn build 进行打包。

打包完成后，会在每个包的目录下创建 dist 文件，里面包含打包的结果。

#### 设置环境变量

安装 cross-env，可以跨平台设置环境变量

```js
yarn add cross-env -D -W
```



修改 scripts 的 build 脚本：

```js
// /package.json
"scripts": {
  "build:prod": "cross-env NODE_ENV=production rollup -c",
  "build:dev": "cross-env NODE_ENV=development rollup -c",
  // ...

}
```



运行两个脚本：

​	prod - 生产环境，代码会被压缩
​	dev - 开发环境，代码不会被压缩

#### 清理

​	清理所有包中的 node_modules
清理所有包中的 node_modules 可以使用 lerna clean 命令。

在根目录 package.json 中添加这个脚本：

```js
// /package.json
"scripts": {
	"clean": "lerna clean",
	// ...
}

```

执行命令：

```js
yarn clean

# 展示要清理的node_modules目录，并提示是否继续

? Proceed? # y
```

执行完毕，所有包node_modules 被删除（根目录的不受影响）

​	清理所有包中的 dist
使用第三方库 rimraf 清理指定目录

```js
yarn add rimraf -D -W
```



分别在每个包的 package.json 中配置脚本：

```js
// /package.json
"scripts": {,
	"del": "rimraf dist"
	// ...
}
```



使用 yarn workspaces 执行每个包中的命令：

```js
yarn workspaces run del
```


注意这里不是指定单个工作区的 workspace，而是指定所有工作区的 workspaces

