# 大白话之DSL（领域特定语言）

## 背景

因为最近在做一个需求，其中涉及复杂流程编排与多种状态流转。

我们的场景是我们系统有商户入驻的概念，入驻先得申请，等运营提交审批动作后进入审批流程；

期间有不少的业务分支可能审批的流程都是不一样的。比如A场景某个业务可能只要风控审批；

另一个B场景的业务可能希望其他人员审批。

而且这个审批节点是变化的（随时可能需要调整审批节点或顺序），所以要求能动态更新审批规则节点。

![img](https://pic2.zhimg.com/80/v2-2569ecb43e9a385bae442ba2621aaa11_720w.webp)



## 那这个背景跟我今天分享的DSL有什么联系呢？

其实通过我的需求背景你应该能感受出来，业务场景本身比较复杂，原来硬编码话业务逻辑非常混乱，扩张性和维护性也很差。根本达不到动态扩展的要求。而且也支持不了动态修改的能力。所以仔细思索后发现 “规则引擎” 能干这些事情。

比如国内的LiteFlow等框架，其原理部分都有提到DSL（领域特定语言）。

因为这些组件势必要考虑用某一语言比如Json，XML来定义规则内容，定义好规则内容后需要解析、运行规则吧。

你看这里讲到通过支持XML，JSON等语言来定义诠释规则 ，就跟DSL相关了。

接下来会好好讲下DSL，还是大白话思路讲解DSL知识点，希望能起到一个入门作用。

## 什么是DSL


DSL是一种工具，它的核心价值在于，它提供了一种手段，可以更加清晰地就系统某部分的意图进行沟通。

这种清晰并非只是审美追求。一段代码越容易看懂，就越容易发现错误，也就越容易对系统进行修改。

因此，我们鼓励变量名要有意义，文档要写清楚，代码结构要写清晰。基于同样的理由，我们应该也鼓励采用DSL。

按照定义来说，DSL是针对某一特定领域，具有受限表达性的一种计算机程序设计语言。

### ***这一定义包含3个关键元素\***：

1）**语言性**（language nature）：DSL是一种程序设计语言，因此它必须具备连贯的表达能力——不管是一个表达式还是多个表达式组合在一起。

2）**受限的表达性**（limited expressiveness）：通用程序设计语言提供广泛的能力：支持各种数据、控制，以及抽象结构。

这些能力很有用，但也会让语言难于学习和使用。DSL只支持特定领域所需要特性的最小集。使用DSL，无法构建一个完整的系统，相反，却可以解决系统某一方面的问题。

3）**针对领域**（domain focus）：只有在一个明确的小领域下，这种能力有限的语言才会有用。这个领域才使得这种语言值得使用。

比如正则表达式，/\d{3}-\d{3}-\d{4}/就是一个典型的DSL，解决的是字符串匹配这个特定领域的问题。

## DSL的分类

按照类型，DSL可以分为三类：

1. 内部DSL（Internal DSL）
2. 外部DSL（External DSL）
3. 以及语言工作台（Language Workbench）。

### Internal DSL

Internal DSL是一种通用语言的特定用法。

用内部DSL写成的脚本是一段合法的程序，但是它具有特定的风格，而且只用到了语言的一部分特性，用于处理整个系统一个小方面的问题。

用这种DSL写出的程序有一种自定义语言的风格，与其所使用的宿主语言有所区别。例如一些开源状态机就是Internal DSL，它不支持脚本配置，使用的时候是Java语言，但并不妨碍它也是DSL。

> builder.externalTransition()
> .from(States.STATE1)
> .to(States.STATE2)
> .on(Events.EVENT1)
> .when(checkCondition())
> .perform(doAction());

### External DSL

External DSL是一种“不同于应用系统主要使用语言”的语言。

外部DSL通常采用自定义语法，不过选择其他语言的语法也很常见（XML就是一个常见选 择）。比如像Struts和Hibernate这样的系统所使用的XML配置文件。

常见的还有：XML、SQL、JSON、 Markdown

### Workbench

Workbench是一个专用的IDE，简单点说，工作台是DSL的产品化和可视化形态。

三个类别DSL从前往后是有一种递进关系，Internal DSL最简单，实现成本也低，但是不支持“外部配置”。Workbench不仅实现了配置化，还实现了可视化，但是实现成本也最高。他们的关系如下图所示：

![img](https://pic1.zhimg.com/80/v2-685312f5abfaf1391bbe25c23c0dee78_720w.webp)



### 不同DSL该如何选择

几种DSL类型各有各的使用场景，选择的时候，可以这样去做一个判断。

Internal DSL：假如你只是为了增加代码的可理解性，不需要做外部配置，我建议使用Internal DSL，简单、方便、直观。

External DSL：如果你需要在Runtime的时候进行配置，或者配置完，不想重新部署代码，可以考虑这种方式。比如，你有一个规则引擎，希望增加一条规则的时候，不需要重复发布代码，那么可以考虑External。

Workbench：配置也好，DSL Script也好，这东西对用户不够友好。比如在淘宝，各种针对商品的活动和管控规则非常复杂，变化也快。我们需要一个给运营提供一个workbench，让他们自己设置各种规则，并及时生效。这时的workbench将会非常有用。

## 有什么作用、解决了什么问题

场景一：

像我文章开始处介绍的几款框架，构建了专属的内部DSL，延伸出状态机解决方案、规则引擎解决方案等。

> Liteflow:
> 轻量，快速，稳定，可编排的组件式规则引擎/流程引擎。 拥有全新设计的**DSL**规则表达式。 组件复用，同步/异步编排，动态编排，复杂嵌套规则，热部署，平滑刷新规则等等功能，让你加快开发效率！

场景二：

比如SQL，其实也是一门DSL，当然属于外部DSL范畴。还有正则表达式啊，都是解决某一领域特定问题，比较局限！

> 但是外部DSL如果独立构建话，是需要从零开发的DSL，在词法分析、解析技术、解释、编译、代码生成等方面拥有独立的设施。
> 我们说开发外部DSL近似于从零开始实现一种拥有独特语法和语义的全新语言。
> 构建工具make 、语法分析器生成工具YACC、词法分析工具LEX等都是常见的外部DSL。
> 例如：正则表达式、XML、SQL、JSON、 Markdown等；

![img](https://pic2.zhimg.com/80/v2-4eaace61dfdfd0f21a335ae82e3141e5_720w.webp)

场景三

从DDD范畴来讲，有一个通用语言，解决的是有一套通用语言能让领域专家和开发人员面对复杂业务需求很多专有名词都能达到认证上的统一。

![img](https://pic1.zhimg.com/80/v2-7ea1c50541b5c739f12add10ebe8243c_720w.webp)



![img](https://pic3.zhimg.com/80/v2-ff0465dd4fc6ee810d37e20a9332459e_720w.webp)

------

## 公众号

**欢迎大家关注我的公众号：【陶朱公Boy】**。

![img](https://pic4.zhimg.com/80/v2-6aebfefed5d423ef64d6b8950229415f_720w.webp)

里面不仅汇集了硬核的干货技术、还汇集了像左耳朵耗子、张朝阳总结的高效学习方法论、职场升迁窍门、软技能。希望能辅助你达到你想梦想之地！

如果你正计划面试，我还可以免费给大家指导优化你的简历，让你更具竞争优势。