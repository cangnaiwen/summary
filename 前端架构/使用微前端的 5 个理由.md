# 使用微前端的 5 个理由

微前端是一种越来越流行的前端开发架构方法。这很容易代表前端 Web 开发的未来，这就是为什么了解这种架构可以为应用程序和开发团队带来的主要好处是至关重要的。

让我们首先研究什么是微前端架构，然后深入探讨现在采用它的五个理由。

**什么是微前端架构？
**

在过去的几年里，IT 公司已经开始将大型软件分解成更小、更易于管理的块。这种方法背后的想法是拥有许多可以独立开发、测试和部署的服务。

这就是微服务架构在后端开发方面的意义。但同样的方法也可以应用于前端开发，它被称为微前端架构。在Martin Fowler 的官网上，微前端的做法是这样定义的：一种架构风格，其中可独立交付的前端应用程序被组合成一个更大的整体。

这种前端 Web 开发的架构方法变得越来越流行，因为众所周知的陷阱伴随着传统的单体方法。这主要是因为前端软件往往会快速增长，而当使用单体架构时，一切都变得更加难以维护。

另一方面，微前端能够实现不那么复杂和繁琐的架构。特别是，由于微前端方法，可以将整个应用程序拆分为小的、独立的部分。然后，它们中的每一个都可以由不同的前端团队甚至使用不同的技术来实现。这确保了与后端微服务架构相同的可扩展性、灵活性和适应性。此外，这种方法允许在同一网页上混合使用库或不同框架开发的微前端组件。

因此，微前端现在已成为 IT 社区的一种趋势，并且这种方法正在被越来越多地采用，这不足为奇。

**今天选择微前端的 5 个好处**

现在，让我们看看使用它的五个最相关的理由。

**扩展到多个团队
**

通常团队由具有不同背景和技能的开发人员组成。有些是React、其他Vue.js或Angular方面的专家。有些人喜欢用JavaScript编写代码，有些则喜欢用TypeScript编写代码。最初，这代表了一个障碍。唯一的解决办法是找到共同点，尽管这种选择会迫使一些开发人员学习新技术并失去他们的专业知识。因此，我们寻找解决方案并决定采用微前端架构方法。

多亏了这一点，才能够将原来的团队拆分成多个团队，让每个人都能发挥自己的最佳水平。这是因为不同的团队可以根据他们必须处理的业务逻辑在架构、测试和编码风格方面做出最佳决策。此外，这种方法本质上会导致代码和样式隔离，使每个团队独立于其他团队。

微前端在最终结果方面也很有帮助。事实上，拥有多个可以自由使用他们喜欢的技术的小团队意味着他们本质上不受限制，因此更有动力编写更高质量的代码。

**采用不同的技术堆栈
**

由于微前端由小而独立的部分组成，因此它们中的每一个都可以使用不同的技术堆栈来实现。这是一种不可思议的强大力量。首先，因为起始团队可以根据特定技术堆栈的专业知识拆分成许多小团队，这也尊重单一责任原则。其次，由于许多技术堆栈将用于同一个项目，因此聘请新开发人员变得更加容易。

此外，微前端方法实际上消除了对特定技术的锁定现象，或者至少大大减少了。这是因为团队始终可以决定选择新的技术堆栈，而无需翻译之前开发的内容。另外，微前端架构包含的每个块肯定比前端单体小，将其转化为新技术将花费更少的时间。

此外，由于我们的团队采用了微前端方法，因此我们受到激励去尝试新技术、库和框架。事实上，无论何时你必须在应用程序中移植一个新的部分，都可以决定采用一个全新的技术栈。这是学习如何使用市场上众多 JavaScript 框架的宝贵机会。

**开发和部署变得更快
**

另一个需要解决的重要方面是，通过采用微前端，作为一个团队的前端开发过程会得到极大的改进。主要原因是，不是让一个大团队被迫处理不可避免的沟通开销，而是同时处理不同功能的较小独立团队的一部分，而不管实现细节如何。

可以想象，这也代表着在发布新功能方面向前迈出了一大步。原因是开发过程有了很大改进，主要原因是与大型单体软件相比，构建小型微前端更快、更容易。因此，部署时间也将显着缩短。事实上，每当团队完成一项功能的工作时，他们都可以在线部署它而无需等待。

换句话说，微前端应用程序基于同时处理独立功能的独立团队。这不能不代表一个实现更高发布率的机会，特别是随着小团队数量的增加。

**它使Web 应用程序更易于维护
**

如果你曾经处理过大型应用程序，你就会知道它们很容易变得难以维护，尤其是当它们是单一的并且必然会变大时。另一方面，微前端基于分而治之的方法。这意味着，通过为Web 应用程序选择此架构，可以使每个业务需求更易于测试和维护。

测试大型单体应用程序具有挑战性并且需要花费大量时间，我们都知道这一点。但自从我们采用了微前端方法后，一切都变了。每个团队现在负责测试其开发的功能，这些功能远小于完整的前端应用程序。这加快了整个过程并使其更容易。因此，现在没有人害怕测试。此外，每个独立的团队现在都可以自由使用他们喜欢的测试工具和技术。

此外，处理小块意味着更容易理解正在发生的事情的流程。这导致 Web 应用程序构建在许多更可靠且在需要时易于维护的小部件上。

**它代表了前端开发的未来
**

根据2020 年微服务现状报告，24% 的开发人员使用过微前端。这意味着越来越多的公司正在利用这种方法的力量，并且一些流行的前端应用程序有望在不久的将来采用它。换句话说，微前端可能代表前端开发的下一步。

我毫不怀疑它代表了整体式前端开发方法的自然演变。另一方面，它仍然是一项相对较新且有些不成熟的技术，还有很长的路要走。这也是为什么存在应该讨论的缺点（例如某些微前端实现导致依赖项重复）。

与此同时，Web 应用程序也在不断发展，可以毫不畏惧地说微前端是前端开发的自然演变。

**结论**

在这篇文章中，我们研究了采用微前端架构方法的五个最重要的原因。微前端方法允许将前端应用程序拆分为彼此独立的小块。

虽然不如后端开发中使用的微服务架构那么流行，但其背后的思想几乎是一样的。微前端架构现在成为趋势并不奇怪，它可能代表了前端开发的自然演变。这也是为什么知道它是不可或缺的，而学习现在采用它的主要原因就是本文的目的。