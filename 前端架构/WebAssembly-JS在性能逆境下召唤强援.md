# WebAssembly-JS在性能逆境下召唤强援

## 目录

- webassembly的作用
- webassembly项目的编码流程
- webassembly的性能提升
- webassembly的兼容性
- 通过Rust接入WebAssembly
- 通过C/C++接入WebAssembly
- webassembly相关的接口 API
- webassembly的未来展望
- webassembly的使用场景及其限制
- webassembly的产品案例

## webassembly的作用

webassembly是一种底层的二进制数据格式和一套可以操作这种数据的JS接口的统称。我们可以认为webassembly的范畴里包含两部分

- wasm: 一种体积小、加载快并且可以在Web浏览器端运行的底层二进制数据格式，并且可以由C++等语言转化而来
- webassembly的操作接口：例如WebAssembly.instantiate就可以将一份wasm文件编译输出为JS能够直接调用的模块对象

**打破性能瓶颈**

一直以来，我们都比较关心JS的运行速度问题，V8引擎解决了绝大多数情况下遇到的问题，但是少数情况下我们进行大量本地运算的时候，仍然可能遇到性能瓶颈，需要优化，这个时候webassembly的作用就凸现出来了

## **webassembly项目的编码流程**

- 性能无强关的部分用JS编写
- 性能强相关的，并且需要大量本地运算的部分，先用C++/Rust编写，通过命令行工具转化为wasm代码后让JS调用

![img](https://pic1.zhimg.com/80/v2-5c81d1866b0ae8b1d0550e55e1fe6204_720w.webp)



## **玄学的webassembly性能提升**

webassembly相对于纯JS的性能提升是随具体场景和条件的变化而变化的

> 当您使用WebAssembly时，不要总是期望得到20倍的加速。您可能只得到2倍的加速或者20%的加速。或者，如果您在内存中加载非常大的文件时，或者需要在WebAssembly和JavaScript之间进行大量通信时，那么速度可能会变慢。
> 作者：Robert 《Level Up With WebAssembly》一书的作者，同时也是一位生物信息学软件工程师

```text
参考链接：https://python.freelycode.com/contribution/detail/1528
```

在上面的文章的作者Robert，做了这样一个实验，他使用 seqtk，一个用C编写的评估DNA测序数据质量(通常用于操作这些数据文件)的软件，去对比webassembly相对于普通JS带来的性能提升

**一.Robert的对比测试结果**

下面是他的测试结果

- **第一步**：运行序列分析软件seqtk，对比性能：**9倍**提升
- **第二步**：删除不必要的printf输出，对比性能：**13倍**提升
- **第三步**：去除函数的重复调用后，对比性能：**21倍**提升

![img](https://pic1.zhimg.com/80/v2-33d0814e9c351aa761c1e5b390df2aac_720w.webp)

当然，上面的概括也许太过简略，大家可以看看Robert的原文以得到更为详细的认识


二.**运行Fibonacci函数的性能对比**

有位博主，对比了运行递归无优化的Fibonacci函数的时候，WebAssembly版本和原生JavaScript版本的性能差距，下图是这两个函数在值是45、48、50的时候的性能对比。

![img](https://pic4.zhimg.com/80/v2-dcbf6e78235f342945d0e6161f8f73e7_720w.webp)

```text
文章链接：https://segmentfault.com/a/1190000016949129
作者：detectiveHLH
```

**三.IVweb的的性能对比测试**

IVWeb团队对长度不同的文本进行加密处理，对比webassembly相对于纯JS的性能提升，结果发现

- 对于长文本（2M文本） 的密集计算，webassembly的性能提升很大
- 对于短文本（"IVWEB"）的密集计算，webassembly和纯JS性能相差无几

**第一组测试**：2M长文本100000 次加密处理

![img](https://pic2.zhimg.com/80/v2-d654423254b38e00546a1fa6b5dccfc9_720w.webp)

**第二组测试**："ivweb"短字符加密100000 次

![img](https://pic3.zhimg.com/80/v2-9db70f93419613bc3915c75c81599b62_720w.webp)

```text
资料来源：https://zhuanlan.zhihu.com/p/79792515
```



**从上面的资料中我了解到，webassembly性能提升的确存在，但是这个提升的范围是随条件和场景而变化的，需要遵循一定的原则**

## webassembly的**兼容性**

下面是我在can i use上查到的结果，可以看到在现代浏览器上兼容良好，覆盖率达到88%。主要的问题在于IE浏览器不支持（IE11）

![img](https://pic3.zhimg.com/80/v2-c864f0635d4ab18db2f7d9229369ac5e_720w.webp)

**IE兼容解决方案**

Internet Explorer 11 是最后一个占有很大的市场份额，但不支持wasm的浏览器。我们可以通过 binaryen 项目的 wasm2js 工具，将我们的 WebAssembly 编译成 JavaScript，就可以获得 IE11 的大部分支持了

## 实战 WebAssembly

在浏览器中使用WebAssembly主要有两种方式：

- 编写Rust代码，然后通过wasm-pack转化成wasm代码
- 编写C/C++代码，然后通过Emscripten转化成wasm代码

> 备注：Rust是一门高性能的系统编程语言

## **通过Rust接入WebAssembly**

参考MDN的这篇文档能够非常方便地跑起一个Demo

```text
文章：《Rust 和 WebAssembly 用例》
地址： https://developer.mozilla.org/zh-CN/docs/WebAssembly/Rust_to_wasm
```

1.安装rustup，初始化Rust环境，它会顺带安装cargo等工具（相当于前端的Node安装）

```text
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

2.安装编译工具wasm-pack（相当于前端的babel）

```ps1con
cargo install wasm-pack
```

3.创建一个文件夹，进入后运行下面代码，初始化一个Rust 项目

```text
cargo new --lib hello-wasm
```

初始化的文件夹如下所示



![img](https://pic3.zhimg.com/80/v2-ded9bf959812a088fa5ae14e81e2ad82_720w.webp)

4.修改lib.rs,改为以下几段Rust代码,这段代码的is_odd是一个判断数字是否为奇数的方法

```rust
extern crate wasm_bindgen;

use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn is_odd(n: u32) -> bool {
    n % 2 == 1
}
```

5.修改配置文件Cargo.toml

这个文件和我们的package.json有点像，我们就依样画葫芦，这个文件大概要写成下面这个样子

```text
[package]
name = "hello-wasm"
version = "0.1.0"
authors = ["作者名"]
edition = "2018"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html
[lib]
crate-type = ["cdylib"]

[dependencies]
wasm-bindgen = "0.2"
```

**备注**

- dependencies中必须要有wasm-bindgen这个依赖
- 同时还要指定crate-type = ["cdylib"]，否则转化不能成功

6.运行以下命令进行编译转化

```text
wasm-pack build --scope [自己的名字]
// My Example
wasm-pack build --scope penghuwan
```

**编译开始**

![img](https://pic4.zhimg.com/80/v2-15156ec9ac7be5ae43bc8f49627b8e4b_720w.webp)

**编译成功后，新增了pkg文件夹和target文件夹**

![img](https://pic4.zhimg.com/80/v2-f4749717c6033e167e8d693c633c2c8b_720w.webp)



让我们看看pkg文件夹下的文件有哪

![img](https://pic3.zhimg.com/80/v2-2c88cf8e0b6fb6efee1e95dd094321b2_720w.webp)

\7. 将包发布到npm

```text
1.cd pkg
2.npm publish --access=public
```

8.安装刚刚发布的wasm模块，并通过webpack工具加载后，在浏览器运行以下代码

```js
const js = require("hello-wasm");
js.then(js => {
 const num1 = js.is_odd(3);
 const num2 = js.is_odd(4);
  console.log(num1);
  console.log(num2);
});
```

9.浏览器输出

![img](https://pic3.zhimg.com/80/v2-3d0a8b669c500810646043a9faa90b3a_720w.webp)



## **通过C/C++接入WebAssembly**

**1.首先要按照文档下载编译工具emscripten**

```text
https://emscripten.org/docs/getting_started/Tutorial.html#tutorial
```

**备注:**如果没有将source ./emsdk_env.sh写入到启动文件中的话，那么每次使用前都要在给定目录下运行一遍

**2.创建一个文件h.c,写入以下代码**

```cpp
#include <stdio.h>

int main(int argc, char ** argv) {
 printf("Hello World");
}
```

3.用命令行编译它

```text
emcc h.c -s WASM=1 -o h.js
```

生成文件如下图所示

![img](https://pic2.zhimg.com/80/v2-b56a2037d59da69402f97b15a196f2b5_720w.webp)

4.运行生成的h.js，则可看到输出了Hello World

![img](https://pic4.zhimg.com/80/v2-2a1998376f1e78bd1860508893fd2173_720w.webp)

## **WebAssembly相关的接口 API**

看了上面的案例，你可能会觉得有些奇怪：怎么我们没有涉及浏览器提供的webassembly的API呀？

其实是有的,只不过在工具编译的时候自动帮忙填写了一些API而已，我们看下上面从h.c编译出来的h.js的一些片段就知道了

![img](https://pic4.zhimg.com/80/v2-4e3b54f72d230fe993e8e8a997abb8a7_720w.webp)

**下面我们就来介绍下怎么手动去写这些API**

**接口**

**>> WebAssembly.Instance**

实例包含所有的 WebAssembly 导出函数 ,允许从JavaScript 调用 WebAssembly 代码.

**对象属性**

- exports属性: 一个对象，该对象包含从WebAssembly模块实例导出的所有函数属性

**>> WebAssembly.Module**

包含已经由浏览器编译的无状态 WebAssembly 代码，可以高效地与 Workers 共享、缓存在 IndexedDB 中，和多次实例化。

**对象属性**

- exports属性：一个数组，内容是所有已声明的接口的描述。
- imports属性和：一个数组，内容是所有已声明的引用的描述。

```text
参考链接： https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/WebAssembly/Instance
```

**方法**

**>> WebAssembly.instantiate**

它是编译和实例化 WebAssembly 代码的主要方法

- 参数:包含你想编译的wasm模块二进制代码的ArrayBuffer的类型实例

返回值： 一个Promise, resolve后的值如下所示

```js
{
  module: 一个被编译好的 WebAssembly.Module 对象. 
  instance: 一个WebAssembly.Instance对象
}
```

Example

```js
fetch('simple.wasm').then(response =>
  response.arrayBuffer()
).then(bytes =>
  WebAssembly.instantiate(bytes)
).then(result =>
  result.instance.exports // exports是wasm中输出的
);
```

## webassembly的未来展望

- 多线程
- SIMD（单指令流多数据流）
- 64位寻址
- 流式编译（在下载的同时编译 WebAssembly 文件）
- 分层编译器
- 隐式 HTTP 缓存

![img](https://pic3.zhimg.com/80/v2-7396f8c0cbe3de1be990535ffebcc5ca_720w.webp)

```text
参考文章：https://hacks.mozilla.org/2018/10/webassemblys-post-mvp-future/
```

## **webassembly的使用场景及其限制**

之前我们已经说到，webassembly适用于JS难以解决的大计算量的应用场景，如图像/视频编辑、计算机视觉，3D游戏等等。在这些场景下，webassembly能够大限度地提高速度，弥补JS的缺陷和硬伤。

同时在另一方面，我们也需要认识到以下几点：

1. 其实在大多数场景下我们都不需要用到webassembly。因为V8等JS引擎的优化带来了巨大的性能提升，已经足够让JS应对绝大多数的普通场景了，所以只有在以上的少数场景下，我们才需要做这种“二次提升”
2. 和很多其他特性一样，兼容性同样是webassembly的一道坎，现代浏览器虽然支持度良好，但是在国内IE泛滥的特殊情况下， 这仍然是对webassembly的一个挑战。不过在桌面应用上或者一些对兼容性要求较低的工具型网页运用上，webassembly已经生根发芽，甚至能够遍地开花。

## **webassembly的产品案例**

**设计工具Figma**

一般情况下，为了使用速度，设计工具都会选择Adobe等本地应用，而不会选择浏览器网页应用，而能够同时打开十几个画板也没有卡顿的Figma正在尝试改变这一认知，webassembly让它具有高效流畅的体验

![img](https://pic1.zhimg.com/80/v2-b2de680d1c7e01072c8308f13855bfa8_720w.webp)

**白鹭游戏引擎**

白鹭游戏引擎是一套HTML5游戏开发解决方案，它衍生了开发莽荒纪同名手游、梦道、坦克风云的等游戏，而利用 WebAssembly，白鹭引擎让游戏运行性能提升了300%。

![img](https://pic1.zhimg.com/80/v2-b6554cdff4210673e5b6497e6559787c_720w.webp)

**OpenGL 图形引擎Magnum**

Magnum 是一款数据可视化 OpenGL 图形处理引擎，也采用了WebAssembly支撑浏览器环境的应用

![img](https://pic1.zhimg.com/80/v2-89dc98b99a27ca556d6da2025abf7ff4_720w.webp)

```text
参考资料:https://my.oschina.net/editorial-story/blog/1615877
```