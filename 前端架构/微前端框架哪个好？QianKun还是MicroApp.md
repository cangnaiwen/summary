# 微前端框架哪个好？QianKun还是MicroApp

在当前云原生微服务、业务中台、低代码平台等IT架构下，不再是传统的烟囱式应用系统建设，而是打破企业业务部门竖井，建立企业级的信息化平台（数据中台、业务中台），那么对业务开发的解耦和聚合将成为关键技术，目前对于系统后端已有成熟的微服务架构，基于SpringBoot开发微服务，通过SpringCloud或istio进行微服务治理。前端也同样有类似的需求，如何支持不同的前端团队开发各自业务的UI页面，运行时通过统一的框架集成整合起来，这就是微前端框架出现的主要诉求。

## 一、什么是微前端

2016年底，“Micro frontend”一词首次出现在ThoughtWorks Technology Radar上。它将微服务的概念扩展到前端世界。当前的趋势是构建一个功能丰富、功能强大的浏览器应用程序，也就是单页应用程序，它位于微服务架构之上。随着时间的推移，前端层(通常由一个独立的团队开发)不断增长，并且越来越难以维护。这就是我们所说的前端巨石（[Frontend Monolith](https://link.zhihu.com/?target=https%3A//www.youtube.com/watch%3Fv%3DpU1gXA0rfwc)）。



![img](https://pic1.zhimg.com/80/v2-df9087debaee9aff87c161317a697e94_720w.webp)

Micro frontend背后的理念是将网站或网页应用视为独立团队所拥有的功能的组合。每个团队都有自己关心和擅长的独特业务或任务领域。团队是跨功能的，开发从数据库到用户界面的端到端特性。

微前端是一种类似于微服务的架构，它将微服务的理念应用于浏览器端，即将 Web 应用由单一的单体应用转变为**多个小型前端应用聚合为一的应用**。各个前端应用还可以**独立运行、独立开发、独立部署**。



![img](https://pic4.zhimg.com/80/v2-f93e2dda198b445881a1a9f8dac37d97_720w.webp)



## 二、微前端有什么优势

**1、复杂度可控:** 每一个UI业务模块由独立的前端团队开发,避免代码巨无霸,保持开发时的高速编译,保持较低的复杂度,便于维护与开发效率。

**2、独立部署:** 每一个模块可单独部署,颗粒度可小到单个组件的UI独立部署,不对其他模块有任何影响。

**3、技术选型灵活:** 也是最具吸引力的,在同一项目下可以使用如今市面上所有前端技术栈vue react angular, 也包括未来的前端技术栈。

**4、容错:** 单个模块发生错误,不影响全局，就跟后端微服务一样。

**5、扩展:** 每一个服务可以独立横向扩展以满足业务伸缩性，解决资源的不必要消耗；



## 三、微前端框架选型

我们团队在选型微前端框架时，调研了市面上实现微前端的框架，可供选择的有iframe、sigle-spa、qiankun和microApp。single-spa太过于基础，对原有项目的改造过多，成本太高； iframe在所有微前端方案中是最稳定的、上手难度最低的，但它有一些无法解决的问题，例如性能低、通信复杂、双滚动条、弹窗无法全局覆盖，它的成长性不高，只适合简单的页面渲染。剩下的只有qiankun和microApp了。

![img](https://pic4.zhimg.com/80/v2-54f479648cb64932eb0d5d449cec714f_720w.webp)



### 1、京东MicroApp

MicroApp是一款基于类WebComponent进行渲染的微前端框架，不同于目前流行的开源框架，它从组件化的思维实现微前端，旨在降低上手难度、提升工作效率。它是目前市面上接入微前端成本最低的框架，并且提供了JS沙箱、样式隔离、元素隔离、预加载、资源地址补全、插件系统、数据通信等一系列完善的功能。MicroApp与技术栈无关，也不和业务绑定，可以用于任何前端框架和业务。

![img](https://pic4.zhimg.com/80/v2-ded24e5f4f92bb505dd5baa09797fc03_720w.webp)

MicroApp 的核心功能在CustomElement基础上进行构建，CustomElement用于创建自定义标签，并提供了元素的渲染、卸载、属性修改等钩子函数，我们通过钩子函数获知微应用的渲染时机，并将自定义标签作为容器，微应用的所有元素和样式作用域都无法逃离容器边界，从而形成一个封闭的环境。

### 2、阿里乾坤qiankun

qiankun 是一个基于 [single-spa](https://link.zhihu.com/?target=https%3A//github.com/CanopyTax/single-spa) 的[微前端](https://link.zhihu.com/?target=https%3A//micro-frontends.org/%22%20%5Ct%20%22_blank)实现库，旨在帮助大家能更简单、无痛的构建一个生产可用微前端架构系统。qiankun 孵化自蚂蚁金融科技基于微前端架构的云产品统一接入平台，在经过一批线上应用的充分检验及打磨后，我们将其微前端内核抽取出来并开源，希望能同时帮助社区有类似需求的系统更方便的构建自己的微前端系统，同时也希望通过社区的帮助将 qiankun 打磨的更加成熟完善。目前 qiankun 已在蚂蚁内部服务了超过 200+ 线上应用，在易用性及完备性上，绝对是值得信赖的。



![img](https://pic4.zhimg.com/80/v2-de4debe260a1106a4f0341de5e8ef873_720w.webp)